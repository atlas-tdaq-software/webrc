#!/bin/sh

# -Dorg.eclipse.jetty.util.log.class=org.eclipse.jetty.util.log.StdErrLog \
version=1.1.0
source /sw/tdaq/setup/setup_tdaq-12-00-00.sh

# we'll setenv BEARER_TOKEN before seinding commands so that caller of acquire (RCCommander) will get fresh tonek via env
export TDAQ_TOKEN_ACQUIRE="env"

# since use of java/config layer with JNI, this is needed to prevent crash (also in Igui)
export TDAQ_ERS_NO_SIGNAL_HANDLERS=1

#-Dtdaq.ipc.init.ref="corbaloc:iiop:10.193.6.75:54305/%ffipc/partition%00initial/ipc/partition/initial" \

exec $TDAQ_JAVA_HOME/bin/java
-Dorg.eclipse.jetty.LEVEL=INFO \
-Djacorb.log.default.verbosity=2 -Djacorb.poa.log.verbosity=2 -Djacorb.poa.monitoring=off \
-Djacorb.giop.conn.log.verbosity=2 \
-XX:+UseParallelOldGC -XX:+UseNUMA \
-Xms1024M -Xmx4096M \
-Djacorb.poa.thread_pool_shared=true -Djacorb.poa.thread_pool_min=25 -Djacorb.poa.thread_pool_max=100 \
-Djacorb.poa.thread_priority=5 -Djacorb.poa.queue_wait=on -Djacorb.poa.queue_max=100000 -Djacorb.poa.queue_min=99950 \
-Djacorb.connection.client.idle_timeout=10000 \
-cp ${TDAQ_CLASSPATH}:target/webrc-${version}.war tdaq/webrc/server/Start
