#!/bin/bash

# -Dorg.eclipse.jetty.util.log.class=org.eclipse.jetty.util.log.StdErrLog \
# -verbose:gc -Xloggc:/tmp/webrc_gc_`/bin/date +%s` -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintTenuringDistribution -XX:+PrintGCApplicationConcurrentTime -XX:+PrintGCApplicationStoppedTime \
version=1.1.0

source /sw/tdaq/setup/setup_tdaq-12-00-00.sh
_here=$(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}}))

# cd to the web app so the servlet/jetty server code does not need to know the version
cd ${_here}/target/webrc-${version}

# we'll setenv BEARER_TOKEN before sending commands so that caller of acquire (RCCommander) will get fresh tonek via env
# export TDAQ_TOKEN_ACQUIRE="env"

# since use of java/config layer with JNI, this is needed to prevent crash (also in Igui)
export TDAQ_ERS_NO_SIGNAL_HANDLERS=1
export TDAQ_IPC_ENABLE_TLS=1

#-Dtdaq.ipc.init.ref="corbaloc:iiop:10.193.6.75:54305/%ffipc/partition%00initial/ipc/partition/initial" \

exec $TDAQ_JAVA_HOME/bin/java \
-Dlog4j.configuration="file:${_here}/etc/log4j.xml" \
-Dtdaq.webrc.properties="${_here}/etc/webrc.properties.gpn" \
-Dorg.eclipse.jetty.LEVEL=INFO \
-Djacorb.log.default.verbosity=1 -Djacorb.poa.log.verbosity=1 -Djacorb.poa.monitoring=off \
-Djacorb.giop.conn.log.verbosity=1 \
-XX:+UseParallelOldGC -XX:+UseNUMA \
-Xms2024M -Xmx8096M \
-Djacorb.poa.thread_pool_shared=true -Djacorb.poa.thread_pool_min=20 -Djacorb.poa.thread_pool_max=80 \
-Djacorb.poa.thread_priority=5 -Djacorb.poa.queue_wait=on -Djacorb.poa.queue_max=200000 -Djacorb.poa.queue_min=190000 \
-Djacorb.connection.client.idle_timeout=10000 \
-Dtdaq.ipc.init.ref=${TDAQ_IPC_INIT_REF} \
-cp WEB-INF/classes:WEB-INF/lib/*:${TDAQ_CLASSPATH} \
 tdaq/webrc/server/Start 2>${_here}/logs/stderr.$$ 1>${_here}/logs/stdout.$$


