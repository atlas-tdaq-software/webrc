package tdaq.webrc ;


/**
 *  Defines functions used from RCTreeProvider, implemented by different HomePage instances
 */

interface IHomePage {

	default String getGlobalRCState() {
		return "NONE" ;
	}
	
	// called from ISReceiver (via RCNodesHolder) when RootController state changes
	default void globalRCStateChanged(final String rcstate, boolean busy) {

	}

	default HomePage.Mode getMode() {
		return HomePage.Mode.DISPLAY ;
	}

	// Needed in HomePage
	default void partitionGone(final String partition) {

	}
}