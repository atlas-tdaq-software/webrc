package tdaq.webrc ;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.HashMap;
import java.util.Map;


// load properties from file and provide a staic API to access by name
public class Configuration {

    static final String propsFileName = "tdaq.webrc.properties" ;

    static final Map<String, String> DEFAULT_CONFIG = new HashMap<String, String>() {
        /**
        *
        */
        private static final long serialVersionUID = 1899007518588583425L;

        {
        put("tdaq.release.version", "tdaq-11-02-00");
        put("local.bin.path",       "/usr/local/webrc/bin");
        put("partlogs.url.path",    "/partlogs/");
        put("pmginfo.url.path",     "/info/current/");
        put("http.portnumber",      "8888");
        put("tdaq.release.setup",   "/sw/tdaq/setup/setup_tdaq-11-02-00.sh") ;
        put("dbroot.path",          "/tbed/git/oks/tdaq-11-02-00") ;
        put("readonly",             "false") ;
        put("experimental",         "false") ;
        put("rctree.update.interval",       "1000") ;
        put("erstable.update.interval",     "2000") ;
        put("ldap.url.anonymous",                  "ldap://xldap.cern.ch") ;
        put("ldap.url",                         "ldap://cerndc.cern.ch:389") ;
        put("is.executor.threads",              "8") ;
        put("erstable.rows",                    "10") ;
        put("erstable.maxmessages",             "20000") ;
        put("erstable.timeformat",              "dd/MM HH:mm:ss") ; // "yyyy-MM-dd HH:mm:ss z"
        put("chart.rates.seconds.update",       "10000") ; // milliseconds
        put("chart.rates.minutes.update",       "40000") ; // "yyyy-MM-dd HH:mm:ss z"
        put("busy.source.inst",                         "L1CT.CTP.Instantaneous.BusyFractions") ;
        put("busy.source.run",                          "L1CT.CTP.PerRun.BusyFractions") ;
        put("busy.index",                           "3") ; // index of ctpcore_objects[] attribute
        put("busy.cables.index",                    "5") ; // index of ctpout_cables[] attribute
        put("busy.ctpcore.total.index",             "9") ; // index of total busy object in ctpcore_objects[] vector
        put("busy.ctpcore.simple.index",            "15") ; // index of Simple deadtime busy object in ctpcore_objects[] vector
        put("busy.ctpcore.complex.index",           "13") ; // index of Complex busy object in ctpcore_objects[] vector
        put("busy.ctpcore.total.fraction.index",    "1") ; // index of fraction float attribute in ctpcore type
        put("busy.run.display.threshold",           "0.1") ; // %, display threshold for accumulated busy 
        put("grafana.url",    "/tdaq/pbeastDashboard/d-solo/0MtKyc2Mk/basic-dashboard-for-atlas-for-webrc?orgId=1") ; // appended to host
        put("grafana.refresh",    "&theme=light&refresh=30s") ; // refresh interval for panels
        put("grafana.interval",   "&from=now-10m&to=now") ; 
        put("grafana.segment.panelid.TDAQ", "2") ; 
        put("grafana.segment.panelid.HLT",  "16") ; 
        put("grafana.segment.panelid.HLTSV",    "26") ; 
        put("grafana.segment.panelid.SFO",  "23") ; 
        put("grafana.segment.panelid.L1CentralTrigger", "36") ; 
        put("grafana.segment.panelid.ATLAS", "twiki/bin/view/Main/PlanOfTheDay?cover=print") ; 
        put("erstable.acrfilter.location", "combined/sw/Igui-ers.data.xml") ; 
        put("expert.mode.group.required", "atlas-tdaq-cc-expert") ; 
        put("control.mode.group.required", "atlas-testbed-user") ;
        put("is.mode.pull", "false") ; 
   }};

    static Properties props ;

    static {
        String fileName = System.getProperty(propsFileName) ;
        if ( fileName == null ) {
            System.err.println("FATAL: System property " + propsFileName + " must be defined, exiting...") ;
            System.exit(1) ;
        }
        try ( InputStream input = new FileInputStream(fileName)) {
            props = new Properties();
            props.load(input);
        } catch (final IOException ex) {
            System.err.println("FATAL: Failed to load properties from file " + fileName + ": " + ex.getMessage()) ;
            System.exit(1) ;            
        }
    }

    public static String get(final String propname) {
        String prop = System.getProperty(propname) ; // first look into -D in JVM command line
        if ( prop != null ) { return prop ; } ;
        prop = props.getProperty(propname) ; // now check the config file
        if ( prop == null ) {
            return DEFAULT_CONFIG.get(propname) ; // may be null, got an exceptiin later, fatal anyway
        }
        return prop ;
    }

    public static int getInt(final String propname) {
        String prop = props.getProperty(propname) ;
        if ( prop == null ) {
            prop = DEFAULT_CONFIG.get(propname) ; // may be null, got an exceptiin later, fatal anyway
        }
        try {
            int ret = Integer.parseInt(prop) ;
            return ret ;
        } catch ( final NumberFormatException ex ) {
            ex.printStackTrace() ;
        }            
        return 0 ;  
    }
}