package tdaq.webrc;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession; 
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication; 
import org.apache.wicket.util.lang.Bytes;

/**
 * Application object for your web application.
 * If you want to run this application without deploying, run the Start class.
 * 
 * @see tdaq.Start#main(String[])
 */
public class WebRCApplication extends AuthenticatedWebApplication
{
	// either define System property tdaq.ipc.init.ref, $TDAQ_IPC_INIT_REF
	final static String ipcref_property = "tdaq.ipc.init.ref" ;
	// for tbed, tdaq-08-03-01, taken from file:/tbed/tdaq/sw/ipc/tdaq-08-03-01/ipc_root.ref converted to corbalock
	final static String def_ipc_init = "corbalocc:iiop:10.193.6.75:58679/%ffipc/partition%00initial/ipc/partition/initial" ;

	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends WebPage> getHomePage()
	{
		return HomePage.class;
	}

	@Override
	protected Class<? extends AbstractAuthenticatedWebSession> getWebSessionClass(){
			return SignInSession.class;
	}

	@Override
	protected Class<? extends WebPage> getSignInPageClass() {
			return SignInPage.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init()
	{
		super.init();
		
		System.err.println("Initializing webrc application");

		getStoreSettings().setMaxSizePerSession(Bytes.kilobytes(5000));
		
		// avoid serialization
		setPageManagerProvider(new NoSerializationProvider(this));
		
		// for debug
		//getDebugSettings().setAjaxDebugModeEnabled(true) ;
	}
	
	@Override
	public void sessionUnbound(String sessionId) {
		super.sessionUnbound(sessionId);
		ers.Logger.log("Session unbound: " + sessionId) ;
	}    
	
	@Override
	public void onDestroy() {
		super.onDestroy() ;
		ISReciever.exitCleanup() ;
		ConfigProvider.exitCleanup() ;
		ERSProvider.cleanUp() ;
		HomePage.exitCleanup() ;
		ers.Logger.log("WebRCApplication destroyed");
	} 

}
