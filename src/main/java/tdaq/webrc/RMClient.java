package tdaq.webrc ;

import com.sun.jna.Library ;
import com.sun.jna.Native ;

import daq.rmgr.*;

/** Class to re-implement a missing method in RMClientImpl */
public class RMClient extends RMClientImpl {

    public RMClient() {
        super() ;
    }

    // JNA magic
private interface CLibrary extends Library {
    CLibrary INSTANCE = (CLibrary) Native.load("c", CLibrary.class);   
    int getpid ();
}

/**
 * Requests resource for process. This method got process id automatically from the system and this value is used in the RM server in resource request and stored in the RM in case
 * of resource was granted. This value can be used to free resource or get information about it.
 * @return int  RM handle identifier. If this value is equled to zero - means there are no needs of resources and can run. If handle less than zero - process shoudl be stopped (no available resources for it). 
 * @param partition java.lang.String Name of the partition
 * @param resource  java.lang.String Resource configuration UID
 * @param computerID  java.lang.String Computer object UID.
 * @param userID  java.lang.String Computer object UID.
 * 
 * @exception daq.rmgr.ObjectNotFoundE  Some accociated with the applciation object was not found in the RM server. It could be computer or RM resource etc.
 * @exception daq.rmgr.UnavailableResourcesE   There are no enough resources for the application.
 * @exception daq.rmgr.ResourceNotFoundE Requested resource was not found in configuration. 
 * @exception RuntimeException  Runtime exception. May be RM server object did not found.
*/
/*
public int requestResourceForMyProcess( String partition, String resource, String computerID, String userID )  throws  
            daq.rmgr.ObjectNotFoundE, daq.rmgr.UnavailableResourcesE ,daq.rmgr.ResourceNotFoundE, RuntimeException {
    int process_id = CLibrary.INSTANCE.getpid() ;
    ResID[] res_lst = new ResID[1];
    res_lst[0] = new ResID();
    res_lst[0].id = resource;
    long tm = System.currentTimeMillis();
    tm = (tm << 5) >> 5;
    int time = (int) tm;
    rmgr.ResMgr rmdb = getDDB() ;
    int hndl = rmdb.requestResourcesForProcess( partition, res_lst, computerID, process_id, userID, time, true);
    return hndl;
} 
*/

}
