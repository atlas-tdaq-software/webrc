package tdaq.webrc;

// import java.time.ZoneId;
import java.util.List;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Map ;
import java.util.concurrent.ConcurrentHashMap ;
import java.net.InetAddress ;
//import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.Executors ;
import java.util.concurrent.ExecutorService ;

import org.apache.wicket.util.time.Duration;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.AjaxSelfUpdatingTimerBehavior;
import org.apache.wicket.ajax.AbstractAjaxTimerBehavior;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.extensions.ajax.markup.html.AjaxEditableLabel;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.extensions.markup.html.repeater.tree.DefaultNestedTree;
import org.apache.wicket.extensions.markup.html.repeater.tree.NestedTree;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.Application;
import org.apache.wicket.request.http.WebRequest;
import org.apache.wicket.request.Request;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.core.request.handler.IPartialPageRequestHandler;
import org.apache.wicket.util.cookies.CookieUtils ;
import org.apache.wicket.markup.head.IHeaderResponse ;
import org.apache.wicket.markup.head.JavaScriptHeaderItem ;
import org.apache.wicket.protocol.http.request.WebClientInfo ;
import org.apache.wicket.protocol.http.WebSession ;

import daq.rc.Command;
import daq.rmgr.RMClientImpl;

/**
 * Main page of the web application, container of all other Components
 */
public class HomePage extends WebPage implements IHomePage, AutoCloseable {
	// global counter, updated from SessionListener
	private	static AtomicInteger sessionCounter = new AtomicInteger(0) ;

	private static Map<String, HomePage> allPages = new ConcurrentHashMap<String, HomePage>() ;
	
	public static pmgClient.PmgClient pmgclient ; // used to query PMG for partition owner, also from PartitionExitMonitor to link to RootController
		
	public enum Mode { CONTROL, DISPLAY } ;
//	private enum RCState { NONE, INITIAL, CONFIGURED, CONNECTED, RUNNING, ONHOLD } ;

	private static final long serialVersionUID = 343453451L;
	public static final String rc_app_name = "RootController";
	public static final String default_rc_app_name = "DefaultRootController";
	private static final String default_subscription = "sev=FATAL OR sev=ERROR OR sev=WARNING";
	private PartitionsChoice partitionsChoice ;
	private String currentPartition = new String("Not selected") ;
	private DropDownChoice<String> ersrowsChoice ;
	private AjaxLink<String> ersPauseButton ;
	private AjaxEditableLabel<String> ersSubscrLabel ;
	private ERSProvider ersProvider ;
	RCTreeNodeProvider rctreeprov = null ;
	NestedTree<RCNode> tree = null ;
	private ERSDataTable ersTable ;
	private ERSIssueFilter ersFilter = new ERSIssueFilter() ;

	// global RC commands
	private AjaxLink<String> firstCommandButton, secondCommandButton, thirdCommandButton , reloadButton ;
	private String firstCommandButtonLabel = "SHUTDOWN" ; // always visible (unless in NONE)
	private String secondCommandButtonLabel = "INITIALIZE" ; // currently allowed command, always visible, changing
	private String thirdCommandButtonLabel = "HOLD TGR" ; // available in RUNNING only
	private boolean triggerOnHold = false ;
	String globalRCState = "NONE" ;
	AtomicBoolean globalRCBusy = new AtomicBoolean(false) ;
	AtomicBoolean globalStateChanged = new AtomicBoolean(true) ;

	private String selectedNodeName = "Not selected" ;
	private Label selectedNodeLabel ;

	// partition owner, current user, control mode
	private String partitionOwner ; // user under which account RootController runs
	private Label modeLabel, userControlLabel ;
	// private String modeName = "DISPLAY";
	private Mode mode = Mode.DISPLAY ;
	private int rmControlHandle = 0 ; // Resource Manager handle of allocated UserControl partition resource
	// per-application command menu
	private AjaxLink<String> inOutRCButton, ignoreRCButton, restartRCButton, killRCButton, disableRCButton ;
	private String inOutButtonLabel = "OUT" ;
	private String disableRCButtonLabel = "DISABLE" ;
	private String killRCButtonLabel = "KILL" ; // or EXIT for RootController
	private ExternalLink logsOutButton, logsErrButton, pmgInfoButton ;
	private WebMarkupContainer rcbuttonsPanel, applogsPanel, pbeastIframe, grafanaUrlPanel, ratesPanel, runparamsPanel ;

	private boolean dbReloadOk = false ;
	private boolean dbReloadNeeded = false ;

	private Label rcPannelErrorLabel ;
	private String rcPannelError ;
	private ModalWindow reloadDbModalWindow, partitionControlModalWindow ;
	private String urlHost, requestUrl ;

	private Label rnlabel, lblabel, recordinglabel, tier0projectlabel, trigkeyslabel ;
	private RatesChart ratesChartMinutes, ratesChartSeconds ;
	
	private AjaxEditableLabel<String> grafanaUrlLabel ;
	private String grafanaUrl = new String() ;
	private DropDownChoice<String> partitions ;
	private java.time.Instant last_update = java.time.Instant.now() ;
	private ExecutorService executor = Executors.newFixedThreadPool(1);

	/** holder of all instances of HomePage - used in application exit handler to deallocate all RM resources */
	// static Set<HomePage> instances = java.util.Collections.newSetFromMap(new java.util.WeakHashMap<HomePage, Boolean>());
	static List<java.lang.ref.WeakReference<HomePage>> instances = new java.util.ArrayList<java.lang.ref.WeakReference<HomePage>>();

	static { 
		try { 
			pmgclient = new pmgClient.PmgClient() ;
		} catch ( final pmgClient.ConfigurationException ex ) {
			ers.Logger.error(ex) ; 
			System.exit(1) ;
		}

		/* attempt to protect from filesystem issues with rt.jar:
		Exception in thread "Thread-65" /sw/atlas/sw/lcg/releases/java/8u222-884d8/x86_64-centos7-gcc8-opt/jre/lib/rt.jar: error reading zip file

		Exception: java.lang.NoClassDefFoundError thrown from the UncaughtExceptionHandler in thread "Thread-65"
		SEVERE rid: 6 opname: notify invocation: throwable was thrown.
		java.lang.NoClassDefFoundError: java/lang/UnsatisfiedLinkError
				at java.lang.ClassLoader.loadLibrary(ClassLoader.java:1867)
		*/
		Thread.setDefaultUncaughtExceptionHandler( new Thread.UncaughtExceptionHandler() {
			public void uncaughtException(Thread t, Throwable e) {
				if ( e instanceof NoClassDefFoundError ) {
					System.err.println("Got NoClassDefFoundError, exiting application: " + e.getMessage()) ;
					System.exit(1) ; // and be restarted by systemd
				}
			}
		} ) ;

	}

	/** called from WebRCApplication.onDestroy() - we need to deallocate all external resources, like if we close centrally all Igui*/
	static void exitCleanup() {
		ers.Logger.log("Exit cleanup for " + instances.size() + " Home pages") ;
		for ( java.lang.ref.WeakReference<HomePage> p: instances ) {
			if ( p.get() != null ) ((HomePage)p.get()).clearRmResource() ;
		}
	}

	public HomePage(final PageParameters parameters) {
		super(parameters) ;
		ers.Logger.info("Empty Constructing HomePage") ;
	}

	public Component getDbSelector() {
		return partitions ;
	}

	public void ConstructHomePage() {
		ers.Logger.info("Real Constructing HomePage") ;

		add(new Label("windowtitle", "TDAQ Web RC: " + Configuration.get("tdaq.release.version"))) ;
	
		urlHost = getRequestHost() ;
		requestUrl = getRequestUrl() ;
		// just for a test 
		// requestUrl = "http://pc-atlas-www.cern.ch" ;
		ers.Logger.info("request URL: " + urlHost);
		setVersioned(false) ;

		//add(new Label("version", getApplication().getFrameworkSettings().getVersion()));
		add(new Label("loggeduser", null)) ;

		add(new Link<Void>("logoutlink") {
			private static final long serialVersionUID = 145645645L;

			@Override
			public void onClick() {
			   AuthenticatedWebSession.get().invalidate();
			   setResponsePage(getApplication().getHomePage());
			}
		 });

		modeLabel = new Label("mode", () -> mode.toString() ) ;
		modeLabel.setOutputMarkupId(true);
		add(modeLabel) ;

		userControlLabel = new Label("usercontrolowner", () -> getUserControlOwner() ) ;
		userControlLabel.add(new AjaxSelfUpdatingTimerBehavior(Duration.milliseconds(10000)));
		userControlLabel.setOutputMarkupId(true);
		add(userControlLabel) ;

		ers.Logger.debug(0, "User control label added");
		// Clock clock = new Clock("clock", ZoneId.of("Europe/Zurich"));
        // add(clock);
		// clock.add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(2)));
		
		//final Label labeltime = new Label("timeStamp", () -> java.time.LocalDate.now()) ;
		//labeltime.add(new AjaxSelfUpdatingTimerBehavior(Duration.milliseconds(1000)));
		//add(labeltime) ;

		addPartitionChoice() ;
		ers.Logger.debug(0, "Partitions choice added");

		addDatabaseChoice() ;
		ers.Logger.debug(0, "DB choice added");

		partitionControlModalWindow = new ModalWindow("partitionControlModalWindow");
		partitionControlModalWindow.setTitle("Control partition infrastructure");
        add(partitionControlModalWindow) ;

		ersProvider = new ERSProvider(ersFilter) ; // no subscription
		rctreeprov = new RCTreeNodeProvider(this) ;
		
		addRCCommandPanel() ;
		ers.Logger.log("RCCommand panel added");

		addRCTree() ;
		ers.Logger.log("RCTree added");

		addRCButtons() ;
		ers.Logger.log("RCButtons added");

		addRatesPlot() ;
		ers.Logger.log("RatesPlot added");

		addPBeastFrame() ;

		addERSTable() ;
		add(new Label("sessioncount", () -> getSessionCount() ).add(new AjaxSelfUpdatingTimerBehavior(Duration.ONE_MINUTE))) ;

		add(new Label("machinemode", () -> getISData.getMachineMode() ).add(AttributeModifier.replace("title",
			() -> getISData.getLCHPage1().replace("<br>", "&amp;#013;&amp;#010;") )));
			 // .replace("<br>", "&#013;&#010;") - does not really work :(
		add(new Label("beammode", () -> getISData.getBeamMode() ).add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(15)))) ;
		add(new Label("stablebeam", () -> getISData.getStableBeam() ).add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(15)))) ;
		add(new Label("ready4physics", () -> getISData.getReady4Physics() ).add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(15)))) ;
		add(new Label("atlasdcs", () -> getISData.getATLASReady(10) ).add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(10)))) ;

		add(new org.apache.wicket.ajax.AjaxNewWindowNotifyingBehavior() {

			private static final long serialVersionUID = 4564564564L;

			@Override
			protected void onNewWindow(AjaxRequestTarget target) {
				System.err.println("New window for the same session requested, creating new Page instance");
				target.appendJavaScript(";alert('You opened a new browser tab to share the same WebRC session: this is UNSUPPORTED.\\n" +
				"Please close this tab and start a new Private browser window.');");

				// setResponsePage(getSignIn().getClass());
			}

		}) ;
		
		registerHttpSession() ;

		instances.add(new java.lang.ref.WeakReference<HomePage>(this));
		
		// create callback in JS in header of the page, so we can call it from JS when browser page closes
		add(new AbstractDefaultAjaxBehavior() {
            private static final long serialVersionUID = 3544564561L;
            protected void respond(final AjaxRequestTarget target) {
                ers.Logger.log("Browser page closes, need to clean up some RM resources") ;
				clearRmResource() ;
            }

            @Override
            public void renderHead(Component component, IHeaderResponse response){
                super.renderHead(component, response);
                String callbackUrl = getCallbackUrl().toString();
                response.render(JavaScriptHeaderItem.forScript("closure_callback = '" + callbackUrl + "';", "closure_callback"));
            }
        });

		// the way to detect closure of a broswer page hosting the HomePage
		// will stop firing after broser page is closed (but will still remain active if page is in background)
		add(new AbstractAjaxTimerBehavior(Duration.seconds(30)) {
			@Override
			protected void onTimer(AjaxRequestTarget target) {
				synchronized (HomePage.this) {
					last_update = java.time.Instant.now() ; // getEpochSecond()
					ers.Logger.debug(2, "Browser page last update: " + last_update) ;
				}
			}
		}) ;
		
		// check periodically if the Ajax timestamp from HomePage is updating and if not, clear RM token
		executor.execute(new Runnable(){
			public void run() {
				while(true) {
					long ts ;
					synchronized (HomePage.this) {
						ts = last_update.getEpochSecond() ;
					}
					
					if ( java.time.Instant.now().getEpochSecond() - ts > 120 ) {
						ers.Logger.log("Page seems to be closed, no update in 120 seconds") ;
						clearRmResource() ; // clear RM resource asap, the HomePage will be removed along with the session
						return ;
					};
					
					try {
						Thread.currentThread().sleep(30000) ;
					} catch ( final java.lang.InterruptedException ex ) {}
				}
			}
		});

		ers.Logger.log("HomePage constructed");
	}

	private void registerHttpSession() {
        final Request request = getRequest();
        if ( request instanceof WebRequest ) {
            final WebRequest wr = (WebRequest) request;
			javax.servlet.http.HttpServletRequest hsr = null ;
			Object ttt = wr.getContainerRequest();
			if ( ttt instanceof javax.servlet.http.HttpServletRequest ) {
				hsr = (javax.servlet.http.HttpServletRequest)ttt ;
				final String sessionId = hsr.getSession().getId() ;
				ers.Logger.debug(0, "This HomePage created for session " + sessionId) ;
				allPages.put(sessionId, this) ;
				addSession() ; 
			}	
		}
	}

	// query PMG for the owner of RootController
	public static String getPartitionOwner(final String partition)
	throws pmgClient.PmgClientException, is.InfoNotFoundException, is.RepositoryNotFoundException {
		final String root_name = partition.equals("initial") ? default_rc_app_name : rc_app_name ;
		final rc.DAQApplicationInfo root = ISReciever.getDAQAppInfo( partition, root_name) ;
		final pmgClient.Handle handle = pmgclient.lookup(root_name, partition, root.host) ;
		if ( handle == null ) { 
			ers.Logger.debug(1, new ers.Issue("Failed to lookup " + root_name + " on host " + root.host + " in parition " + partition) );
			return "" ; 
		} ;
		final pmgpub.ProcessStatusInfo pinfo = pmgclient.getProcess(handle).getProcessStatusInfo() ;
		return pinfo.start_info.user.name ;
	}

	private String getUserControlOwner() {
		if ( currentPartition.equals("") ) {
			return "nobody" ;
		} 
		if ( mode == Mode.CONTROL ) { // no need to contact to RM, as it is the owner who requested that resource
			return partitionOwner ;
		}
		try {
			daq.rmgr.RMClientImpl rm_client = new daq.rmgr.RMClientImpl() ;
			daq.rmgr.AllocatedResource[] rss = rm_client.getResourceInfo(currentPartition, "UserControl") ;
			ers.Logger.debug(1, "Got UserControl resource for " + currentPartition) ;
			
			if ( rss.length > 0 ) {
				return rss[0].client ;
			}
		} catch ( final Exception ex) {
			ers.Logger.error(ex) ;
		}
		return "nobody" ;
	}

	private void clearRmResource() {
		if ( rmControlHandle != 0 && mode == Mode.CONTROL ) {
			ers.Logger.log("Clearing RM User Control resource in partition " + currentPartition + " with handle " + rmControlHandle) ;
			final daq.rmgr.RMClientImpl rmClient = new daq.rmgr.RMClientImpl() ;
				try {
					rmClient.freeResources(rmControlHandle) ;
					rmControlHandle = 0 ;
				} catch ( final Exception ex ) {
					ers.Logger.error(ex) ;
				}
		}
	}

	public void PartitionConnected(final String _partition, AjaxRequestTarget target) {
		PartitionChanged(_partition, target) ;
		// partitionsChoice.setModelObject(_partition) ;
		partitionsChoice.setSelectedPartition(_partition) ;
		target.add(partitionsChoice) ;
	}
 
	public void PartitionChanged(final String _partition, AjaxRequestTarget target) {
		// final String oldPartition = currentPartition ;

		final int pos = _partition.indexOf(" [") ;
		final String newPartition = (pos == -1 ? _partition : _partition.substring(0, pos)) ;
		if ( newPartition.equals(currentPartition) ) {
			ers.Logger.info("Partition selection not changed. Current partition: " + currentPartition ) ;
			return ;
		}

		clearRmResource() ; // we "closed IGUI" which had UserControl RM resource

		// currentSubscription = "sev=ERROR" ;
		currentPartition = newPartition ;
		ersSubscrLabel.setModelObject(default_subscription) ;
		ers.Logger.info("Partition changed. Current partition/subscription: " + currentPartition + "/" + default_subscription) ;
		try {
			ersProvider.subscribe(currentPartition, default_subscription) ;
			ers.Logger.info("Subscribed in ERS");
			// ersProvider.clear_data() ;
			} 
		catch ( final Exception ex ) {
			// ex.printStackTrace() ;
			target.appendJavaScript(";alert('Failed to subscribe to MTS in partition " + currentPartition + ": " + ex + ".');");
			ers.Logger.error(ex) ;	
			//System.err.println(ex.getMessage());
			partitionsChoice.clearInput() ;
			target.add(partitionsChoice) ;
			//currentPartition = ex.getMessage() ;
			return ;
		}
		
		try { // reload configuration and the tree
			ConfigProvider.loadDB(currentPartition) ;
			// ConfigProvider.unsubscribe(oldPartition) ; not needed, as partition Config is not unloaded and may be needed later
			ers.Logger.info("RC tree is to be reloaded for partition " + currentPartition);
			rctreeprov.partitionChanged(currentPartition) ;
			selectedNodeName = "" ;
			disableComponent(rcbuttonsPanel, target) ;
			disableComponent(applogsPanel, target) ;
			disableComponent(ratesPanel, target) ;
			disableComponent(runparamsPanel, target) ;
			disableComponent(pbeastIframe, target) ;
			disableComponent(grafanaUrlPanel, target) ;
		} catch ( final Exception ex ) {
			currentPartition = ex.getMessage() ;
			ex.printStackTrace() ; 
			target.appendJavaScript(";alert('failed to read configuration from RDB. Check and fix (restart?) your partition infrastructure.');");
			partitionsChoice.clearInput() ;
			target.add(partitionsChoice) ;
			ersProvider.partitionGone(currentPartition) ;
			return ;
		}

		// expand first level
		Iterator<RCNode> it = rctreeprov.getRoots() ;
		while ( it.hasNext() ) {
			tree.expand(it.next());
		}
		
		ratesChartSeconds.partitionChanged(currentPartition) ;
		ratesChartMinutes.partitionChanged(currentPartition) ;

		rc.RCStateInfo root = ISReciever.getRCInfo( currentPartition, currentPartition.equals("initial") ? default_rc_app_name : rc_app_name) ;
		globalRCStateChanged(root.state, root.busy);
		
		/*
		try { 
			subscribeRootExit(currentPartition) ;
		} catch ( final Exception ex ) {
			ex.printStackTrace() ;
		} */

		if ( Configuration.get("readonly").equals("true") ) {
			mode = Mode.DISPLAY ;
			modeLabel.add(new AttributeModifier("mode", Model.of("display"))) ;
		} else { // possibly get control over this partition
			enableControlMode(target) ;
		}

		globalStateChanged.set(true) ;
		updateRCButtons(target) ;
		target.add(tree, userControlLabel, ersSubscrLabel, modeLabel, firstCommandButton, secondCommandButton,
				thirdCommandButton, rnlabel) ;
	}

	/**
	 * Checks the partition owner, i.e. the user ID running the RootController and if it matches the authenticated session user (i.e. SSO user name),
	 * enables CONTROL mode allowing sending commands to the RootConrtoller of the partition.
	 * @param target
	 */
	public void enableControlMode(final AjaxRequestTarget target) {
		
		((SignInSession)AuthenticatedWebSession.get()).getTdaqToken() ;

 		try {
			partitionOwner = "" ;
			partitionOwner = getPartitionOwner(currentPartition) ;
		} catch ( final is.InfoNotFoundException ex ) {
			target.appendJavaScript(";alert('Failed to get RootController PMG info, please re-try in few seconds if you need CONTROL mode');") ;
		} catch ( final is.RepositoryNotFoundException ex ) {
			target.appendJavaScript(";alert('RepositoryNotFoundException: some severe problems with basic partition infrastructure');");
			return ;
		} catch ( final pmgClient.PmgClientException ex ) {
			target.appendJavaScript(";alert('PmgClient Exception: failed to get RootController process info from pmg:\\n"
			+ ex.toString() + "\\nPlease re-try if you need CONTROL mode.');");
		} catch ( final Exception ex ) {
			ex.printStackTrace();
			target.appendJavaScript(";alert('Failed to get process info from pmg:\\n" + ex.getMessage() + "\\nPlease re-try.');");
		}

		// enable Conrtol mode if a) user is the owner of the RootController b) user is member of group control.mode.group.required (e.g. cc-members or tbed-users)
		if ( partitionOwner.equals(((SignInSession)AuthenticatedWebSession.get()).getUser())
				&& 
			 	((SignInSession)AuthenticatedWebSession.get()).getGroups().contains(Configuration.get("control.mode.group.required"))
			  )
			{
			ers.Logger.info("Currently authenticated user is the one who is running partition: " + partitionOwner) ;
			try {
				String localHostName = InetAddress.getLocalHost().getCanonicalHostName() ; // host name of the webrc
				final RMClientImpl rmClient = new RMClientImpl() ;
				
				WebClientInfo wci = WebSession.get().getClientInfo() ; //  does not really work...
				if ( wci != null && wci.getProperties().getHostname() !=null  ) {
					localHostName = wci.getProperties().getHostname() ; // get host name of the web user
				}
				rmControlHandle = rmClient.requestResource(currentPartition, "UserControl", localHostName, partitionOwner) ;
				if ( rmControlHandle >= 0 ) { // success 
					ers.Logger.info("CONTROL mode enabled for user " + partitionOwner) ;
					mode = Mode.CONTROL ;
					modeLabel.add(new AttributeModifier("mode", Model.of("control"))) ;
					return ;
				} else {
					ers.Logger.info("Failed to allocate CONTROL mode resource for user " + partitionOwner) ;
					target.appendJavaScript(";alert('Failed to allocate UserControl resource in RM, it is already taken');") ;
				}
			} catch ( final daq.rmgr.UnavailableResources ex ) {
				target.appendJavaScript(";alert('Failed to allocate UserControl resource in RM: it is already allocated by " + getUserControlOwner() + ". ');") ;
			} catch ( final Exception ex ) {
				ex.printStackTrace() ;
				target.appendJavaScript(";alert('Failed to allocate UserControl resource in RM: + " + ex.getMessage() + " ');") ;
			}
			ers.Logger.info("Failed to allocate CONTROL mode resource for user " + ((SignInSession)AuthenticatedWebSession.get()).getUser()) ;
		}

		ers.Logger.info("CONTROL mode disabled for user " + ((SignInSession)AuthenticatedWebSession.get()).getUser()) ;
		mode = Mode.DISPLAY ;
		modeLabel.add(new AttributeModifier("mode", Model.of("display"))) ;
	}

	void addPartitionChoice() {
		partitionsChoice = new PartitionsChoice("partitions", this) ;

		partitionsChoice.setOutputMarkupId(true);
		// partitionsChoice.setNullValid(false) ;
				
		partitionsChoice.add(new AjaxFormComponentUpdatingBehavior("change") {	
			private static final long serialVersionUID = 1456345645L;
			
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				System.err.println("Ajax change onUpdate called, updating partitions to " + partitionsChoice.getSelectedPartition() );
				if ( partitionsChoice.getSelectedPartition() == null ) return ;
				PartitionChanged(partitionsChoice.getSelectedPartition(), target) ;
				partitionsChoice.partitionGone("") ;
				target.add(modeLabel, tree, inOutRCButton, selectedNodeLabel, ignoreRCButton, firstCommandButton, secondCommandButton, thirdCommandButton) ;
			}
		});

		add(partitionsChoice) ;
	}

	void addDatabaseChoice() {
		ers.Logger.log("Getting tdaq token for getting the groups");	
		((SignInSession)AuthenticatedWebSession.get()).getTdaqToken() ;

		IModel<List<String>> partitionChoices = () -> Partitions.getDBPartitions() ;		
		partitions = new DropDownChoice<String>("databases", new Model<String>(), partitionChoices) {
							private static final long serialVersionUID = 1546467L;
							@Override
							protected String getNullKeyDisplayValue() {
									return "Select partition DB" ;
								}
	
							protected CharSequence getDefaultChoice(String selectedValue) {
								CharSequence ret = super.getDefaultChoice(selectedValue) ;
								if ( ret.length() == 0 ) return "<option selected=\"selected\" value=\"\">Select partition DB</option>" ;
								else return ret ;

							}
						};

		partitions.setOutputMarkupId(true);

		// partitions.setNullValid(false) ;
		partitions.add(new AjaxSelfUpdatingTimerBehavior(Duration.milliseconds(60000)));
		partitions.add(new AjaxFormComponentUpdatingBehavior("change") {	
			private static final long serialVersionUID = 1456345645L;
			
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				ers.Logger.log("Selected partition DB: " + partitions.getModel().getObject() );
				if ( partitions.getModel().getObject() == null ) return ;
				PartitionControl.startPartition(target, partitionControlModalWindow, partitions.getModel().getObject(), HomePage.this) ;
				partitions.clearInput();
				target.add(partitions, modeLabel, tree, inOutRCButton, selectedNodeLabel, ignoreRCButton, firstCommandButton, secondCommandButton, thirdCommandButton) ;
			}
		});

	add(partitions) ;

	if ( !Configuration.get("experimental").equals("true")
		|| !((SignInSession)AuthenticatedWebSession.get()).getGroups().contains(Configuration.get("expert.mode.group.required")) ) {
		ers.Logger.log("Expert mode not enabled for user " + ((SignInSession)AuthenticatedWebSession.get()).getUser() ) ;	
		disableComponent(get("databases"), null) ;
		return ;
	}

	ers.Logger.log("Expert mode enabled for user " + ((SignInSession)AuthenticatedWebSession.get()).getUser() );		
	} // addDatabases


	// main partiton RC commands sent to RootController
	public void addRCCommandPanel() {
		// SHUTDOWN
		firstCommandButton = new AjaxLink<String>("rcfirstbutton") {
			private static final long serialVersionUID = 134563456435L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				System.err.println("SHUTDOWN Button clicked, command to send: " + firstCommandButtonLabel) ;
				sendCommand("SHUTDOWN", target);
			}
		} ;
		firstCommandButton.add(new Label("rcfirstbuttonlabel",() -> firstCommandButtonLabel)) ;

		// INIT, CONFIGURE, START, STOP, UNCONFIGURE
		secondCommandButton = new AjaxLink<String>("rcsecondbutton") {
			@Override
			public void onClick(AjaxRequestTarget target){
				System.err.println("RC Command Button clicked, command to send to Root Controller: " + secondCommandButtonLabel) ;
				sendCommand(secondCommandButtonLabel, target);
			}
		} ;
		secondCommandButton.add(new Label("rcsecondbuttonlabel",() -> secondCommandButtonLabel)) ;

		// HOLD/RESUME TRG
		thirdCommandButton = new AjaxLink<String>("rcthirdbutton") {
			@Override
			public void onClick(AjaxRequestTarget target){
				System.err.println("RC Command Button clicked, command to send to Root Controller: " + thirdCommandButtonLabel) ;
				sendCommand(thirdCommandButtonLabel, target);
			}
		} ;
		thirdCommandButton.add(new Label("rcthirdbuttonlabel",() -> thirdCommandButtonLabel)) ;

		// COMMIT AND RELOAD DB
		reloadButton = new AjaxLink<String>("rcreloadbutton") {
			@Override
			public void onClick(AjaxRequestTarget target){
				System.err.println("Reload DB Button clicked") ;
				sendCommand("RELOAD_DB", target);
			}
		} ;
		
		firstCommandButton.setOutputMarkupId(true);
		secondCommandButton.setOutputMarkupId(true);
		thirdCommandButton.setOutputMarkupId(true);
		reloadButton.setOutputMarkupId(true);
		firstCommandButton.setOutputMarkupPlaceholderTag(true) ;
		secondCommandButton.setOutputMarkupPlaceholderTag(true) ;
		thirdCommandButton.setOutputMarkupPlaceholderTag(true) ;
		reloadButton.setOutputMarkupPlaceholderTag(true) ;

		firstCommandButton.setVisible(false) ;
		secondCommandButton.setVisible(false) ;
		thirdCommandButton.setVisible(false) ;
		reloadButton.setVisible(false) ;
	
		add(firstCommandButton) ;
		add(thirdCommandButton) ;
		add(secondCommandButton) ;
		add(reloadButton) ;

		reloadDbModalWindow = new ModalWindow("reloadDbModalWindow");
		reloadDbModalWindow.setTitle("Reload database");
		add(reloadDbModalWindow) ;
	}

	void sendCommand(final String commandname, AjaxRequestTarget target) {
		rcPannelError = "" ;
		try {
			Command.FSMCommands command = Command.FSMCommands.NEXT_TRANSITION ; 
			if ( commandname.equals("SHUTDOWN") ) {
				command = Command.FSMCommands.SHUTDOWN ;
			} else
			if ( commandname.equals("INITIALIZE") ) {
				command = Command.FSMCommands.INITIALIZE ;
			} else
			if ( commandname.equals("CONFIGURE") ) {
				command = Command.FSMCommands.CONFIG ;
			} else
			if ( commandname.equals("START") ) {
				command = Command.FSMCommands.START ;
			} else
			if ( commandname.equals("STOP") ) {
				command = Command.FSMCommands.STOP ;
			} else
			if ( commandname.equals("UNCONFIG") ) {
				command = Command.FSMCommands.UNCONFIG ;
			} else 
			if ( commandname.equals("HOLD TRG") ) {
				ers.Logger.log("Senging " + commandname) ;
				boolean res = RCCommandSender.HoldTrigger(currentPartition) ;
				if ( res ) {
					triggerOnHold = true ;
				}
				return ;
			} else 
			if ( commandname.equals("RESUME TRG") ) {
				ers.Logger.log("Senging " + commandname) ;
				boolean res = RCCommandSender.ResumeTrigger(currentPartition) ;
				if ( res ) {
					triggerOnHold = false ;
				} 
				return ;
			} else 
			if ( commandname.equals("RELOAD_DB") ) {
				try {
					ConfigProvider.reloadDb(currentPartition, target, reloadDbModalWindow, this) ; // long, throw
					selectedNodeName = "" ;
					disableComponent(rcbuttonsPanel, target) ;
				} catch ( final Exception ex ) {
					ex.printStackTrace(); 
					rcPannelError = ex.getMessage() ;
					try {
						target.add(rcPannelErrorLabel) ;
					} catch ( final Exception ex2 ) {
						ex2.printStackTrace(); 	
					}
					return ;
				}
				return ;
			} else {
				return ;
			}
			RCCommandSender.sendRootTransitionCommand(currentPartition, command) ; // throw
		} catch ( final Exception ex ) {
			rcPannelError = ex.getMessage() ;
			target.add(rcPannelErrorLabel) ;
			ex.printStackTrace(); 
		}
	}

	public void addRCTree() {
		tree = new DefaultNestedTree<RCNode>("rctree", rctreeprov) {

			private static final long serialVersionUID = 4564564561L;
			
			@Override
			public void updateNode(RCNode node, IPartialPageRequestHandler target) {
				super.updateNode(node, target);
				System.err.println("Update node called : " + node.getName());
			}

			@Override
			protected Component newContentComponent(String id, IModel<RCNode> node) {
				
				AbstractLink ret = new AjaxLink<RCNode>(id, node) {
					
					private static final long serialVersionUID = 16574567567L;
					private transient StringBuffer body = null ;

					@Override
					protected void onConfigure() {
						super.onConfigure();
					}
					
					@Override
					public void onComponentTagBody(org.apache.wicket.markup.MarkupStream markupStream, org.apache.wicket.markup.ComponentTag openTag) {
						String busy_inst = node.getObject().getCTPBusy(CTPBusyInfo.BUSY_TYPE.INST) ;
						final String busy_run = node.getObject().getCTPBusy(CTPBusyInfo.BUSY_TYPE.RUN) ;
						final String name = node.getObject().getName() ;
						if ( name.equals("HLT") ) {
							body = new StringBuffer(getDefaultModelObjectAsString());
							body.append("<span class=\"farmavailability\" title=\"Farm availability (%) N of available/free/total PUs\">") ;
						} else
						if ( name.startsWith("HLT-") ) {
							body = new StringBuffer(getDefaultModelObjectAsString());
							body.append("<span class=\"farmavailability\" title=\"Rack occupancy: N of events inside/in output\">") ;
						} else
						if ( name.equals("HLTSV") ) {
							body = new StringBuffer(getDefaultModelObjectAsString());
							body.append("<span class=\"roibbusy\" title=\"ROIB Xoff (%, max over all channels)\">")
							.append(getISData.getROIBBusy(currentPartition)).append("</span>") ;
							body.append("<span class=\"farmavailability\" title=\"HTLSV occupancy: % of used RobinNP pages (max over all channels)\">") ;
						} else
						if ( name.startsWith("SFO-") ) {
							body = new StringBuffer(getDefaultModelObjectAsString());
							body.append("<span class=\"sfooccupancy\" title=\"SFO occupancy: N of events expected/inside\">") ;
						} else {
							if ( !busy_inst.equals("0.00") ) {
								body = new StringBuffer(getDefaultModelObjectAsString());
								if ( node.getObject().isRoot(name) ) {
									if ( busy_inst.startsWith("ON HOLD") && node.getObject().getRCState().equals("RUNNING") ) {
										final String full_busy = busy_inst ;
										if ( busy_inst.length() > 20 ) { // too long to fit into one line in RC tree leaf
											busy_inst = busy_inst.substring(0, 20) + "..." ;
										}
										body.append("<span class=\"ctpbusy-onhold\" title=\" " + full_busy + "\">") ;
									} else {
										body.append("<span class=\"ctpbusy\" title=\"CTP total busy (Simple/Complex deadtime)\">") ;
									}
								} else {
									body.append("<span class=\"subdetbusy\" title=\"Subdetector inst busy %\">") ;
								}
							}
						} // prepare span for inst busy
						if ( body != null ) {
								body.append(busy_inst).append("</span>") ; // fill span
						}

						if ( !busy_run.equals("0.00") ) { // another span if busy_run is available for this element
							if ( body == null ) {
								body = new StringBuffer(getDefaultModelObjectAsString()) ;
							}
							body.append("<span class=\"subdetbusyrun\" title=\"Accumulated run busy %: " + busy_run + "\"" + 
										" style=\"width: " + busy_run + "%;\" >&nbsp;</span>") ;
						}				
						
						if ( body != null ) {
							replaceComponentTagBody(markupStream, openTag, body) ;
						}
						else {
							super.onComponentTagBody(markupStream, openTag) ;
						}
					}

					@Override
					public void onClick(AjaxRequestTarget target) {
						if ( node.getObject() != null ) {
							disableComponent(rcbuttonsPanel, target) ;
							disableComponent(applogsPanel, target) ;
							rcPannelError = "" ;
							target.add(rcPannelErrorLabel, selectedNodeLabel, tree) ;

							final String oldSelectedName = selectedNodeName ;
							selectedNodeName = node.getObject().getName() ;
							ers.Logger.debug(0, "Selected node " + selectedNodeName) ;
							if ( selectedNodeName.equals(oldSelectedName) ) {
								selectedNodeName = "" ;
								rctreeprov.setSelectedNode(null);
								if ( !grafanaUrl.isEmpty()) {
									disableComponent(ratesPanel, target);
								}
								return ;
							}
							disableComponent(pbeastIframe, target);
							disableComponent(grafanaUrlPanel, target);

							rctreeprov.setSelectedNode(node.getObject());
							ersProvider.setSelectedNode(selectedNodeName);
							
							enableComponent(logsErrButton, target) ;
							enableComponent(logsOutButton, target) ;
							
							if ( globalRCState.equals("RUNNING") ) {
								enableComponent(runparamsPanel, target) ;
							} 

							grafanaUrl = getGraphanaUrl(selectedNodeName) ;
							grafanaUrlLabel.setModelObject(grafanaUrl) ;
							enableComponent(grafanaUrlPanel, target) ;
							
							if ( !grafanaUrl.isEmpty() ) {
								enableComponent(pbeastIframe, target) ;
								disableComponent(ratesPanel, target);
							}

							if ( (node.getObject().isApp() || node.getObject().isRCApp())
								 && !node.getObject().getAppState().equals("ABSENT") ) {

								enableComponent(applogsPanel, target) ;
								enableComponent(pmgInfoButton, target) ;
								// enable log buttons only if files are available
								if ( !node.getObject().getErrLogFile().isEmpty() ) {
									enableComponent(logsErrButton, target) ;
								}
								if ( !node.getObject().getOutLogFile().isEmpty() ) {
									enableComponent(logsOutButton, target) ;
								}
							}

							if ( mode == Mode.DISPLAY
								|| !(node.getObject().isSegment() || node.getObject().isApp() || node.getObject().isRCApp()) ) {
								ers.Logger.debug(0, "No command buttons enabled for node " + selectedNodeName) ;	
								return ;
							}

							enableComponent(rcbuttonsPanel, target) ;
							disableComponent(inOutRCButton, target) ;
							disableComponent(restartRCButton, target) ;
							disableComponent(killRCButton, target) ;
							disableComponent(ignoreRCButton, target) ;
							disableComponent(disableRCButton, target) ;

							if ( selectedNodeName.endsWith(rc_app_name) ) {
								if ( node.getObject().getRCError() ) {
									enableComponent(ignoreRCButton, target) ;
									enableComponent(killRCButton, target) ;
								}
								if ( globalRCState.equals("NONE") ) { // RootController in NONE state
									ers.Logger.debug(0, "Enabled EXIT button for RootConrtoller " + selectedNodeName) ;	
									killRCButtonLabel = "EXIT" ;
									enableComponent(killRCButton, target) ;
								}
								return ;
							}

							if ( node.getObject().isRCApp() ) {
								enableComponent(inOutRCButton, target) ;
								if ( node.getObject().getAppMembership() ) { 
									inOutButtonLabel = "OUT" ;
								} else {
									inOutButtonLabel = "IN" ;
								}
							}

							if ( node.getObject().getRCError() && node.getObject().isRCApp() ) {
								enableComponent(ignoreRCButton, target) ;
							}

							if ( node.getObject().isApp() && !node.getObject().getAppState().equals("ABSENT") ) {
								enableComponent(restartRCButton, target) ;
								enableComponent(killRCButton, target) ;
								killRCButtonLabel = "KILL" ;
							}

							// allow disable/enable Segments in NONE state
							if ( globalRCState.equals("NONE")) {
								if ( node.getObject().isComponent() && !selectedNodeName.endsWith(rc_app_name) ) {
									enableComponent(disableRCButton, target) ;
									if ( node.getObject().isEnabled() ) {
										//System.err.println("Node " + node.getObject().getName() + " is enabled");
										disableRCButtonLabel = "DISABLE" ;
									} else {
										//System.err.println("Node " + node.getObject().getName() + " is disabled");
										disableRCButtonLabel = "ENABLE" ;
									}
								} else { // RootController in NONE state
								}
							} else {
								disableComponent(disableRCButton, target) ;
							}
						}
					}
				} ;
				
				// fill the Label with text (node state/name)
				ret.setBody(new Model<String>(node.getObject().toString())) ;

				final StringBuffer tooltip = new StringBuffer();
				
				if ( node.getObject().isSelected() ) {
					ret.add(new AttributeAppender("class", Model.of(" selected "))) ;
				}
							
				if ( node.getObject().isRCApp() && !node.getObject().getRCState().equals("") ) {
					tooltip.append("RC STATE:\t") ;
					if ( !node.getObject().getAppMembership() ) {
						tooltip.append("OUT [").append(node.getObject().getRCState()).append("]") ;
					} else {
						tooltip.append(node.getObject().getRCState()) ;
					}
					tooltip.append("\nAPP STATUS:\t").append(node.getObject().getAppState()) ;
					tooltip.append("\nHOST:\t\t").append(node.getObject().getHost()) ;

					ret.add(new AttributeModifier("rc-controller", node.getObject().getRCState())) ;
					ret.add(new AttributeAppender("class", Model.of(" rc-controller "))) ;
					ret.add(new AttributeModifier("rc-application", node.getObject().getAppState())) ;
					//ret.add(new AttributeAppender("class", Model.of(" " +node.getObject().getRCState() + " "))) ;
					//ret.add(new AttributeAppender("class", Model.of(" " +node.getObject().getAppState() + " "))) ;
					if ( node.getObject().getRCError() )
						{ ret.add(new AttributeAppender("class", Model.of(" FAULT "))) ; }
					if ( node.getObject().getRCBusy() )
						{ ret.add(new AttributeAppender("class", Model.of(" BUSY "))) ; }
					if ( !node.getObject().getAppMembership() )
						{ ret.add(new AttributeAppender("class", Model.of(" OUT "))) ; } ;
				} else if ( !node.getObject().getAppState().equals("") ) {
					tooltip.append("APP STATUS:\t") ;
					if ( !node.getObject().getAppMembership() ) {
						tooltip.append("OUT [").append(node.getObject().getAppState()).append("]") ; ;
					} else {
						tooltip.append(node.getObject().getAppState()) ;
					}
					
					tooltip.append("\nHOST:\t\t").append(node.getObject().getHost()) ;
					ret.add(new AttributeAppender("class", Model.of(" rc-application "))) ;
					ret.add(new AttributeModifier("rc-application", node.getObject().getAppState())) ;
					
					//ret.add(new AttributeAppender("class", Model.of(" " + node.getObject().getAppState() + " "))) ;
					
					if ( node.getObject().getAppState().equals("FAILED") ) {
						tooltip.append("\nEXIT_CODE:\t").append(node.getObject().getExitCode()) ;
						tooltip.append("\nFAILURE:\t").append(node.getObject().getFailureReason()) ;
					}
					//if ( node.getObject().getAppState().startsWith("DIED") ) {
					//	ret.add(new AttributeModifier("DIED", Model.of("TRUE"))) ;	
					//}
					
					if ( !node.getObject().getAppMembership() )
						{ ret.add(new AttributeAppender("class", Model.of(" OUT "))) ; } ;
				} else {
					// System.err.println("Neither RC nor App state available for " + node.getObject().getName());
				};

				if ( !node.getObject().isEnabled() ) { // disabled component
					ret.add(new AttributeModifier("enabled", Model.of("FALSE"))) ;
				} else {
					ret.add(new AttributeModifier("enabled", Model.of("TRUE"))) ;
				}

				if ( !node.getObject().isCommitted() ) { // disabled component
					ret.add(new AttributeModifier("committed", Model.of("FALSE"))) ;
				}

				if ( node.getObject().isSegment() ) { 
					ret.add(new AttributeAppender("class", Model.of(" segment "))) ;
				}

				
				if ( !node.getObject().getRCFailReason().equals("") ) {
					tooltip.append("\nERROR:\n" +  node.getObject().getRCFailReason() );
				}
						
				if ( tooltip.length() > 0 ) {
					ret.add(AttributeModifier.replace("title", tooltip.toString() ));
				}

				return ret ;
			} ;
		};

		tree.setOutputMarkupId(true);
		
		// check if there were fresh IS updates and re-render if so
		int updateinterval = 1000 ;
		try {
			updateinterval = Integer.parseInt(Configuration.get("rctree.update.interval")) ;
		} catch ( final Exception ex ) {
			ex.printStackTrace() ;
		}

		tree.add(new AbstractAjaxTimerBehavior(Duration.milliseconds(updateinterval)) {
			@Override
			protected void onTimer(AjaxRequestTarget target) {
				//rctreeprov.updateTree(tree, target) ;
				if ( rctreeprov.checkUpdated() ) { // refresh only if there were updates in tree elements
					target.add(tree) ; 
				}
				if ( globalStateChanged.getAndSet(false) ) {
					target.add(partitionsChoice) ;

					if ( globalRCState.equals("RUNNING") ) {
						enableComponent(runparamsPanel, target);
					} 

//					if ( getGraphanaUrl(selectedNodeName).isEmpty() && globalRCState.equals("RUNNING") ) {
					if ( grafanaUrl.isEmpty() && globalRCState.equals("RUNNING") ) {
						target.add(lblabel, recordinglabel, tier0projectlabel) ;
						if ( !ratesPanel.isVisible() ) {
							ratesPanel.setVisible(true) ;
							target.add(ratesPanel) ;	
						}
					} else if ( ratesPanel.isVisible() ) {
						ratesPanel.setVisible(false) ;
						target.add(ratesPanel) ;
					}
				}
				updateRCButtons(target) ;
			}
		}) ; 

		add(tree);
	}

	// called either from TRCtree onTimer or from PartitionChanged
	public void updateRCButtons(final AjaxRequestTarget target) {
		if ( globalRCState.equals("RUNNING") ) {
			enableComponent(runparamsPanel,target);
			
//			if ( getGraphanaUrl(selectedNodeName).isEmpty() ) {
			if ( grafanaUrl.isEmpty() ) {
				enableComponent(ratesPanel,target);
			} else {
				disableComponent(ratesPanel,target);
			}
		}

		if ( mode == Mode.DISPLAY || globalRCState.equals("ABSENT") ) {
			disableComponent(firstCommandButton, target) ;
			disableComponent(secondCommandButton, target) ;
			disableComponent(thirdCommandButton, target) ;
		} else { // mode CONTROL
			firstCommandButtonLabel = "SHUTDOWN";
			enableComponent(firstCommandButton, target);
				
			if ( globalRCBusy.get() ) {
				disableComponent(secondCommandButton, target);
				disableComponent(thirdCommandButton, target);
			} else {
				enableComponent(secondCommandButton, target);
				
				if ( globalRCState.equals("NONE") ) {
					secondCommandButtonLabel = "INITIALIZE";
					disableComponent(firstCommandButton, target) ;
					disableComponent(thirdCommandButton, target) ;
					if ( ConfigProvider.dbModified(currentPartition) ) {
						setDbReloadNeeded(true) ;
					}
					if ( getDbReloadNeeded() ) {
						enableComponent(reloadButton, target) ;	
					}
				} else 
				if ( globalRCState.equals("INITIAL") ) {
					secondCommandButtonLabel = "CONFIGURE";
					disableComponent(thirdCommandButton, target) ;
				} else 
				if ( globalRCState.equals("CONNECTED") ) {
					secondCommandButtonLabel = "UNCONFIG";
					thirdCommandButtonLabel = "START";
					enableComponent(thirdCommandButton, target);

				} else 
				if ( globalRCState.equals("RUNNING") ) {
					secondCommandButtonLabel = "STOP";
					if ( triggerOnHold ) {
						thirdCommandButtonLabel = "RESUME TRG"; // TODO flashing
					} else {
						thirdCommandButtonLabel = "HOLD TRG";
					}
					enableComponent(thirdCommandButton, target);
				}
			}
		}
	}

	public void addRCButtons() {

		rcbuttonsPanel = new WebMarkupContainer ("rcbuttonspanel") ;
		rcbuttonsPanel.setOutputMarkupPlaceholderTag(true) ;
		rcbuttonsPanel.setOutputMarkupId(true) ;

		selectedNodeLabel = new Label("rcnodeselected", () -> selectedNodeName ) ;
		selectedNodeLabel.setOutputMarkupId(true) ;
		rcbuttonsPanel.add(selectedNodeLabel) ;

		inOutRCButton = new AjaxLink<String>("rcinoutbutton") {
		@Override
		public void onClick(AjaxRequestTarget target){
			if ( selectedNodeName.equals("") ) return ;
			System.err.println("IN/OUT Button clicked, command to send: " + inOutButtonLabel) ;
			boolean enable = true ;
			if ( inOutButtonLabel.equals("OUT") ) { enable = false ; }
			final String[] components = {selectedNodeName} ;
			rcPannelError = "" ;
			try {
				RCNode parent = RCNodesHolder.findNode(currentPartition, selectedNodeName).getParent() ;
				if ( parent == null ) return ;
				final daq.rc.Command command = new daq.rc.Command.ChangeStatusCmd(enable, components) ;
				RCCommandSender.sendCommand(currentPartition, parent.getName(), command) ;
				if ( enable ) {
					inOutButtonLabel = "OUT" ;
				} else {
					inOutButtonLabel = "IN" ;
				}
				target.add(inOutRCButton) ;
			} catch ( final Exception ex ) {
				rcPannelError = ex.getMessage() ;
				target.add(rcPannelErrorLabel) ;
				ex.printStackTrace(); 
			}
		}
		} ;
		inOutRCButton.setOutputMarkupId(true) ;
		inOutRCButton.setOutputMarkupPlaceholderTag(true) ;
		disableComponent(inOutRCButton, null) ;
		inOutRCButton.add(new Label("rcinoutbuttonlabel",() -> inOutButtonLabel)) ;
		rcbuttonsPanel.add(inOutRCButton) ;

		ignoreRCButton = new AjaxLink<String>("rcignorebutton") {
		@Override
		public void onClick(AjaxRequestTarget target){
			// System.err.println("Ignore Button clicked, command to send to?") ;
			if ( selectedNodeName.equals("") ) return ;
			try {
				// final daq.rc.Command command = new daq.rc.Command.IgnoreErrorCmd() ;
				RCCommandSender.sendCommand(currentPartition,
					selectedNodeName, new daq.rc.Command.IgnoreErrorCmd()) ;
			} catch ( final Exception ex ) {
				rcPannelError = ex.getMessage() ;
				target.add(rcPannelErrorLabel) ;
				ex.printStackTrace(); 
			}	
		}
		};

		ignoreRCButton.setOutputMarkupPlaceholderTag(true) ;
		disableComponent(ignoreRCButton, null) ;
		rcbuttonsPanel.add(ignoreRCButton) ;

		restartRCButton = new AjaxLink<String>("rcrestartbutton") {
			private static final long serialVersionUID = 45645645645L;

			@Override
			public void onClick(AjaxRequestTarget target){
				ers.Logger.debug(0,"Restart Button clicked on " + selectedNodeName + " in partition " + currentPartition) ;
				if ( selectedNodeName.equals("") ) return ;
				try {
					final RCNode parent = RCNodesHolder.findNode(currentPartition, selectedNodeName).getParent() ;
					if ( parent == null ) return ;
					final String[] components = {selectedNodeName} ;
					final daq.rc.Command command = new daq.rc.Command.RestartAppsCmd(components) ;
					RCCommandSender.sendCommand(currentPartition, parent.getName(), command) ;
				} catch ( final Exception ex ) {
					rcPannelError = ex.getMessage() ;
					target.add(rcPannelErrorLabel) ;
					ex.printStackTrace(); 
				}	
			}
			};
	
		
		restartRCButton.setOutputMarkupPlaceholderTag(true) ;
		disableComponent(restartRCButton, null) ;
		rcbuttonsPanel.add(restartRCButton) ;

		killRCButton = new AjaxLink<String>("rckillbutton") {
			@Override
			public void onClick(AjaxRequestTarget target){
				if (  selectedNodeName.endsWith(rc_app_name)  ) { // KILL for RootController is sending it EXIT
					try {
						ers.Logger.debug(0,"EXIT Button clicked in partition " + currentPartition) ;		
						RCCommandSender.sendCommand(currentPartition, selectedNodeName, new daq.rc.Command.ExitCmd()) ;
						disableComponent(rcbuttonsPanel, null) ;
						disableComponent(applogsPanel, null) ;
						target.add(rcbuttonsPanel, applogsPanel) ;
					} catch ( final Exception ex ) {
						rcPannelError = ex.getMessage() ;
						target.add(rcPannelErrorLabel) ;
						ex.printStackTrace(); 
					}	
				}
				if ( selectedNodeName.equals("") ) return ;
				try {
					RCNode parent = RCNodesHolder.findNode(currentPartition, selectedNodeName).getParent() ;
					if ( parent == null ) return ;
					final String[] components = {selectedNodeName} ;
					final daq.rc.Command command = new daq.rc.Command.StopAppsCmd(components) ;
					RCCommandSender.sendCommand(currentPartition, parent.getName(), command) ;
				} catch ( final Exception ex ) {
					rcPannelError = ex.getMessage() ;
					target.add(rcPannelErrorLabel) ;
					ex.printStackTrace(); 
				}	
			}
			};
	
		killRCButton.setOutputMarkupPlaceholderTag(true) ;
		killRCButton.add(new Label("rckillbuttonlabel",() -> killRCButtonLabel)) ;
		
		disableComponent(killRCButton, null) ;
		rcbuttonsPanel.add(killRCButton) ;

		disableRCButton = new AjaxLink<String>("rcdisablebutton") {
			@Override
			public void onClick(AjaxRequestTarget target){
				ers.Logger.log("Disable/Enable resource clicked on selected " + selectedNodeName) ;
				if ( selectedNodeName.equals("") ) return ;
				try {
					RCNode component = RCNodesHolder.findNode(currentPartition, selectedNodeName) ;
					if ( component == null ) return ; // not a Resource
					dal.Segment segment = ConfigProvider.getSegment(currentPartition, component.getName()) ;
					disableSegment(segment, disableRCButtonLabel) ; // throw
					component.setCommitted(false) ;
					setDbReloadNeeded(true) ;
					enableComponent(reloadButton, target);
					if ( disableRCButtonLabel.equals("ENABLE") ) {
						disableRCButtonLabel = "DISABLE" ;
					} else {
						disableRCButtonLabel = "ENABLE" ;
					}
					target.add(tree, disableRCButton) ;
				} catch ( final Exception ex ) {
					rcPannelError = ex.getMessage() ;
					target.add(rcPannelErrorLabel) ;
					ex.printStackTrace(); 
				}	
			}
			};
			
		disableRCButton.add(new Label("rcdisablebuttonlabel",() -> disableRCButtonLabel)) ;	
		disableRCButton.setOutputMarkupPlaceholderTag(true) ;
		disableComponent(disableRCButton, null) ;
		rcbuttonsPanel.add(disableRCButton) ;
		
		add(rcbuttonsPanel) ;

		applogsPanel = new WebMarkupContainer ("applogspanel") ;
		applogsPanel.setOutputMarkupPlaceholderTag(true) ;
		applogsPanel.setOutputMarkupId(true) ;

		// link to log files
		logsOutButton = new ExternalLink("logsoutbutton", () -> getSelectedNodeOutLogURL(), Model.of("Std Out")) ;
		logsErrButton = new ExternalLink("logserrbutton", () -> getSelectedNodeErrLogURL(), Model.of("Std Err")) ;
		pmgInfoButton = new ExternalLink("pmginfobutton", () -> getSelectedNodePmgInfoURL(), Model.of("PMG Info")) ;
		logsOutButton.add(new AttributeModifier("enabled", Model.of("FALSE")))  ;
		logsErrButton.add(new AttributeModifier("enabled", Model.of("FALSE")))  ;
		pmgInfoButton.add(new AttributeModifier("enabled", Model.of("FALSE")))  ;
		logsOutButton.setOutputMarkupId(true) ;
		logsErrButton.setOutputMarkupId(true) ;
		pmgInfoButton.setOutputMarkupId(true) ;

		rcPannelErrorLabel = new Label("rcpanelerror", () -> rcPannelError) ;
		rcPannelErrorLabel.setOutputMarkupId(true) ;
		
		applogsPanel.add(logsOutButton, logsErrButton, pmgInfoButton, rcPannelErrorLabel) ;
		add(applogsPanel) ;

		disableComponent(applogsPanel, null) ;
		disableComponent(rcbuttonsPanel, null) ;
			
		} // addRCButtons 

	public String getSelectedNodeOutLogURL() {
		final String base = requestUrl + Configuration.get("partlogs.url.path") ;
		if ( selectedNodeName.equals("") ) return base ;
		RCNode selected = RCNodesHolder.findNode(currentPartition, selectedNodeName) ;
		if ( selected != null ) {
			ers.Logger.debug(1,"Selected node: " + selected.getName() + " out log file: " + selected.getErrLogFile()) ;
			return base + selected.getHost() + "/" + currentPartition + "/" +
					selected.getOutLogFile() + "?head=100&tail=500";
		}
		return base ;
	}

	public String getSelectedNodeErrLogURL() {
		final String base = requestUrl + Configuration.get("partlogs.url.path") ;
		if ( selectedNodeName.equals("") ) return base ;
		RCNode selected = RCNodesHolder.findNode(currentPartition, selectedNodeName) ;
		if ( selected != null ) {
			ers.Logger.debug(1,"Selected node: " + selected.getName() + " err log file: " + selected.getErrLogFile()) ;
			return base + selected.getHost() + "/"  + currentPartition + "/" +
					selected.getErrLogFile() + "?head=100&tail=500";
		}
		return base ;
	}

	// info/current/<partition>/is/PMG/PMG.<host>|<app>
	public String getSelectedNodePmgInfoURL() {
		final String base = requestUrl + Configuration.get("pmginfo.url.path") + currentPartition ;
		if ( selectedNodeName.equals("") ) return base ;
		RCNode selected = RCNodesHolder.findNode(currentPartition, selectedNodeName) ;
		if ( selected != null && selected.isApp() ) {
			if ( selected.getHost().isEmpty() ) {
				System.err.println("Empty Host for node " + selectedNodeName) ;
				return base ;
			}
			return base + "/is/PMG/PMG." + selected.getHost() + "|" + selected.getName();
		} else {
			System.err.println("Node not found: " + selectedNodeName) ;
		}

		return base ;
	}

	@Override
	public Mode getMode() {
		return mode ;
	}

	@Override
	public synchronized String getGlobalRCState() {
		return globalRCState ;
	}
	
	// called from ISReceiver (via RCNodesHolder) when RootController state changes
	@Override
	public synchronized void globalRCStateChanged(final String rcstate, boolean busy) {
	
		if ( !globalRCState.equals(rcstate) || busy != globalRCBusy.get() ) {
			globalRCState = rcstate ;
			globalRCBusy.set(busy); ;
			globalStateChanged.set(true) ;
		}
	}

	public static int getSessionCount() {
		return sessionCounter.get() ;
	}
	
	public static void addSession() {
		sessionCounter.incrementAndGet() ;
	}

	/** called from Session listener with session ID stored in static allPages,
	 * so we can get particular HomePage and clean up other global objects 
	 */
	public static void removeSession(final String sid) {
		// rctreeprov.unregister() ;
		final HomePage hp = allPages.remove(sid) ;
		if ( hp != null ) {
			ers.Logger.debug(0, "Cleaning up HomePage associated with session " + sid) ;
			hp.sessionRemoved() ;
			sessionCounter.decrementAndGet() ;
		} else {
			ers.Logger.debug(1, "Could not find HomePage of the session " + sid) ;
		}
	}

	private void sessionRemoved() {
		rctreeprov.unregister() ;
		clearRmResource() ;
	}

	public void addERSTable() {
		ersSubscrLabel = 
		new AjaxEditableLabel<String>("ers-subscription", new Model<String>()) {
			
			private static final long serialVersionUID = -7013368000830433249L;

			@Override
			protected void onSubmit(AjaxRequestTarget target) {
				super.onSubmit(target); 
				System.err.println("New subscription (not validated!): " + getModelObject());

				try {
					ersProvider.subscribe(currentPartition, getModelObject()) ;
					//currentSubscription = getModelObject() ;
					System.err.println("Subscribed in ERS: " + currentPartition + ":" + getModelObject());
				} catch ( final Exception ex ) {
					//currentSubscription = ex.getMessage() ;
					target.appendJavaScript(";alert('Failed to subscribe, bad expression.\\nExample:\\n" 
					+ "(app=HLT* and (not (param(algorithm)=muons and context(file)=test.cpp)) or sev=fatal or msg=rc::*)');");
				} 
				target.add(this); 
			}
		} ;
		
		ersSubscrLabel.setOutputMarkupId(true);
		add(ersSubscrLabel);

		/*
		AjaxEditableLabel<String> ers_filter = 
		new AjaxEditableLabel<String>("ers-filter", new Model<String>(ersFilterRegexp)) {
			private static final long serialVersionUID = -7013368000830423449L;
			
			@Override
			protected void onSubmit(AjaxRequestTarget target) {
				try {
					// ersProvider.setFilter(getModelObject());
					super.onSubmit(target);
					System.err.println("ERS regexp filter set to : " + getModelObject());
				} catch ( final java.util.regex.PatternSyntaxException ex ) {
					target.appendJavaScript(";alert('Bad regular expression: " + ex.getMessage().replaceAll("\\n","\\\\n") + "');");
					ersFilterRegexp = ex.getMessage() ;
					System.err.println("Bad regexp: " + ex.getMessage().replaceAll("\\n","\\\\n"));
				}				
				target.add(this) ;
			}
		} ; 

		ers_filter.setOutputMarkupId(true);
		add(ers_filter);
		*/

		add (new AjaxCheckBox("ersshowchained", new Model<Boolean>(false)) {
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				ersProvider.showChained(getModelObject().booleanValue()) ;
				target.add(ersTable.getTable()) ;
			}
		} ) ;

		add (new AjaxCheckBox("ersshowfilters", new Model<Boolean>(false)) {
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				ersTable.headersHidden(!getModelObject().booleanValue(), target) ;
			}
		} ) ;

		add (new AjaxCheckBox("ersfilterbytree", new Model<Boolean>(false)) {
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				ersProvider.filterByTree(getModelObject().booleanValue()) ;
				target.add(ersTable.getTable()) ;
			}
		} ) ;

		add (new AjaxCheckBox("acrfilter", new Model<Boolean>(false)) {
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				ersProvider.acrFilter(getModelObject().booleanValue()) ;

				try {
					ersProvider.subscribe(currentPartition, default_subscription) ;
					ers.Logger.info("Subscribed in ERS: " + default_subscription + ", ACR filter: " + getModelObject().booleanValue());
					} 
				catch ( final Exception ex ) {
					// ex.printStackTrace() ;
					target.appendJavaScript(";alert('Failed to subscribe to MTS in partition " + currentPartition + ": " + ex + ".');");
					ers.Logger.error(ex) ;	
					return ;
				}

				target.add(ersTable.getTable()) ;
			}
		} ) ;

		java.util.List<String> choices = java.util.Arrays.asList(new String[] { "10", "20", "50", "200" });
	
		ersrowsChoice = new DropDownChoice<String>("ersrowsselector", Model.of("10"), choices) ;
		ersrowsChoice.add(new AjaxFormComponentUpdatingBehavior("change") {	
			private static final long serialVersionUID = 1456345645L;
			
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				try {
					int newsel = Integer.parseInt(ersrowsChoice.getModel().getObject()) ;
					ersTable.getTable().setItemsPerPage(newsel) ;
					target.add(ersTable.getTable()) ;
				} catch ( final Exception ex ) {
					ex.printStackTrace();
				}
			}
		} );

		add(ersrowsChoice) ;

/*		ersClearButton = new AjaxLink<String>("ersclearbutton") {
			@Override
			public void onClick(AjaxRequestTarget target) {
				// TODO not implemented
				//ersProvider.clear_data();
				//target.add(ersTable.getTable()) ;
			} ;
		};
		
		add(ersClearButton) ;
*/

		ersTable = new ERSDataTable("erstable", ersProvider, ersFilter, Configuration.getInt("erstable.rows")) ;
		add(ersTable.getTable()) ;
		
		ersPauseButton = new AjaxLink<String>("erspausebutton", new Model<String>("Pause")) {
			/**
			 *
			 */
			private static final long serialVersionUID = 9124914522262026350L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				if ( getModelObject().equals("Pause") ) {
					setBody(Model.of("Resume")) ;
					setModelObject("Resume");
					add(new AttributeModifier("flashing", Model.of("TRUE"))) ;
					ersTable.pause(target);
				} else {
					setBody(Model.of("Pause")) ;
					setModelObject("Pause");
					add(new AttributeModifier("flashing", Model.of("FALSE"))) ;
					ersTable.resume(target);
				}
				target.add(this) ;
			} ;
		};

		add(ersPauseButton) ;

		add(((AbstractLink)(ersTable.getExportLink())).setBody(Model.of("Export"))) ;
	}
	
	void addRatesPlot() {
	
		ratesChartSeconds = new RatesChart("ratesplotseconds", 600, 350);
		ratesChartSeconds.setOutputMarkupPlaceholderTag(true) ;
		ratesChartSeconds.setOutputMarkupId(true) ;

		ers.Logger.info("Constructed seconds plot");

		ratesChartSeconds.add(new AbstractAjaxTimerBehavior(Duration.milliseconds(Configuration.getInt("chart.rates.seconds.update"))) {
			@Override
			protected void onTimer(AjaxRequestTarget target) {
				target.add(ratesChartSeconds) ;
			}
		}) ; 

		ratesChartMinutes = new RatesChart("ratesplotminutes", 600, 350);
		ratesChartMinutes.setOutputMarkupPlaceholderTag(true) ;
		ratesChartMinutes.setOutputMarkupId(true) ;

		ratesChartMinutes.add(new AbstractAjaxTimerBehavior(Duration.milliseconds(Configuration.getInt("chart.rates.minutes.update"))) {
			@Override
			protected void onTimer(AjaxRequestTarget target) {
				target.add(ratesChartMinutes) ;
			}
		}) ;
		
		ers.Logger.info("Constructed minutes plot");

		ratesPanel = new WebMarkupContainer ("ratespanel") ;
		ratesPanel.setOutputMarkupPlaceholderTag(true) ;
		ratesPanel.setOutputMarkupId(true) ;
		runparamsPanel = new WebMarkupContainer ("runparamspanel") ;
		runparamsPanel.setOutputMarkupPlaceholderTag(true) ;
		runparamsPanel.setOutputMarkupId(true) ;

		rnlabel = new Label("runnumber", () -> getISData.getRN(currentPartition) ) {
			@Override
			protected void onConfigure () {
				super.onConfigure();
				rnlabel.add(AttributeModifier.replace("title", "Run started " + getISData.getSOR(currentPartition) ));
			}		
		};
		
		
		lblabel = new Label("lumiblock", () -> getISData.getLB(currentPartition) ) ;
		recordinglabel = new Label("recording", () -> getISData.getRecording(currentPartition)) {
			@Override
			protected void onConfigure () {
				super.onConfigure();
				if ( recordinglabel.getDefaultModelObject().equals("ON") ) {
					tier0projectlabel.setVisible(true) ;
					recordinglabel.add(new AttributeModifier("recording", Model.of("ON"))) ;
				} else {
					recordinglabel.add(new AttributeModifier("recording", Model.of("OFF"))) ;
					tier0projectlabel.setVisible(false) ;
				}
			}
		} ;

		tier0projectlabel = new Label("tier0project", () -> getISData.getT0Project(currentPartition) ) ;
		trigkeyslabel = new Label("triggerkeys", () -> getISData.getTriggerKeys(currentPartition) ) ;
		tier0projectlabel.setOutputMarkupPlaceholderTag(true) ;
		rnlabel.setOutputMarkupId(true) ;
		lblabel.setOutputMarkupId(true) ;
		recordinglabel.setOutputMarkupId(true) ;
		tier0projectlabel.setOutputMarkupId(true) ;
		trigkeyslabel.setOutputMarkupId(true) ;
		runparamsPanel.add(rnlabel, lblabel, recordinglabel, tier0projectlabel, trigkeyslabel) ;
		
		ratesPanel.add(ratesChartSeconds, ratesChartMinutes) ;
		add(ratesPanel, runparamsPanel) ;
		ers.Logger.info("Rates panel added");

		ratesPanel.setVisible(false) ;
		runparamsPanel.setVisible(false) ;
	}
	
	// returns panel ID in Graphana dashboard per segment from Configuration
	public String getGraphanaIdFromConfig(final String name) {
		if ( selectedNodeName.startsWith("TDAQ") ) {
			return Configuration.get("grafana.segment.panelid.TDAQ") ; 
		} else  
		if ( selectedNodeName.startsWith("SFO") ) {
			return Configuration.get("grafana.segment.panelid.SFO") ;
		}

		final String id = Configuration.get("grafana.segment.panelid." + name) ;
		if ( id == null ) {
			return "" ;
		}
		return id ;
	}

	// returns URL to Graphana panel per segment from user's browser cookie (if any)
	public String getGraphanaUrl(final String name) {
	
		final String value = new CookieUtils().load("webrc.grafanaurl." + currentPartition + "." + name) ;
		if ( value != null ) {
			return value ;
		}

		String panelId = getGraphanaIdFromConfig(name) ;
		if ( panelId.isEmpty() ) {
			return "" ;
			// panelId = "21" ;  //default, RC state
		} 

		if ( name.equals("ATLAS") ) { // predefined: Plan of the day
			return panelId ; // twiki/bin/view/Main/PlanOfTheDay
		}

		// this is for test, normally it is the same host as main page
		// final StringBuffer URL = new StringBuffer("http://pc-atlas-www.cern.ch") ;
		StringBuffer URL = new StringBuffer() ;
	
		URL.append(Configuration.get("grafana.url")).append(Configuration.get("grafana.refresh"))
		.append(Configuration.get("grafana.interval")).append("&var-partition=").append(currentPartition) ;
		URL.append("&panelId=").append(panelId) ;

		return URL.toString() ;
	}

	/*
	public String getCurrentGraphanaUrl() {
		return getGraphanaUrl(selectedNodeName) ;
	} */

	public void addPBeastFrame() {

		pbeastIframe = new WebMarkupContainer("pbeastframe") {
			private static final long serialVersionUID = 4564356454341L;
			
			public void onComponentTag(ComponentTag tag) {
				super.onComponentTag(tag);
				final String sep = ( grafanaUrl.charAt(0) == '/' ? "" : "/" ) ;
				System.err.println("URL for pbeast frame: " + requestUrl + sep + grafanaUrl) ;
				tag.put("src", requestUrl + sep + grafanaUrl);
			}
		} ;

		pbeastIframe.setOutputMarkupId(true) ;
		pbeastIframe.setOutputMarkupPlaceholderTag(true) ;
		add(pbeastIframe) ;
		pbeastIframe.setVisible(false) ;
							
		System.err.println("PBeastFrame added ") ;

		grafanaUrlLabel = new AjaxEditableLabel<String>("grafanaurl", new Model<String>(grafanaUrl)) {
			
			private static final long serialVersionUID = -7013368000830433249L;

			@Override
			protected void onSubmit(AjaxRequestTarget target) {
				super.onSubmit(target); 
				grafanaUrl = getModelObject() ;
				final String key = "webrc.grafanaurl." + currentPartition + "." + selectedNodeName ;
				if ( grafanaUrl == null ) {
					grafanaUrl = "" ;
					new CookieUtils().remove(key) ;
				}
				new CookieUtils().save(key, grafanaUrl) ;
				System.err.println("Stored URL for " + key + ": " + grafanaUrl) ;
				
				target.add(pbeastIframe); 
			}
		} ;

		grafanaUrlPanel = new WebMarkupContainer("grafanaurlpanel") ;
		
		grafanaUrlPanel.setOutputMarkupId(true) ;
		grafanaUrlPanel.setOutputMarkupPlaceholderTag(true) ;
		grafanaUrlPanel.setVisible(false) ;
		grafanaUrlPanel.add(grafanaUrlLabel) ;
		add(grafanaUrlPanel) ;
	}

	@Override
	public synchronized void partitionGone(final String partition) {
		System.err.println("Partition has gone: " + partition);
		ratesChartSeconds.partitionChanged("") ;
		ratesChartMinutes.partitionChanged("") ;
		globalRCStateChanged("ABSENT", false) ;
		selectedNodeName = "" ;
		globalStateChanged.set(true) ;
		clearRmResource() ;
		ersProvider.partitionGone(partition) ;
		currentPartition = "" ;
		// RCNodesHolder.clearPartition(partition) ; // here we are called from RCNodesProvider
        
		partitionsChoice.partitionGone(partition) ;
	}

	// taken from Igui
    private final static class DalComponentComparator implements java.util.Comparator<dal.Component> {

        /**
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        @Override
        public int compare(final dal.Component o1, final dal.Component o2) {
            return o1.UID().compareTo(o2.UID());
        }
	}
	
	public void disableSegment(final dal.Segment segment, final String command) throws config.ConfigException {
		// final dal.TemplateSegment ts = dal.TemplateSegment_Helper.cast(dalSegment) ;
		ers.Logger.log("Segment to " + command + ": " + segment.UID());
		config.Configuration dbRw = ConfigProvider.getRdbRw(currentPartition) ; // throw config.SystemException
		String compid = segment.UID();
		if ( segment.is_templated() ) {
			compid = segment.UID().substring(segment.UID().indexOf(':', 0)+1) ;
		}

		dal.Component comp2disable = dal.Component_Helper.get(dbRw, compid) ;
		ers.Logger.log("DAL Component to " + command + ": " + comp2disable + ": " + comp2disable.UID());
		dal.Component[] disabled_array ;
		dal.Partition p_rw ;

		try {
			p_rw = dal.Partition_Helper.get(dbRw, currentPartition) ;
			disabled_array = p_rw.get_Disabled() ;
		} catch ( final config.SystemException ex ) {
			if ( ex.toString().contains("rdb.SessionNotFound") ) {
				ers.Logger.error(new ers.Issue("Partiton was restarted, invalid RDBRW session " + currentPartition, ex)) ;
				ConfigProvider.clearConfig(currentPartition) ; // clear
				dbRw = ConfigProvider.getRdbRw(currentPartition) ; // and re-try
				p_rw = dal.Partition_Helper.get(dbRw, currentPartition) ;
				disabled_array = p_rw.get_Disabled() ;				
			} else {
				throw ex ;
			}
		}
		
		java.util.AbstractSet<dal.Component> disabled =
			new ConcurrentSkipListSet<dal.Component>(new DalComponentComparator()) ;

		if ( disabled_array != null ) {
			java.util.Collections.addAll(disabled, disabled_array);
		}

		StringBuilder message = new StringBuilder() ;
		if ( command.equals("DISABLE") ) {
			disabled.add(comp2disable) ;
			message.append("Disabled component ").append(comp2disable.UID()) ;
		} else
		if ( command.equals("ENABLE") ) {
			if ( disabled.contains(comp2disable) ) {
				ers.Logger.log("Disabled set contains component: " + comp2disable.UID());
			}
			if ( !disabled.remove(comp2disable) ) {
				throw new RuntimeException("Component " + comp2disable.UID() + " not found in Disabled list in the Partition", null) ;
			}
			message.append("Enabled component ").append(comp2disable.UID()) ;
		}
		
		try {
			RCCommandSender.setToken() ;
			ers.Logger.log("Setting disabled Partition attribute ...") ;
			p_rw.set_Disabled(disabled.toArray(new dal.Component[disabled.size()]));
			ers.Logger.log("Committing to RDB_RW...") ;
			dbRw.commit(message.toString()) ;
			ers.Logger.log("Committed") ;		
		} catch (final config.ConfigException ex) {
			ers.Logger.log("Commit to RDB_RW failed:") ;	
			ers.Logger.error (ex) ;
		} finally {
			RCCommandSender.clearToken() ; 
		}
	}

	public void setException( final String message, final AjaxRequestTarget target ) {
		rcPannelError = message ;
		target.add(rcPannelErrorLabel) ;
	}
	
	public Component getRcTree() {
		return tree ;
	}
	
	public void setDbReloadOk(boolean res) {
		dbReloadOk = res ;
	}
	
	public boolean getDbReloadOk() {
		return dbReloadOk ;
	}
	
	public void setDbReloadNeeded(boolean res) {
		dbReloadNeeded = res ;
	}
	
	public boolean getDbReloadNeeded() {
		return dbReloadNeeded ;
	}

	public void dbReloaded( AjaxRequestTarget target ) {
		disableComponent(reloadButton, target);
	}
		
	public RCTreeNodeProvider getTreeProvider() {
		return rctreeprov ;
	}

	private void disableComponent(final Component comp, final AjaxRequestTarget target) {
		comp.setVisible(false) ;
		comp.setEnabled(false) ;
		comp.add(new AttributeModifier("enabled", Model.of("FALSE"))) ;
		if ( target != null ) { target.add(comp) ; } ;
	}
	
	private void enableComponent(final Component comp, final AjaxRequestTarget target) {
		comp.setVisible(true) ;
		comp.setEnabled(true) ;
		comp.add(new AttributeModifier("enabled", Model.of("TRUE"))) ;
		if ( target != null ) { target.add(comp) ; } ;
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		
		ers.Logger.log("HomePage: Initializing") ;
		
		if( !AuthenticatedWebSession.get().isSignedIn() ) {
			ers.Logger.log("Not yet signed in, doing nothing") ;
			return ;
		}

		ers.Logger.log("HomePage: Authenticated, proceed to constructing") ;

		ConstructHomePage() ;
	}

	@Override
	protected void onConfigure() {
	   super.onConfigure() ;
	   ers.Logger.log("Configuring");
	   AuthenticatedWebApplication app = (AuthenticatedWebApplication)Application.get();
	   //if user is not signed in, redirect him to sign in page
	   if( !AuthenticatedWebSession.get().isSignedIn() ) {
			ers.Logger.log("HomePage: redirect to SignIn page") ;
			app.restartResponseAtSignInPage();
	   }
	   ers.Logger.log("Signed In") ;
	   // BuildHomePage() ;
	   get("loggeduser").setDefaultModel( Model.of(((SignInSession)AuthenticatedWebSession.get()).getUserFullName() + 
	   " (" + ((SignInSession)AuthenticatedWebSession.get()).getUser() + ")") ) ;

	   if ( !currentPartition.equals("Not selected") ) {
		   partitionsChoice.clearInput() ;
	   }
	   ers.Logger.log("onConfigure completed") ;
	}

	/** aka destructor */
	@Override
	protected void finalize() throws Throwable {
		ers.Logger.log("Destroying HomePage for current partition " + currentPartition) ;
		clearRmResource() ;
		// ??
		// ersProvider.partitionGone(currentPartition) ;
		rctreeprov.unregister() ;
		// rctreeprov = null ;
	}
		/** aka destructor v2 as finalize is deprecated*/
	@Override
	public void close() throws Exception {
		ers.Logger.log("Destroying HomePage for current partition " + currentPartition) ;
		clearRmResource() ;
		// ??
		// ersProvider.partitionGone(currentPartition) ;
		rctreeprov.unregister() ;
		// rctreeprov = null ;
	}
	
	@Override
	protected void onDetach() {
		// System.err.println("Cleaning up resources here?");
		super.onDetach();
	}
 
	/** Returns a base URL of the server as accessed by client, e.g. https://atlasop.cern.ch */
	private String getRequestUrl(){
        // this is a wicket-specific request interface
        final Request request = getRequest();
        if( request instanceof WebRequest ){
            final WebRequest wr = (WebRequest) request;
            // this is the real thing
			javax.servlet.http.HttpServletRequest hsr = null ;
			Object ttt = wr.getContainerRequest();
			if ( ttt instanceof javax.servlet.http.HttpServletRequest ) {
				hsr = (javax.servlet.http.HttpServletRequest)ttt ;
			
				String reqUrl = hsr.getRequestURL().toString();
				/*System.err.println("HTTP headers from HomePage:") ;
				for ( final String hdr: java.util.Collections.list(hsr.getHeaderNames()) ) {
					System.err.println("Header " +  hdr + ":" + hsr.getHeader(hdr));
				} */
				
				final String referer = hsr.getHeader("Referer") ; // https://atlasop.cern.ch/tdaq/webrc/...
				if ( referer != null ) {
					int slash = referer.indexOf("//") ;
					if ( slash != -1 ) {
						int pos = referer.indexOf('/', slash + 2) ;
						if ( pos != -1 ) {
							// System.err.println("Request URL from referrer header: " + referer.substring(0, pos)) ; 
							return referer.substring(0, pos) ;
						}
					}
				}

				// if comes from 2 proxy like at P1: X-Forwarded-Server:atlasop.cern.ch, pc-atlas-www-priv.cern.ch
				// SSO-2 sets the header: Header X-Forwarded-Host:atdaq-tbed-webrc.app.cern.ch

				final String server = hsr.getHeader("X-Forwarded-Server") ;
				final String proto = hsr.getHeader("X-Forwarded-Proto") ;
				if ( server != null ) {
					int pos = server.indexOf(',', 0) ;
					if ( pos == -1 ) {
						return (proto == null ? "http": proto) + "://" + server ;
					} else {
						final String first_server = server.substring(0, pos) ;
						return (proto == null ? "http": proto) + "://" + first_server ;
					}
				} else {
					ers.Logger.warning(new ers.Issue("Failed to get X-Forwarded-Server headers, returning " + reqUrl)) ;
					return reqUrl ;
				}
			}
		} else {
			ers.Logger.error(new ers.Issue("Failed to get WebRequest!")) ;
		}
		return null;
    }

	private String getRequestHost(){
        // this is a wicket-specific request interface
        final org.apache.wicket.request.Request request = getRequest();
        if( request instanceof WebRequest ){
			final WebRequest wr = (WebRequest) request;
			return  wr.getUrl().getProtocol() + "://" +  wr.getUrl().getHost() ;
        }
        return null;

    }


}
