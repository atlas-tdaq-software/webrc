package tdaq.webrc;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.http.WebRequest ;
import org.apache.wicket.request.Request ;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.value.ValueMap;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession; 
import org.apache.wicket.markup.html.WebMarkupContainer;

public class SignInPage extends WebPage {

    private static final long serialVersionUID = -5544163930069945985L;
    private Label feedback;

    public SignInPage() {
        feedback = new Label("feedback", "") ;
        add(feedback);
        add(new SignInForm("signInForm"));
    }

    public final class SignInForm extends Form<Void> {
        private static final long serialVersionUID = 5009804789148606994L;
        private static final String USERNAME = "username";
        private static final String PASSWORD = "password";

        private final ValueMap properties = new ValueMap();

        @Override
        protected void onSubmit() {
            super.onSubmit() ;
            //System.err.println("Form submission called") ;
        }

        public SignInForm(final String id) {
            super(id);

            // Attach textfield components that edit properties map model
            add(new TextField<>(USERNAME, new PropertyModel<String>(properties, USERNAME)));
            add(new PasswordTextField(PASSWORD, new PropertyModel<>(properties, PASSWORD)));
        
            final String preauth_user = getAuthUser() ;
            
            WebMarkupContainer ssopanel = new WebMarkupContainer ("ssologinpanel") ;
            ssopanel.setOutputMarkupPlaceholderTag(true) ;
            ssopanel.setOutputMarkupId(true) ;
            add(ssopanel) ;

            if ( preauth_user == null ) {
                System.err.println("Disabling SSO") ;
                ssopanel.setEnabled(false) ;
                ssopanel.setVisible(false) ;
            }

            ssopanel.add(new Label("ssouser", preauth_user == null ? "no SSO": preauth_user)) ;

            Button ssologin = new Button("ssologin") {
                private static final long serialVersionUID = 5732212649064256637L;

                public void onSubmit() {
                    System.err.println("Attempting SSO signIn") ;
                    if ( AuthenticatedWebSession.get().signIn(preauth_user, "abcdef") ) {
                        System.err.println("SSO signIn succeeded, proceed to home page") ;
                        setResponsePage(getApplication().getHomePage());
                    }
                    else {
                        System.err.println("SSO signIn failed!") ;
                        feedback.setDefaultModelObject("Failed to authenticate in CERN LDAP") ;
                    }
                }
            } ;
            
            ssologin.setDefaultFormProcessing(false);
            ssopanel.add(ssologin) ;

            add(new Button("ldapsignin") {
                private static final long serialVersionUID = 6818653103730264781L;

                public void onSubmit() {
                    // setEnabled(false) ;
                    // Sign the user in
                    if ( AuthenticatedWebSession.get().signIn(getUsername(), getPassword()) ) {
                        // continueToOriginalDestination();
                        setResponsePage(getApplication().getHomePage());
                    }
                    else {
                        feedback.setDefaultModelObject("Failed to authenticate in CERN LDAP") ;
                    }
                }
            });
        }

        private String getPassword() {
            return properties.getString(PASSWORD);
        }

        private String getUsername() {
            return properties.getString(USERNAME);
        }
    }

    private String getAuthUser(){
        // this is a wicket-specific request interface
        final Request request = getRequest();
        if( request instanceof WebRequest ){
            final WebRequest wr = (WebRequest) request;
            // this is the real thing
			javax.servlet.http.HttpServletRequest hsr = null ;
			Object ttt = wr.getContainerRequest();
			if ( ttt instanceof javax.servlet.http.HttpServletRequest ) {
				hsr = (javax.servlet.http.HttpServletRequest)ttt ;
				System.err.println("SignInPage: HTTP Request Headers:");
				for ( final String hdr: java.util.Collections.list(hsr.getHeaderNames()) ) {
					System.err.println("Header " +  hdr + ":" + hsr.getHeader(hdr));
                }
                
				// check if the app is accessed from CERN SSO Oauth2, where the user
				final String authuser = hsr.getHeader("X-Forwarded-User") ;
				if ( authuser != null ) {
					System.err.println("Authenticated from new CERN SSO as " + authuser);
					return authuser ;
				}
				// otherwise, get "X-Remote-User" from ATLAS GW (also fom SSO) - if this is empty, 
                return hsr.getHeader("X-Remote-User") ;
			}
        }
        return null;

    }
}