package tdaq.webrc;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication ;
import org.apache.wicket.protocol.http.WebSession ;
import org.apache.wicket.Session ;
import org.apache.wicket.request.Request ;
import org.apache.wicket.request.Response ;
import org.apache.wicket.request.cycle.IRequestCycleListener;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.util.lang.Bytes;


public class WebRCApplicationProbe extends WebApplication
{
	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends WebPage> getHomePage()
	{
		return org.apache.wicket.util.tester.DummyHomePage.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init()
	{
		super.init();
		
		System.err.println("Initializing probe application");
        
        getRequestCycleListeners().add(new IRequestCycleListener() {
            @Override
            public void onEndRequest(RequestCycle cycle) {
                // TODO this seem not working, the request fails
                // Session.get().invalidate(); 
                Object ttt = cycle.getRequest().getContainerRequest() ;
                if ( ttt instanceof javax.servlet.http.HttpServletRequest ) {
                    javax.servlet.http.HttpServletRequest hsr = (javax.servlet.http.HttpServletRequest)ttt ;
                    // hsr.getSession().invalidate() ;
                    hsr.getSession().setMaxInactiveInterval(30);
                    // System.err.println("Invalidated http probe session: " + hsr.getSession().getId()) ;
                }
            } ;
        })	;

        // org.apache.wicket.Session.get().invalidate() ;
    }
    
    @Override
    public final Session newSession(Request request, Response response) {
        WebSession ws = new WebSession(request) ;
        return ws ;
    }

	@Override
	public void sessionUnbound(String sessionId) {
		super.sessionUnbound(sessionId);
		ers.Logger.debug(1, "probe session unbound: " + sessionId);
		//restartResponseAtSignInPage();
	}    
	
}
