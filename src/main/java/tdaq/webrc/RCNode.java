package tdaq.webrc;

import java.io.Serializable;
import java.util.List ;
import java.util.Set ;
import java.util.HashSet ;
import java.util.concurrent.atomic.AtomicLong ;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;

import ers.Logger;

import rc.DAQApplicationInfo ;
import rc.RCStateInfo ;

/**
 * represents a single node in the RC Tree. The actual container of the RCNodes is RCNodesHolder.
 */
class RCNode implements Serializable {

    private static final long serialVersionUID = -7720658462757669479L;

    private static final int load_levels = 2 ; // how many RC tree levels load initially 

    /**  pool used to lazy-load apps states from IS in background */
    private static ExecutorService executor = java.util.concurrent.Executors.newFixedThreadPool(4) ;
    //private static ExecutorService executor = java.util.concurrent.Executors.newSingleThreadExecutor(); // newCachedThreadPool()
    //private static ExecutorService executor = java.util.concurrent.Executors.newCachedThreadPool() ;

    private String name ;
    private String segmentName ;
    private final String partition ;
    private RCNode parent = null ;

    /** time of the last update from IS, used to sort out unordered callbacks, i.e. to ignore the deplayed ones */    
    private AtomicLong timestamp = new AtomicLong(0L) ; 
    private String rcState = new String() ;        ///< RunControl state   
    private String appState = new String() ;       ///< PMG state  
    private String appStateLabel = new String() ; 
    private String rcFailReason = new String() ;
    private String appErrLogFile = new String() ;
    private String appOutLogFile = new String() ;
    private String failureReason = new String() ;
    private String host = new String() ;
    private boolean rcBusy = false ;
    private boolean rcError = false ;
    private boolean appMemebership = true ;
    private boolean enabled = true ; // for Resources and Segments
    private boolean selected = false ; // for Resources and Segments
    private boolean appExited = true ; // flag to refresh out/err file names
    private int appExitCode = 0 ;
    private int appExitSignal = 0 ;

    private List<RCNode> children = new CopyOnWriteArrayList<RCNode>() ;
    private boolean isLoaded = false ; // created first-level subtree
    private boolean isSegment = false ; // Controller of a Segment
    private boolean isRCApp = false ; // RCApplication (may be leaf)
    private boolean isApp = false ; // To exclude containers like infrastrcuture
    private boolean isComponent = false ; // for Resources and Segments
    private boolean isNotCommitted = false ; // for Resources and Segments, temporary state
    private boolean isSetupSegment = false ; // for Resources and Segments, temporary state
    private boolean isBusyProvider = false ; // for segments of detector causing busy
    private float   busy = 0.0f ;

    public RCNode(final String _name, final String part) {
        name = _name ;
        partition = part ;
        segmentName = "" ;
		RCNodesHolder.addNode(partition, name, this) ;
    }

    public RCNode(final String _name, final String part, final RCNode _parent, boolean is_segment) {
        this(_name, part) ;
        parent = _parent ;
        isSegment = is_segment ;
        if ( isSegment ) { segmentName = _name ; }
        Set<String> parents = new HashSet<String>() ;
        getParents(parents) ; // when constructing an application, get it's parent segment for use in ERS filter by tree
        ers.Logger.debug(1, "My parents: " + parents) ;
        ConfigProvider.addParents(part, name, parents) ;
    }

    // constructor from BaseApplication
    public RCNode(final dal.BaseApplication _app, final String part, final RCNode _parent) {
        this(_app.UID(), part, _parent, false) ;
        isApp = true ;
        getAppState() ; // lazy load
        if ( dal.RunControlApplicationBase_Helper.cast(_app) != null ) {
            isRCApp = true ;
            getRCState() ; // lazy
        }
    }

    // constructor from Segment
    public RCNode(final dal.Segment seg, final String part, final RCNode _parent) throws config.ConfigException {
        this(seg.get_controller(), part, _parent) ;
        segmentName = seg.UID() ;
        isSegment = true ;
        isComponent = true ;
        if ( RODBusyIS.isRODBusyAvailable(segmentName) || CTPBusyInfo.isDetBusyAvailable(segmentName) || name.equals("RootController") ) {
            isBusyProvider = true ;
            // System.err.println("CTP Busy is available for " + segmentName) ;
        }
        else {
            // System.err.println("NO CTP Busy for " + segmentName) ;
        }
 
    }

    // constructor from Partition
    public RCNode(final dal.Partition part) {
        this(part.UID().equals("initial") ? "DefaultRootController" : "RootController", part.UID()) ;
        isSegment = true ;
        segmentName = part.UID() ; // just for display
        isRCApp = true ;
        isApp = true ;
        getAppState() ; // lazy load
        getRCState() ;
        try {
            dal.OnlineSegment onlseg = part.get_OnlineInfrastructure() ;
            dal.Segment seg = part.get_segment(onlseg.UID());        
            RCNode onlnode = new RCNode(onlseg.UID(), part.UID(), this, true) ;
            onlnode.setSetupSegment() ;

            children.add(onlnode) ;
            onlnode.loadChildren(load_levels) ;
            for ( dal.Segment segment: seg.get_nested_segments() ) {
                if ( segment.is_disabled() ) {
                    RCNode segnode = new RCNode(segment.UID() , part.UID(), this, true) ;
                    segnode.setEnabled(false) ;
                    addNode(segnode) ;
                    // System.err.println("Added disabled segment " + segment.UID()) ;
                    continue ;
                }
                // System.err.println("Added subsegment " + segment.UID()) ;
                RCNode segnode = new RCNode(segment, part.UID(), this) ;
                addNode(segnode) ;
                if ( load_levels > 0 ) {
                    segnode.loadChildren(load_levels) ;
                }
            } 
        } catch ( final Exception ex ) {
            name = ex.getMessage() ;
            isSegment = false ;
            System.err.println("Failed to load partition tree for " + part.UID()) ;
            ex.printStackTrace() ;
			isLoaded = false ;
			return ; // isLoaded = false
        }
		isLoaded = true ;
    }

    private void getParents(Set<String> parents) {
        if ( parent != null ) {
            parent.getParents(parents) ;
            parents.add(parent.getName()) ;
        }
    }   

    // called from IS callback
    // NB: _appInfo is of ThreadLocal<rc.DAQApplicationInfo> type, so it must not be used in other threads, e.g. captured for a Runnable
    public synchronized boolean updateState(final DAQApplicationInfo _appInfo, final RCStateInfo _rcInfo, long time) {
        if  ( time == 0L && _appInfo == null && _rcInfo == null ) {
            rcState = "ABSENT" ;
            appState = "ABSENT" ;
            appStateLabel = "ABSENT" ;
            appMemebership = true ;
            rcError = false ;
            rcBusy = false ;
            return true ;
        }
        if ( timestamp.get() > time && time != 0L ) { // delayed or unordered callback
            // System.err.println("Ignored callback with ts " + time + " Last ts: " + timestamp.get())  ;
            return false ;
        }
        if ( time > 0L ) { timestamp.set(time) ; }

        if ( _appInfo != null ) {
            appExitCode = _appInfo.exitCode ;
            appState = _appInfo.status ;
            appStateLabel = _appInfo.status ;
            appExitSignal = _appInfo.exitSignal ;
            appMemebership = _appInfo.membership ;
            host = _appInfo.host ;
            if ( appState.equals("UP") && appExited ) { // get and store log file name, lazy call
                appExited = false ;
                final String appname = _appInfo.applicationName ;
                executor.execute( () -> {
                    try {
                        ers.Logger.debug(1, "Async getting PMG data for app "+ appname) ;
                        final pmg.PMGPublishedProcessDataNamed processinfo = ISReciever.getProcessInfo(partition, host, appname) ;
                        synchronized(RCNode.this) {
                            appOutLogFile = processinfo.std_out.substring(processinfo.std_out.lastIndexOf('/')+1) ;
                            appErrLogFile = processinfo.std_err.substring(processinfo.std_err.lastIndexOf('/')+1) ;
                        }
                        
                        } catch ( final is.InfoNotFoundException ex ) {
                            // perhaps OK, try later  
                        } catch ( final is.RepositoryNotFoundException ex ) {
                            // partition seems to have gone, late execution, OK...  
                        }
                    } ) ;   
            } 
            // if ( appState.equals("EXITED") && isRCApp ) { rcState = "" ; } // display AppInfo
            if ( appState.equals("ABSENT") && isRCApp ) {
                rcState = "ABSENT" ;
                rcBusy = false ;
                rcError = false ; 
            }
            if ( appState.equals("EXITED") ) {
                appStateLabel = "ABSENT" ; // Igui- conformant
                appExited = true ; // to refresh out/eer file at next UP (e.g. in case of application restart)
                if ( appExitSignal != 0 ) {
                    appState = "DIED=" + Integer.toString(appExitSignal) ;
                } else {
		            appState = appState + "(" + Integer.toString(appExitCode) + ")" ;
		        }
                if ( isRCApp ) {
                    rcState = "ABSENT" ;
                    rcBusy = false ;
                    rcError = false ;
                }
            } // display AppInfo, as there may be no callback with RC state in case of crash
            if ( appState.equals("FAILED") ) {
                try {
                    pmg.PMGPublishedProcessDataNamed processinfo = ISReciever.getProcessInfo(partition, _appInfo.host, _appInfo.applicationName) ;
                    failureReason = processinfo.error_msg ;
                } catch ( final is.InfoNotFoundException ex ) {
                        // perhaps OK, try later  
                } catch ( final is.RepositoryNotFoundException ex ) {
                        // partition seems to have gone, late execution, OK...  
                }
            }
			
        } else
        if ( _rcInfo != null && !appState.equals("ABSENT") && !appState.equals("EXITED")) {
            rcState = _rcInfo.state ;
            rcBusy = _rcInfo.busy ;
            rcError = _rcInfo.fault ;
            rcFailReason = "" ;
            for ( final String reas: _rcInfo.errorReasons ) {
                rcFailReason += reas + "\n" ;
            }
        }
        
        return true ;
    }

    public synchronized String getRCState() {
        if ( rcState.equals("") && isRCApp ) { // lazy loading of info from IS
            pullRCState() ;
        }
        return rcState ; 
    }

    // lazy pull fresh state from IS
    public void pullRCState() {
        executor.execute( new Runnable() {
            
            @Override
            public void run() {
                final RCStateInfo rcinfo = ISReciever.getRCInfo(partition, name) ;
                if ( rcinfo != null ) {
                    updateState(null, rcinfo, 0L) ;
                    if ( isRoot(name) ) {
                        ISReciever.updateRoots(partition, rcinfo.state, rcinfo.busy) ;
                    }
                }
            }
        }) ;
    }

    public synchronized boolean getRCBusy() {
        return rcBusy ; 
    }

    public synchronized boolean getRCError() {
        return rcError ; 
    }

    public synchronized int getExitCode() {
        return appExitCode ; 
    }

    public synchronized String getRCFailReason() {
        StringBuilder ret = new StringBuilder(rcFailReason) ;
        for (RCNode rcNode : children) {
            if ( rcNode.getRCError() ) {
                ret.append(rcNode.getRCFailReason()).append('\n') ;
            }
        }
        return ret.toString() ; 
    }

    public synchronized String getErrLogFile() {
        return appErrLogFile ; 
    }
    
    public synchronized String getOutLogFile() {
        return appOutLogFile ; 
    }
 
    public synchronized String getFailureReason() {
        return failureReason ; 
    }

    public synchronized String getHost() {
        return host ; 
    }
    
    public synchronized void setSetupSegment() {
        isSetupSegment = true ; 
    }

    public synchronized boolean isSetupSegment() {
        return isSetupSegment ; 
    }

    public synchronized String getAppState() {
        if ( appState.equals("") && !isSetupSegment 
             && !name.endsWith(":infrastructure") && !partition.equals("") ) { // lazy loading of info from IS
            pullAppState() ;
        }
        return appState ; 
    }

    public void pullAppState() {
        executor.execute( new Runnable() {
            
            @Override
            public void run() {
                // System.err.println("Lazy getting IS app state for " + name);
                DAQApplicationInfo appinfo = ISReciever.getDAQAppInfo(partition, name) ;
                updateState(appinfo, null, 0L) ;
            }
        }) ;
    }

    public synchronized int getAppExitCode() {
        return appExitCode ; 
    }
    public synchronized int getAppExitSignal() {
        return appExitSignal ; 
    }
    public synchronized boolean getAppMembership() {
        return appMemebership ; 
    }

    public synchronized boolean isLoaded() {
        return isLoaded ;
    }

    public synchronized void setLoaded() {
        isLoaded = true ;
    }

    public boolean isSegment() {
        return isSegment ;
    }

    public boolean isComponent() {
        return isComponent ;
    }

    public boolean isRCApp() {
        return isRCApp ;
    }

    public boolean isApp() {
        return isApp ;
    }

    public boolean isEnabled() {
        return enabled ;
    }

    public boolean isSelected() {
        return selected ;
    }

    public void setSelected(boolean sel) {
        selected = sel ;
    }

    public void setEnabled(boolean state) {
        enabled = state ;
        isComponent = true ;
    }

    public boolean isCommitted() {
        return !isNotCommitted ;
    }

    public void setCommitted(boolean state) {
        isNotCommitted = !state ;
    }

    public String getName() {
        return name ;
    }
    
    public RCNode getParent() {
        if ( parent == null ) return null ;
        if ( parent.getName().endsWith(":infrastructure") ) {
            return parent.getParent() ;
        }
        if ( parent.isSetupSegment() ) {
            return parent.getParent() ;
        }
        return parent ;
    }

    @Override
    public String toString() {					
        if ( !getRCState().equals("") && !getRCState().equals("ABSENT") && isSegment ) { return "[" + rcState + "] " + segmentName ; }
        if ( !enabled && isSegment ) { return segmentName ; }
        if ( !getRCState().equals("") && !getRCState().equals("ABSENT") ) { return "[" + rcState + "] " + name ; }
        if ( !getAppState().equals("") ) { return "[" + appStateLabel + "] " + name; }
        if ( name.endsWith(":infrastructure") ) { return "infrastructure" ; }
        if ( isSetupSegment ) { return "Online Segment" ; }
        return "[UNKNOWN] " + name ;
    }

    private void addNode(final RCNode child) {
        children.add(child) ;
    }

    // used in loadChildren to add infrastrcuture apps to parent's Infrastrcuture subtree
    private RCNode getInfrastrcuture() {
        if ( isRoot(name) ) {
            return children.get(0).getInfrastrcuture() ;
        }
        if ( children.size() == 0 || !children.get(0).getName().endsWith(":infrastructure") ) {
            RCNode infr = new RCNode(name + ":infrastructure", partition, this, true) ; 
            children.add(0, infr) ;
        }
        return children.get(0) ;
    }   

    //* loads 1st level subtree from RDB, i.e. lazy-loads only the part of the tree that is requested to be disaplyed */ 
    public synchronized void loadChildren(int level) {
        if ( isLoaded || !isSegment || !enabled ) return ; // nothing to do
        
        // System.err.println("Loading subtree of " + name) ;
        
        try {
            // System.err.println("Getting segment " + partition + "@" + segmentName) ;
            dal.Segment segment = ConfigProvider.getSegment(partition, segmentName) ; // RDB config call, throws
            dal.BaseApplication[] infapps = segment.get_infrastructure() ;
            if ( infapps.length != 0 ) {
                RCNode infr = getParent().getInfrastrcuture() ;
                for ( dal.BaseApplication app: infapps ) {
                    infr.addNode(new RCNode(app, partition, infr)) ;
                }
                infr.setLoaded() ;
            }
            for ( dal.BaseApplication app: segment.get_applications() ) {
                addNode(new RCNode(app, partition, this));
            }
            if ( !segment.class_name().equals("OnlineSegment") ) {     // loaded into partition
                // System.err.println("Loading nested segments of " + segment.UID()) ;
                for ( dal.Segment chseg: segment.get_nested_segments() ) {                
                    if ( chseg.is_disabled() ) {
                        RCNode segnode = new RCNode(chseg.UID(), partition, this, true) ;
                        segnode.setEnabled(false) ;
                        children.add(segnode) ;
                        continue ;
                    }
                    RCNode seg = new RCNode(chseg, partition, this) ;
                    addNode(seg) ;
                    if ( level > 0 ) {
                        seg.loadChildren(level-1) ;
                    }
                } ;
            }
			isLoaded = true ;
        } catch ( final Exception ex ) {
            System.err.println("Failed to load children of " + name) ;
            ex.printStackTrace();
        }
    }

    public synchronized List<RCNode> getChildren(final String rcstate, final HomePage.Mode mode) {
        if ( rcstate.equals("NONE") && mode == HomePage.Mode.CONTROL ) { // in NONE state we'll see disabled items
            return children ;
        }
        List<RCNode> ret = new CopyOnWriteArrayList<RCNode>(children) ;
        ret.removeIf((RCNode node) -> !node.isEnabled()); // return only enabled
        return ret ;
    }

    public synchronized boolean hasChildren() {
        return !children.isEmpty() ;
    }

    public synchronized void clearChildren() {
        for ( RCNode ch: children ) {
            ch.clearChildren() ;
        }
        children.clear() ;
    }

    public boolean isRoot(final String name) {
        return name.equals("RootController") || name.equals("DefaultRootController") ;
    }

    public boolean isCTPBusy() {
        return isBusyProvider ;
    }
 
    public float getCTPBusyFloat(CTPBusyInfo.BUSY_TYPE type) {
        if ( isBusyProvider ) {
            if ( RODBusyIS.isRODBusyAvailable(segmentName) ) {
                busy = RODBusyIS.getDetBusyFraction(partition, segmentName) ;
            }
            else {
                busy = CTPBusyInfo.getDetBusyFraction(partition, segmentName, type) ;
                if ( type == CTPBusyInfo.BUSY_TYPE.RUN ) {
                    float threshold = 0.0f;
                    try { 
                        threshold = Float.parseFloat(Configuration.get("busy.run.display.threshold")) ;
                    } catch ( final NumberFormatException | NullPointerException ex ) {
                        Logger.error(ex) ;
                        return 0.0f ;
                    }
                    if ( busy < threshold ) return 0.0f ;
                }
            } ;
            return busy ;
        }

        java.util.List<Float> list = new java.util.ArrayList<Float>() ;
        
        if ( children.isEmpty() ) return 0.0f ;

        for ( RCNode child: children ) {
            float ch_busy = child.getCTPBusyFloat(type) ;
            if ( ch_busy > 0.0f ) { list.add(child.getCTPBusyFloat(type)) ; }
        }
        if ( list.isEmpty() ) { return 0.0f ; }

        // System.err.println("Getting combined CTP busy for " + name + ": " +java.util.Collections.max(list).floatValue()  ) ;
        
        return java.util.Collections.max(list).floatValue() ;
    }

    public String getCTPBusy(CTPBusyInfo.BUSY_TYPE type) {
        if ( name.equals("RootController") && type == CTPBusyInfo.BUSY_TYPE.INST ) {
            int rc_busy_count = getISData.getRCBusyCount(partition) ;
            if ( rc_busy_count > 0 ) {
				StringBuffer ret = new StringBuffer("ON HOLD") ;
                if ( rc_busy_count > 1 ) {
                    ret.append(" [").append(rc_busy_count).append("]") ;
                }
				if ( getRCState().equals("RUNNING") ) {
					ret.append(" [").append(getISData.getBusySource(partition)).append("]") ;
				}
				return ret.toString() ;
            }
            return CTPBusyInfo.getTotalBusyFraction(partition) ;
        }

        if ( !getRCState().equals("RUNNING") && type == CTPBusyInfo.BUSY_TYPE.RUN ) { return "0.00"; } ;

        if ( name.equals("RootController") ) { return "0.00"; } ;

        if ( type == CTPBusyInfo.BUSY_TYPE.INST ) {
            if ( name.equals("HLT") ) {
                return getISData.getHLTFarmAvailability(partition) ;   
            }
    //		if ( name.contains("HLTMPPU") ) {
    //            return getISData.getMotherInfo(partition, name) ;   
    //        }
            if ( name.startsWith("HLT-") ) {
                return getISData.getRackOccupancy(partition, name) ;   
            }
            if ( name.equals("HLTSV") ) {
                return getISData.getHLTSVAvailability(partition) ;   
            }
            if ( name.startsWith("SFO") ) {
                return getISData.getSFOOccupancy(partition, name) ;   
            }
        }
        ers.Logger.debug(2, "Getting CTP busy for " + name + ": " + String.format("%.2f", getCTPBusyFloat(type))) ;
        try {
            return String.format("%.2f", getCTPBusyFloat(type)) ;
        }
        catch ( final Exception ex ) {
            ex.printStackTrace() ;
            return "0.00" ;
        }
        
    }
}
