package tdaq.webrc ;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.ExecutorService;
import java.io.BufferedReader;
import java.io.IOException;

import org.apache.wicket.util.time.Duration ;
import org.apache.wicket.model.PropertyModel ;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AbstractAjaxTimerBehavior;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession ;

import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow ;

public class PartitionControlPanel extends Panel
{
    private static final long serialVersionUID = -7484792636892353920L;

    private static ExecutorService executor = java.util.concurrent.Executors.newSingleThreadExecutor(); // newCachedThreadPool()

    private StringBuffer output = new StringBuffer(4096) ;
    private String update = "" ;
    private TextArea<String> outputArea ;
    private AtomicBoolean updated = new AtomicBoolean(false) ;
    private AtomicBoolean partition_launched = new AtomicBoolean(false) ;
    private Process last_command = null ;

    private String startbutton_id ;
    private String killbutton_id ;
    private Component db_selector ;
    private String daq_token ;
   
    public boolean launchStatus() {
        return partition_launched.get() ;
    }

    public String getOutput() {
       return output.toString() ;
    }

    public void setOutput(final String newOutput) {
        output = new StringBuffer(newOutput) ;
    }
    
    public synchronized void appendOutput(final String added) {
        output.append(added).append("\n") ;
        update = update + added.replace("\n", "\\n") + "\\n" ;
        updated.set(true) ;
    }

    // make sure last_command is killed
    public void cleanup() {
        if ( last_command != null ) {
            try {
                last_command.destroy() ; 
                last_command.destroyForcibly() ;
            } catch ( final Exception ex ) {
                ex.printStackTrace(); 
            }
        } ;
    }

    void disableButton(final String id, boolean flag, AjaxRequestTarget target) {
        if ( flag ) {
            target.appendJavaScript("document.getElementById('" + id + "').style.visibility = 'visible' ;") ;
        } else {
            target.appendJavaScript("document.getElementById('" + id + "').style.visibility = 'hidden' ;") ;
        }
    }

    void cursorWait(AjaxRequestTarget target) {
       // target.appendJavaScript("document.getElementById('" + output_id + "').style.cursor = 'wait';");
    }

    void cursorDefault(AjaxRequestTarget target) {
       // target.appendJavaScript("document.getElementById('" + output_id + "').style.cursor = 'default';");
    }
            
    public PartitionControlPanel(final String id, final String release_setup, 
        final String partition, final String config, final ModalWindow window, final Component dbselection) {
        super(id);
        db_selector = dbselection ;
        daq_token = ((SignInSession)AuthenticatedWebSession.get()).getTdaqToken() ;

        //outputArea = new TextArea<String>("commandoutput", new PropertyModel<String>(this, "output")) ;
        //outputArea = new TextArea<String>("commandoutput") ;
        //outputArea.setOutputMarkupId(true) ;
        //output_id = outputArea.getMarkupId() ;
   
        add(new AbstractAjaxTimerBehavior(Duration.ONE_SECOND) {
            private static final long serialVersionUID = 456345745675L;
            @Override
            protected void onTimer(AjaxRequestTarget target) {

                // if ( updated.getAndSet(false) ) {
                if ( true ) {
                    // target.focusComponent() ;
                    // target.add(outputArea) ;
                    //target.appendJavaScript("var obj=document.getElementById(\"outputarea\");var txt=document.createTextNode('" + output + "');obj.appendChild(txt)");    
                    synchronized (this) {
                    target.appendJavaScript("document.getElementById(\"outputarea\").value += '" + update + "';");
                    update = "" ;
                    }
                    //target.appendJavaScript("document.getElementById(\"outputarea\").scrollTop = document.getElementById(\"outputarea\").scrollHeight") ;
                } else {
                }
            }
    
        });

        // add(outputArea) ;

        AjaxLink<Void> startbutton = 
        new AjaxLink<Void>("startpartitionbutton") {
            private static final long serialVersionUID = -10814481540767L;
            
            @Override
            public void onClick(AjaxRequestTarget target) {
                //target.appendJavaScript("$('#"+ myId + "').prop('style.visibility', 'hidden');");
                //target.appendJavaScript("document.getElementById('" + myId + "').style.visibility = 'hidden' ;");
                disableButton(startbutton_id, false, target);
                disableButton(killbutton_id, false, target);
                cursorWait(target);
                executor.execute( () -> {
                    try {
                        // partition is daq/partitions/part_test.data.xml
                        //String partname = partition.substring(partition.lastIndexOf('/')+1, partition.firstIndexOf('.')) ;
                        ProcessBuilder pb = new ProcessBuilder(Configuration.get("local.bin.path") + "/setup_daq", "-ng", "-d", config, "-p", partition) ;
                        java.util.Map<String, String> env = pb.environment();
                        env.put("PATH", Configuration.get("local.bin.path") + ":" + env.get("PATH")) ;

                        if ( daq_token != null) {
                            ers.Logger.log("Using daq token to authenticate user starting the partition") ;
                            ers.Logger.debug(1, daq_token);
                            env.put("TDAQ_TOKEN_ACQUIRE", "env") ; // so that RootController/rc_bootstrap is started under user account by pmg_start_app from setup_daq
                            env.put("BEARER_TOKEN", daq_token) ; // pass this to RootController env so that it passes the token to PMG when starting all the rest processes    
                        } else {
                            ers.Logger.log("No daq token from the web session, using the default tocken aquire mechanism") ;
                        }

                        pb.redirectErrorStream(true);
                        pb.directory(new java.io.File("/tmp")) ;
                        ers.Logger.log("Starting setup_daq process to launch partition " + partition + ": " + pb.toString()) ;
                        String line;
                        last_command = pb.start() ;
                        BufferedReader br = new BufferedReader(new java.io.InputStreamReader(last_command.getInputStream()), 8);
                            while ((line = br.readLine()) != null) {
                            appendOutput(line);
                            ers.Logger.debug(1, line);
                        }
                        int status = last_command.waitFor() ;
                        ers.Logger.log("setup_daq process exited with status " + status);
                        if ( status != 0 ) {
                            appendOutput("**********\n setup_daq finished with ERROR. You need to fix your configuration.");
                            partition_launched.set(false);
                        } else {
                            appendOutput("**********\n setup_daq finished without errors, you can connect to your parttiion");
                            partition_launched.set(true);
                        }
                        appendOutput("\n");
                    } catch ( final InterruptedException ex ) {
                        appendOutput("**********\n process interrupted: " + ex.getMessage() );
                        ex.printStackTrace() ;
                    } catch (final IOException ex) {
                        appendOutput("**********\n failed to run setup_daq: " + ex.getMessage()) ;
                        ex.printStackTrace() ;
                    } catch ( final Exception ex) {
                        ex.printStackTrace() ;
                    } 
                    finally {
                        ers.Logger.debug(0, "Process finished") ;
                        disableButton(startbutton_id, true, target);
                        disableButton(killbutton_id, true, target);
                        cursorDefault(target);
                    }
                }
                ) ;
            }
        } ;
        startbutton.setOutputMarkupId(true) ;
        startbutton_id = startbutton.getMarkupId() ;
        add(startbutton) ;
        
        AjaxLink<Void> killbutton = 
        new AjaxLink<Void>("killpartitionbutton") {
            private static final long serialVersionUID = -34345564564565L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                String command = 
                    "rc_sender -p " + partition + " -n RootController -c EXIT" + " ; sleep 1; " +
                    "pmg_kill_partition -p " + partition +
                    "; pmg_list_partition -p " + partition ;
                disableButton(startbutton_id, false, target);
                disableButton(killbutton_id, false, target);
                executor.execute( () -> {
                    try {
                        ProcessBuilder pb = new ProcessBuilder("/bin/bash", "-c", command);
                        java.util.Map<String, String> env = pb.environment();
                        env.put("TDAQ_TOKEN_ACQUIRE", "env") ; // so that RootController/rc_bootstrap is started under user account by pmg_start_app from setup_daq
                        env.put("BEARER_TOKEN", daq_token) ; // pass this to RootController env so that it passes the token to PMG when starting all the rest processes

                        pb.redirectErrorStream(true);
                        String line;
                        last_command = pb.start() ;
                        BufferedReader br = new BufferedReader(new java.io.InputStreamReader(last_command.getInputStream()), 16);
                        while ((line = br.readLine()) != null) {
                            appendOutput(line);
                        }
                        int status = last_command.waitFor() ;
                        if ( status == 0 ) {
                            appendOutput("**********\nSuccessfully cleaned up partition infrastructure");
                        }
                        ers.Logger.log("Kill partition process finished with status " + status);          
                    } catch ( final InterruptedException ex ) {
                        appendOutput("**********\n process interrupted: " + ex.getMessage() );
                        ex.printStackTrace() ;
                    } catch (final IOException ex) {
                        ex.printStackTrace() ;
                    } finally {
                        ers.Logger.debug(0, "Process finished");
                        disableButton(startbutton_id, true, target);
                        disableButton(killbutton_id, true, target);
                    } 
                }) ;                
            }
        };
        killbutton.setOutputMarkupId(true) ;
        killbutton_id = killbutton.getMarkupId() ;
        add(killbutton) ;

        add(new AjaxLink<Void>("cancelbutton") {
            private static final long serialVersionUID = -3454564563456L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                cleanup() ;
                target.appendJavaScript("document.getElementById(\"selectdatabase\").selectedIndex = \"0\";") ;
                target.add(db_selector) ; // update dropdown elemnent in HomePage
                window.close(target) ;
            }
        });
    }

    
}