package tdaq.webrc ;

import tdaq.webrc.hltsv.* ;
import tdaq.webrc.dcm.* ;
import tdaq.webrc.HLTMPPU.* ;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class getISData {

    final static String lb_info_path = "RunParams.LumiBlock" ;
    final static String lb_info_attribute = "LumiBlockNumber" ; // U32
    final static String rn_info_path = "RunParams.LumiBlock" ;
    final static String rn_info_attribute = "RunNumber" ; // U32
    final static String runtype_info_path = "RunParams.RunParams" ;
    final static String runtype_info_attribute = "run_type" ; // String
    final static String runinfo_info_path = "RunParams.RunParams" ;
    
    final static String trigl1key_path = "RunParams.TrigConfL1PsKey" ;
    final static String trigl1key_attribute ="L1PrescaleKey" ; // U32
    final static String trighltkey_path = "RunParams.TrigConfHltPsKey" ;
    final static String trighltkey_attribute = "HltPrescaleKey" ; // U32
    final static String trigsmkey_path = "RunParams.TrigConfSmKey" ;
    final static String trigsmkey_attribute = "SuperMasterKey" ; // U32
    final static String trigbgkey_path = "RunParams.TrigConfL1BgKey" ;
    final static String trigbgkey_attribute ="L1BunchGroupKey" ; // U32

	static CachedInfo<String> ATLAS_status ;
	static CachedInfo<String> Trigger_keys ;
	static CachedInfo<String> HLTSV_availability, ROIB_Busy ;

    public getISData() { }
		
	static {
		ATLAS_status = new CachedInfo<String>(10, new CachedInfo.ValueProvider<String>() {
			@Override
			public String provideValue() {
				try {
					is.InfoList isenum = new is.InfoList(new ipc.Partition("ATLAS"), "DDC", java.util.regex.Pattern.compile(".*\\.partitionState")) ;
					for ( int i=0; i < isenum.size(); i++ ) {
						is.AnyInfo info = new is.AnyInfo() ;
						isenum.getInfo(i, info) ;
						try {
							if ( ((Integer)info.getAttribute(1)).intValue() != 1 ) {
								return "NON READY" ;
							}
						}
						catch ( final java.lang.NullPointerException ex ) {
							System.err.println("Failed to read info " + info.getName() + ": " + ex.getMessage());
							return "?" ;
						}
					}
					if ( isenum.size() > 0 ) {  return "READY" ; }
					else { return "N/A" ; }
				}
				catch ( final is.RepositoryNotFoundException ex ) {
				}
				catch ( final Exception ex ) {
					ex.printStackTrace() ;
				}
				return "?" ;
			}
		} ) ;
    
		Trigger_keys = new CachedInfo<String>(10, new CachedInfo.ValueProvider<String>() {
			@Override
			public String provideValue() {
				TTCInfo.TrigConfSmKey smkey = new TTCInfo.TrigConfSmKey() ;
				TTCInfo.TrigConfL1PsKey l1key = new TTCInfo.TrigConfL1PsKey()  ;
				TTCInfo.TrigConfHltPsKey hltkey = new TTCInfo.TrigConfHltPsKey() ;
				TTCInfo.TrigConfL1BgKey bgkey = new TTCInfo.TrigConfL1BgKey() ;
				is.Repository repo = new is.Repository(new ipc.Partition("ATLAS")) ;
				try {
					repo.getValue(trigsmkey_path, smkey) ;
					repo.getValue(trigl1key_path, l1key) ;
					repo.getValue(trighltkey_path, hltkey) ;
					repo.getValue(trigbgkey_path, bgkey) ;
				} catch ( final is.RepositoryNotFoundException | is.InfoNotFoundException ex ) {
					return "Unknown" ;
				}
				catch ( final Exception ex ) {
					System.err.println(ex);
					ex.printStackTrace();
					return "Unknown" ;
				}
				return "[" + Integer.toString(bgkey.L1BunchGroupKey) + "] " +
						Integer.toString(smkey.SuperMasterKey) + "/" + Integer.toString(l1key.L1PrescaleKey) + 
						"/" + Integer.toString(hltkey.HltPrescaleKey)  ;
			}
		}) ;

		HLTSV_availability = new CachedInfo<String>(5, new CachedInfo.ValueProvider<String>() {
			@Override
			public String provideValue() {
				HLTSV hltsv_is = new HLTSV() ;
				try {
					new is.Repository(new ipc.Partition("ATLAS")).getValue("DF.HLTSV.Events", hltsv_is) ; // HLTSV.FracAvailable, index 5, float
					//int max_avail = hltsv_is.AvailableCores ;
					//int cores_avail = hltsv_is.MaxAvailable ;
					long[] used_pages = hltsv_is.RNP_Used_pages ;
					final long max_pages = 4996 ; // some fixed number in hw or fw
		
					long max_used = Arrays.stream(used_pages).max().getAsLong() ;
				   
					// return Integer.toString(cores_avail) + "/" + Integer.toString(max_avail) ;
					return Long.toString(max_used*100/max_pages) + "%" ; // % occupancy
				}
				catch ( final is.InfoNotFoundException | is.RepositoryNotFoundException | NoSuchElementException ex ) {
				}
				catch ( final Exception ex ) {
					ex.printStackTrace() ;
				}
				return "" ;
			}
		} ) ;

		ROIB_Busy = new CachedInfo<String>(5, new CachedInfo.ValueProvider<String>() {
			@Override
			public String provideValue() {
				HLTSV hltsv_is = new HLTSV() ;
				try {
					new is.Repository(new ipc.Partition("ATLAS")).getValue("DF.HLTSV.Events", hltsv_is) ; // HLTSV.FracAvailable, index 5, float
					double[] xoff_per = hltsv_is.RNP_XOFF_per ;
					double max_xoff_per = Arrays.stream(xoff_per).filter(value -> value < 100.0).max().getAsDouble() ;

					return String.format("%.1f", max_xoff_per) + "%" ; 
				}
				catch ( final is.InfoNotFoundException | is.RepositoryNotFoundException | NoSuchElementException ex ) {
				}
				catch ( final Exception ex ) {
					ex.printStackTrace() ;
				}
				return "" ;
			}
		} ) ;
	}

    public static synchronized String getRN(final String partition) {
        TTCInfo.LumiBlock lbinfo = new TTCInfo.LumiBlock() ;
        try {
            new is.Repository(new ipc.Partition(partition)).getValue(lb_info_path, lbinfo) ;
        } catch ( final Exception ex ) {
            return "Unknown" ;
        }
        return Integer.toString(lbinfo.RunNumber) ;
    }
    
    public static synchronized String getLB(final String partition) {
        TTCInfo.LumiBlock lbinfo = new TTCInfo.LumiBlock() ;
        try {
            new is.Repository(new ipc.Partition(partition)).getValue(lb_info_path, lbinfo) ;
        } catch ( final Exception ex ) {
            return "Unknown" ;
        }
        return Integer.toString(lbinfo.LumiBlockNumber) ;
    }

    public static synchronized String getRecording(final String partition) {
        rc.RunParams rpars = new rc.RunParams() ;
        try {
            new is.Repository(new ipc.Partition(partition)).getValue(runinfo_info_path, rpars) ;
        } catch ( final Exception ex ) {
            return "Unknown" ;
        }
        if ( rpars.recording_enabled != 0 ) {
            return "ON" ;
        }
        return "OFF" ;
    }   
    
    public static synchronized String getSOR(final String partition) {
        rc.RunParams rpars = new rc.RunParams() ;
        try {
            new is.Repository(new ipc.Partition(partition)).getValue(runinfo_info_path, rpars) ;
        } catch ( final Exception ex ) {
            ex.printStackTrace();
            return "Unknown" ;
        }
        return rpars.timeSOR.toString() ;
    } 

    public static synchronized String getT0Project(final String partition) {
        rc.RunParams rpars = new rc.RunParams() ;
        try {
            new is.Repository(new ipc.Partition(partition)).getValue(runinfo_info_path, rpars) ;
        } catch ( final Exception ex ) {
            return "Unknown" ;
        }
        return rpars.T0_project_tag ;
    }

    public static synchronized String getRunType(final String partition) {
        return "data_test" ;
    }

    public static synchronized int getRCBusyCount(final String partition) {
        is.AnyInfo rpars = new is.AnyInfo() ;
        try {
            new is.Repository(new ipc.Partition(partition)).getValue("RunParams.GlobalBusy", rpars) ;
            return ((Integer)rpars.getAttribute(1)).intValue() ;
        }
        catch ( final is.InfoNotFoundException | is.RepositoryNotFoundException ex ) {
        }
        catch ( final Exception ex ) {
            ex.printStackTrace() ;
        }
        return 0 ;
    }

    /**
     * Returns space-separated busy source/reason(s) as published by CHIP in RunParams.HoldTrigger-<ID> objects
     * 
     * @param partition
     * @return String with busy source/reason(s)
     */
	public static String getBusySource(final String partition) {
		// rc.HoldTriggerInfo htinfo = new rc.HoldTriggerInfo() ;
        
        final StringBuffer ret = new StringBuffer() ;
        try {

        is.InfoList busy_list = new is.InfoList(new ipc.Partition(partition), "RunParams", java.util.regex.Pattern.compile("HoldTrigger-.*") ) ;
        for ( int i = 0 ; i < busy_list.size() ; i++ ) {
                try {
                    rc.HoldTriggerInfo hold_info = new rc.HoldTriggerInfo() ;
                    busy_list.getInfo(i, hold_info) ;
                    ret.append(hold_info.causedByString).append("/").append(hold_info.reasonString).append(" ") ;
                } catch ( final is.InfoNotCompatibleException ex ) {
                    ers.Logger.error(ex) ;
                    continue ;
                }
        }
        } catch ( final is.InvalidCriteriaException | is.RepositoryNotFoundException ex ) {
            ers.Logger.error(ex) ;
        }

        return ret.toString() ;
	}

    public static synchronized String getTriggerKeys(final String partition) {
		if ( partition.equals("ATLAS") ) { return Trigger_keys.getValue() ; }
		return "" ; // TODO?
    }

    public static synchronized String getHLTFarmAvailability(final String partition) {
        is.AnyInfo hltsv = new is.AnyInfo() ;
        is.AnyInfo dcm = new is.AnyInfo() ;
        try {
            new is.Repository(new ipc.Partition(partition)).getValue("DF.HLTSV.Events", hltsv) ; // HLTSV.FracAvailable, index 5, float
            new is.Repository(new ipc.Partition(partition)).getValue("DF.TopMIG-IS:HLT.info", dcm) ; // DCM.ProxBusyPUs, index 19, U64
            // long frac_avail = Math.round(((Float)hltsv.getAttribute(5)).floatValue()*100.0) ;
            int cores_avail = ((Integer)hltsv.getAttribute(4)).intValue() ;
            long busy_pus = ((Long)dcm.getAttribute(19)).longValue() ;
            long free_pus = ((Long)dcm.getAttribute(20)).longValue() ;
			long frac_avail = Math.round(((cores_avail+0.1)/(busy_pus+free_pus+0.1))*100.0) ;
            
            return Long.toString(frac_avail) + "% (" + Integer.toString(cores_avail) + "/" + Long.toString(free_pus) + "/" 
                 + Long.toString(busy_pus+free_pus) + ")" ;
        }
        catch ( final is.InfoNotFoundException | is.RepositoryNotFoundException ex ) {
        }
        catch ( final Exception ex ) {
            ex.printStackTrace() ;
        }
        return "" ;
    }

    public static String getHLTSVAvailability(final String partition) {
		if ( partition.equals("ATLAS") ) { return HLTSV_availability.getValue() ; }

        HLTSV hltsv_is = new HLTSV() ;
        try {
            new is.Repository(new ipc.Partition(partition)).getValue("DF.HLTSV.Events", hltsv_is) ; // HLTSV.FracAvailable, index 5, float
            //int max_avail = hltsv_is.AvailableCores ;
            //int cores_avail = hltsv_is.MaxAvailable ;
            long[] used_pages = hltsv_is.RNP_Used_pages ;
            final long max_pages = 4996 ; // some fixed number in hw or fw

            long max_used = 0;
            for (int i = 0; i<used_pages.length; i++ ) {
                if ( used_pages[i] > max_used ) max_used = used_pages[i] ;
            }
           
            // return Integer.toString(cores_avail) + "/" + Integer.toString(max_avail) ;
            return Long.toString(max_used*100/max_pages) + "%" ; // % occupancy
        }
        catch ( final is.InfoNotFoundException | is.RepositoryNotFoundException ex ) {
        }
        catch ( final Exception ex ) {
            ex.printStackTrace() ;
        }
        return "" ;
	}

	public static String getROIBBusy(final String partition) {
		if ( partition.equals("ATLAS") ) { return ROIB_Busy.getValue() ; }
		return "" ;
	}

// SFO occupancy per SFO
// DF_IS:HLT.SFO-15.Counters.Global[0]: EventsInsideAverage U32
// DF_IS:HLT.SFO-15.Input [1] WaitingEventsAverage U32
public static synchronized String getSFOOccupancy(final String partition, final String sfo) {
    is.AnyInfo sfo_counters = new is.AnyInfo() ;
    is.AnyInfo sfo_input = new is.AnyInfo() ;
    try {
        new is.Repository(new ipc.Partition(partition)).getValue("DF_IS:HLT." + sfo + ".Counters.Global", sfo_counters) ;
        new is.Repository(new ipc.Partition(partition)).getValue("DF_IS:HLT." + sfo + ".Input", sfo_input) ;
        int inside = ((Integer)sfo_counters.getAttribute(0)).intValue() ;
        int waiting = ((Integer)sfo_input.getAttribute(1)).intValue() ;
        
        return Integer.toString(inside) + "/" + Integer.toString(waiting) ;
    }
    catch ( final is.InfoNotFoundException | is.RepositoryNotFoundException ex ) {
    }
    catch ( final Exception ex ) {
        ex.printStackTrace() ;
    }
    return "" ;
}

// per-rack statistics: events inside/in output queue / PU crashes
// DF_IS:HLT.DefMIG-IS:HLT-28:tpu-rack-44.info  DCM schema EventsInside (25) EventsOnOutputQueue (26)
public static synchronized String getRackOccupancy(final String partition, final String rack) {
	DCM dcminfo = new DCM() ;
    try {
        new is.Repository(new ipc.Partition(partition)).getValue("DF_IS:HLT.DefMIG-IS:" + rack + ".info", dcminfo) ;
        int inside = dcminfo.EventsInside ;
        int output = dcminfo.EventsOnOutputQueue ;
        
        return Integer.toString(inside) + "/" + Integer.toString(output) ;
    }
    catch ( final is.InfoNotFoundException | is.RepositoryNotFoundException ex ) {
    }
    catch ( final Exception ex ) {
        ex.printStackTrace() ;
    }
    return "" ;
}

public static synchronized String getMotherInfo(final String partition, final String pu) {
	HLTMPPUMotherInfo mother_info = new HLTMPPUMotherInfo() ;
	// DF_IS:HLT-24:tpu-rack-55.HLTMPPU-24:HLT-24:tpu-rack-55:pc-tdq-tpu-55004.PU_MotherInfo
    try {
        new is.Repository(new ipc.Partition(partition)).getValue("DF_IS:" + pu + "PU_MotherInfo", mother_info) ;
        return Integer.toString(mother_info.NumActive) + "/" + Integer.toString(mother_info.NumExited) ;
    }
    catch ( final is.InfoNotFoundException | is.RepositoryNotFoundException ex ) {
    }
    catch ( final Exception ex ) {
        ex.printStackTrace() ;
    }
    return "" ;
}

public static String getMachineMode()  { // LHC.MachineMode DdcStringInfo
	is.AnyInfo mode = new is.AnyInfo() ;
	try {
        new is.Repository(new ipc.Partition("initial")).getValue("LHC.MachineMode", mode) ;
        return mode.getAttribute(1).toString() ;
    }
    catch ( final is.InfoNotFoundException | is.RepositoryNotFoundException ex ) {
    }
    catch ( final Exception ex ) {
        ex.printStackTrace() ;
    }
	return "?" ;
}

public static String getLCHPage1()  { // LHC.LHCPage1Msg DdcStringInfo
	is.AnyInfo msg = new is.AnyInfo() ;
	try {
        new is.Repository(new ipc.Partition("initial")).getValue("LHC.LHCPage1Msg", msg) ;
        return msg.getAttribute(1).toString() ;
    }
    catch ( final is.InfoNotFoundException | is.RepositoryNotFoundException ex ) {
    }
    catch ( final Exception ex ) {
        ex.printStackTrace() ;
    }
	return "" ;
}
public static String getBeamMode() { // LHC.MachineMode DdcStringInfo
	is.AnyInfo mode = new is.AnyInfo() ;
	try {
        new is.Repository(new ipc.Partition("initial")).getValue("LHC.BeamMode", mode) ;
        return mode.getAttribute(1).toString() ;
    }
    catch ( final is.InfoNotFoundException | is.RepositoryNotFoundException ex ) {
    }
    catch ( final Exception ex ) {
        ex.printStackTrace() ;
    }
	return "?" ;
}

public static String getStableBeam() { // LHC.MachineMode DdcStringInfo
	is.AnyInfo mode = new is.AnyInfo() ;
	try {
        new is.Repository(new ipc.Partition("initial")).getValue("LHC.StableBeamsFlag", mode) ;
		if ( ((Integer)mode.getAttribute(1)).intValue() == 1 ) { return "ON" ; }
		return "OFF" ;
    }
    catch ( final is.InfoNotFoundException | is.RepositoryNotFoundException ex ) {
    }
    catch ( final Exception ex ) {
        ex.printStackTrace() ;
    }
	return "?" ;
}

public static String getReady4Physics() { // RunParams.Ready4Physics Ready4PhysicsInfo
	is.AnyInfo r4p = new is.AnyInfo() ;
	try {
        new is.Repository(new ipc.Partition("ATLAS")).getValue("RunParams.Ready4Physics", r4p) ;
		if ( ((Boolean)r4p.getAttribute(2)).booleanValue() ) { return "YES" ; }
		return "NO" ;
    }
    catch ( final is.InfoNotFoundException | is.RepositoryNotFoundException ex ) {
    }
    catch ( final Exception ex ) {
        ex.printStackTrace() ;
    }
	return "?" ;
}

public static String getATLASReady(int cache_threshold) { // in seconds

	return ATLAS_status.getValue() ;
}
}