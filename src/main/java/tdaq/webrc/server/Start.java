package tdaq.webrc.server;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;

import org.eclipse.jetty.jmx.MBeanContainer;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.webapp.WebAppContext;


import tdaq.webrc.Configuration ;
/**
 * Jetty server
 */
public class Start
{
	/**
	 * Main function, starts the jetty server.
	 *
	 * @param args
	 */
	public static void main(String[] args)
	{
		// also awaiable in runtime -D or via web.xml
		// System.setProperty("wicket.configuration", "deployment");
		// System.setProperty("wicket.configuration", "development");

		Server server = new Server();

		HttpConfiguration http_config = new HttpConfiguration();
//		http_config.setSecureScheme("https");
//		http_config.setSecurePort(8443);
		http_config.setOutputBufferSize(32768);

		ServerConnector http = new ServerConnector(server, new HttpConnectionFactory(http_config));
		final String httpport = tdaq.webrc.Configuration.get("http.portnumber") ;
		int portnumber = 8888 ;
		if ( httpport != null ) {
			try {
				portnumber = Integer.parseInt(httpport) ;
			}
			catch (final NumberFormatException ex) {
				ex.printStackTrace() ;
				System.err.println("FATAL: Bad http.portnumber format: " + ex.getMessage()) ;
				System.exit(1) ;
			}
		}
		http.setPort(portnumber);
		// http.setIdleTimeout(1000 * 60 * 60); // ms, 1hr
		http.setIdleTimeout(1000 * 60 * 5); // 5 mins

		server.addConnector(http);

		WebAppContext bb = new WebAppContext();
		bb.setServer(server);
		if ( Configuration.get("application.context") != null ) {
			bb.setContextPath(Configuration.get("application.context")) ;
			System.err.println("Webapp context set to: " + Configuration.get("application.context")) ;
		}
		// bb.setWar("webrc-0.1.war") ;
		// bb.setWar("src/main/webapp") ;
		// bb.setWar("target/webrc-1.0.2") ;
		bb.setWar(".") ; // we need to cd to the webapp folder, copied from target/webrc-1.0.2

		// uncomment the next two lines if you want to start Jetty with WebSocket (JSR-356) support
		// you need org.apache.wicket:wicket-native-websocket-javax in the classpath!
		// ServerContainer serverContainer = WebSocketServerContainerInitializer.configureContext(bb);
		// serverContainer.addEndpoint(new WicketServerEndpointConfig());

		// uncomment next line if you want to test with JSESSIONID encoded in the urls
		// ((AbstractSessionManager)
		// bb.getSessionHandler().getSessionManager()).setUsingCookies(false);

		server.setHandler(bb);

		MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
		MBeanContainer mBeanContainer = new MBeanContainer(mBeanServer);
		server.addEventListener(mBeanContainer);
		server.addBean(mBeanContainer) ;

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() { 
				System.err.println("Interrupted, stopping Jetty server...") ;
				try {
					server.stop() ;
				} catch ( final Exception ex ) {
					System.err.println("Something went wrong in shutdown: " + ex.getMessage()) ;
					ex.printStackTrace(); 
					System.exit(66) ;
				}
			}
		 });

		try {
			server.start();
			server.join();
			System.err.println("Exiting Jetty, bye-bye") ;
		}
		catch (final Exception ex) {
			ex.printStackTrace();
			System.err.println("Interrupted with Exception, Exiting Jetty: " + ex.getMessage()) ;
			System.exit(100);
		}
	}
}
