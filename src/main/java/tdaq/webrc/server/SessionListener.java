package tdaq.webrc.server ;

import javax.servlet.http.HttpSessionListener ;
import javax.servlet.http.HttpSessionEvent ;

public class SessionListener implements HttpSessionListener {
    @Override
    public void sessionCreated(HttpSessionEvent se) {
        ers.Logger.debug(1, "HTTP Session created: " + se.getSession().getId() ) ;
        // tdaq.webrc.HomePage.addSession() ;
        // System.err.println("Context path:" + se.getSession().getServletContext() ) ;
        
        se.getSession().setMaxInactiveInterval(60*60*4) ; // 4 hrs
        //se.getSession().setMaxInactiveInterval(1) ; // 4 hrs
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        ers.Logger.debug(1, "HTTP Session destroyed: " + se.getSession().getId() );
        tdaq.webrc.HomePage.removeSession(se.getSession().getId()) ;
    }
}
    