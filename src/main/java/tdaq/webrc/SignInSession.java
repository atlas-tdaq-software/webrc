package tdaq.webrc;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.http.WebRequest ;
import org.apache.wicket.request.cycle.RequestCycle ;
import org.apache.hc.client5.http.classic.methods.HttpPost ;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient ;
import org.apache.hc.client5.http.impl.classic.HttpClients ;
import org.apache.hc.core5.http.message.StatusLine ;
import org.apache.hc.core5.http.io.entity.EntityUtils ;
import org.apache.hc.core5.http.NameValuePair ;
import org.apache.hc.core5.http.message.BasicNameValuePair ;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity ;

import daq.tokens.JWToken ;
import daq.tokens.VerifyTokenException ;

public final class SignInSession extends AuthenticatedWebSession {
    
    private static final long serialVersionUID = 3461763584170512489L;
    private String userid;
    private String email;
	private ArrayList<String> groups = new ArrayList<String>() ; // CERN SSO auth e-groups
	private String userfullname = null ;
    private String preauth_user = null ;
    private String access_token = null ;
    private String tdaq_token = null ;
    private String refresh_token = null ;
    private Request http_request = null ;

    private long daq_token_expire ; // Unix time in seconds, as returned in "exp" field in JWT
    
    public SignInSession(Request request) {
        super(request) ;
        /*
        if( request instanceof WebRequest ) {
            final WebRequest wr = (WebRequest) request;
            // this is the real thing
			javax.servlet.http.HttpServletRequest hsr = null ;
			Object ttt = wr.getContainerRequest();
			if ( ttt instanceof javax.servlet.http.HttpServletRequest ) {
				hsr = (javax.servlet.http.HttpServletRequest)ttt ;
				System.err.println("SignInSession: HTTP Request Headers:");
				for ( final String hdr: java.util.Collections.list(hsr.getHeaderNames()) ) {
					System.err.println("Header " +  hdr + ":" + hsr.getHeader(hdr));
                }
			}
        }
        */
    }

    public String getUserFullName() {
        return userfullname ;
    }

    public String getUserEMail() {
        return email ;
    }

    public String getUser() {
        return userid;
    }

	public ArrayList<String> getGroups() {
        return groups;
    }
     
    /** first instance we get from the HTTP header */
    private void getAccessToken() {

        final Request request = RequestCycle.get().getRequest() ;
            ers.Logger.info("getting SSO access token from request " + request) ;

            if ( request instanceof WebRequest ) {
                final WebRequest wr = (WebRequest) request;
                Object ttt = wr.getContainerRequest();
                if ( ttt instanceof javax.servlet.http.HttpServletRequest ) {
                    javax.servlet.http.HttpServletRequest hsr = (javax.servlet.http.HttpServletRequest)ttt ;
                
                    access_token = hsr.getHeader("X-Forwarded-Access-Token") ; 
                    if ( access_token == null ) {
                        ers.Logger.log("No X-Forwarded-Access-Token in the headers") ;
                    } else {
                        ers.Logger.log("Got X-Forwarded-Access-Token: " + access_token) ;
                    }
                }
            }
            if ( access_token == null ) {
                ers.Logger.error(new RuntimeException("Failed to get SSO access token from http request headers")) ;
            }
        return ;
    }


    // otherwise, check the token expiration and refresh both access_ and refresh_ token from refresh_token
    private void refreshTdaqToken() {
        try {
            getAccessToken() ; // try to refresh from geaders?
            
            exchangeTdaqToken() ;

            return ;
/*

access token refresh from refresh token seems to be broken when two applications are involved

https://github.com/keycloak/keycloak/issues/8756#issuecomment-1177481542


            if ( refresh_token == null  ){
                exchangeTdaqToken() ;
                if ( refresh_token == null  ) {
                    ers.Logger.error(new RuntimeException("No refresh token")) ;
                    return ;
                }
            }
            
            ers.Logger.log("Getting new access_token (tdaq) from refresh_token") ;

            final CloseableHttpClient httpclient = HttpClients.createDefault() ;
            final HttpPost httppost = new HttpPost(Configuration.get("cern.sso.openid.url")) ;
            final List<NameValuePair> params = new ArrayList<NameValuePair>();
            // params.add(new BasicNameValuePair("client_id", Configuration.get("cern.sso.client_id")));
            params.add(new BasicNameValuePair("client_id", "atlas-tdaq-token"));
            params.add(new BasicNameValuePair("grant_type", "refresh_token"));
            params.add(new BasicNameValuePair("refresh_token", refresh_token)) ;

            httppost.setEntity(new UrlEncodedFormEntity(params)) ;

            ers.Logger.log("Executing request " + httppost.getMethod() + "://" + httppost.getUri()) ;

            httpclient.execute(httppost, response -> {
                System.out.println("Response status: " + response.getCode() + " " + response.getReasonPhrase()) ;
                String res = EntityUtils.toString(response.getEntity()) ;
                System.out.println(res) ;
                if ( response.getCode() != 200 ) {
                    System.out.println("Failed to get access token from the refresh claim. Response body:\n" + res) ;
                    access_token = null ;
                    return null ;
                }
                int beg = res.indexOf("\"access_token\":\"", 0) ;
                access_token = res.substring(beg+16, res.indexOf('"', beg+16)) ;
                ers.Logger.debug(0, "access_token: " + access_token) ;
                beg = res.indexOf("\"refresh_token\":\"", 0) ;
                refresh_token = res.substring(beg+17, res.indexOf('"', beg+17)) ;
                ers.Logger.debug(0, ("refresh_token: " + refresh_token)) ;
                EntityUtils.consume(response.getEntity());
                return null ;
            });                
         */
        }
        catch (final Exception ex) {
            ers.Logger.error(new RuntimeException("Failed in HTTP POST", ex)) ;
            access_token = null ;
        }
    }
 

/** 
 * Gets fresh daq token in HTTP POST token-exchage from auth.cern.ch
 * 
 * from Reiner's example https://gitlab.cern.ch/rhauser/oauth-example
 */
    private boolean exchangeTdaqToken() {

        if ( access_token == null ) {
            // System.err.println("getFreshAccessToken: No fresh access token in current request headers") ;
            tdaq_token = null ;
            return false ;
        }

        try {
            final CloseableHttpClient httpclient = HttpClients.createDefault() ;
            final HttpPost httppost = new HttpPost(Configuration.get("cern.sso.openid.url")) ;
            final List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("client_id", Configuration.get("cern.sso.client_id")));
            params.add(new BasicNameValuePair("client_secret", Configuration.get("cern.sso.client_secret"))); 
            params.add(new BasicNameValuePair("grant_type", "urn:ietf:params:oauth:grant-type:token-exchange"));
            params.add(new BasicNameValuePair("audience", "atlas-tdaq-token")) ;
            params.add(new BasicNameValuePair("subject_token", access_token)) ;
            params.add(new BasicNameValuePair("requested_token_type", "urn:ietf:params:oauth:token-type:refresh_token"));

            httppost.setEntity(new UrlEncodedFormEntity(params)) ;

            ers.Logger.log("Executing request " + httppost.getMethod() + "://" + httppost.getUri()) ;

            httpclient.execute(httppost, response -> {
                System.out.println("Response status: " + response.getCode() + " " + response.getReasonPhrase()) ;
                String res = EntityUtils.toString(response.getEntity()) ;
                
                if ( response.getCode() != 200 ) {
                    ers.Logger.error(new RuntimeException("Failed to get access token. Response body:\n" + res)) ;
                    tdaq_token = null ;
                    return false ;
                }
                
//                String[] parts = res.split("\\.") ;
//                javax.json.JsonReader reader = javax.json.Json.createReader();
     
 //               JsonObject personObject = reader.readObject();

                int beg = res.indexOf("\"access_token\":\"", 0) ;
                tdaq_token = res.substring(beg+16, res.indexOf('"', beg+16)) ;
                ers.Logger.debug(1, "tdaq_token: " + tdaq_token) ;
                //beg = res.indexOf("\"refresh_token\":\"", 0) ;
                //refresh_token = res.substring(beg+17, res.indexOf('"', beg+17)) ;
                //System.out.println("refresh_token: " + refresh_token) ;
                
                // ,"expires_in":1199, but Reiner in aquire() makes it 600 only, so we need to refresh every 10 minutes
                // https://gitlab.cern.ch/atlas-tdaq-software/daq_tokens/-/blob/master/src/acquire.cpp#L52
  //              daq_token_expire = java.time.Instant.now().getEpochSecond() + 590 ; // TODO get it from header
    
                try {
                    java.util.Map<String, Object> props = JWToken.verify(tdaq_token) ;
                    Object roles = props.get("cern_roles") ;
                    groups = (java.util.ArrayList<String>)roles ;
                    ers.Logger.log("CERN groups/roles: " + groups) ;
                    userfullname = (String)props.get("name") ;
                    ers.Logger.log("Full name: " + userfullname) ;
                    daq_token_expire = ((Double)props.get("exp")).longValue() ;
                    
                    ers.Logger.log("tdaq_token expires in: " + (daq_token_expire - java.time.Instant.now().getEpochSecond()) + " from now" ) ;
                }
                catch ( final VerifyTokenException | NumberFormatException ex ) {
                    ers.Logger.error(ex) ;
                    tdaq_token = null ;
                }
                catch ( final Exception ex ) { // may be still good tocen?
                    ers.Logger.error(ex) ; 
                    ex.printStackTrace() ;
                }
                EntityUtils.consume(response.getEntity());
                return true ;
            });
            
        } catch (final Exception ex) {
            ers.Logger.error(new RuntimeException("Failed in HTTP POST", ex)) ;
            // ex.printStackTrace() ;
            tdaq_token = null ;
        }

        if ( tdaq_token == null ) {
            return false ;
        }

        return true ;
    }

    /**
     * checks expiration of the token returns it after refreshing if necessary
     */
    public String getTdaqToken() {

        if ( tdaq_token != null && java.time.Instant.now().getEpochSecond() < daq_token_expire - 30 ) { // at least 30 seconds before expiration
            ers.Logger.debug(0, "Returning daq token not expired yet") ;
            return tdaq_token ;
        }
    
        ers.Logger.info("Need to refresh expired daq token") ;
        refreshTdaqToken() ; 

        /* Does not really work, so above we get fresh sso access token and fresh tdaq_token via token-exchange
        try {
                final CloseableHttpClient httpclient = HttpClients.createDefault() ;            
                final HttpPost httppost = new HttpPost(Configuration.get("cern.sso.openid.url"));
                final List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("client_id", Configuration.get("cern.sso.client_id")));
                //params.add(new BasicNameValuePair("client_secret", Configuration.get("cern.sso.client_secret"))); // TODO hide in properties file
                params.add(new BasicNameValuePair("grant_type", "refresh_token"));
                // params.add(new BasicNameValuePair("audience", "atlas-tdaq-token"));
                params.add(new BasicNameValuePair("refresh_token", refresh_token));
                // params.add(new BasicNameValuePair("requested_token_type", "urn:ietf:params:oauth:token-type:refresh_token"));

                httppost.setEntity(new UrlEncodedFormEntity(params)) ;

                ers.Logger.log("Executing request " + httppost.getMethod() + " " + httppost.getUri() + httppost.getHeaders()) ;

                httpclient.execute(httppost, response -> {
                    ers.Logger.log("Response status: " + response.getCode() + " " + response.getReasonPhrase());
                    String res = EntityUtils.toString(response.getEntity()) ;
                    System.out.println("Response body: " + res) ;
                    int beg = res.indexOf("\"access_token\":\"", 0) ;
                    tdaq_token = res.substring(beg+16, res.indexOf('"', beg+16)) ;
                    System.out.println("tdaq_token: " + tdaq_token) ;
                    beg = res.indexOf("\"refresh_token\":\"", 0) ;
                    refresh_token = res.substring(beg+17, res.indexOf('"', beg+17)) ;
                    System.out.println("refresh_token: " + refresh_token) ;
                    EntityUtils.consume(response.getEntity());
                    return null ;
                });
                
            } catch (final Exception ex) {
                ers.Logger.error(new ers.Issue("Failed in HTTP POST to " + Configuration.get("cern.sso.openid.url"), ex)) ;
            }
            */
        return tdaq_token ;
    }
    
    @Override
    public Roles getRoles() {
        return new Roles();
    }

    @Override
    public final boolean authenticate(final String username, final String password) {
        ers.Logger.log("Authenticating") ;
        // System.err.println("SignInSession: stored HTTP Request " + http_request);

        final Request another_request = RequestCycle.get().getRequest() ;
        // System.err.println("SignInSession: fresh HTTP Request " + another_request);

        preauth_user = getPreAuthUser(another_request) ;
        
        if ( preauth_user != null ) {
            ers.Logger.log("Authenticated from SSO: " + preauth_user) ;
            if ( userfullname == null ) {
                System.err.println("Getting LDAP fullname from principal name " + preauth_user) ;
                LdapAuth ldap = new LdapAuth() ;
                try {
                    ldap.getFromPrincipalName(preauth_user) ;
                    userid = ldap.getUserId() ;
                    userfullname = ldap.getUserFullName() ;
                    email = ldap.getUserEMail() ;
                } catch (final Exception ex) {
                    ex.printStackTrace() ;
                }
            }
    
            return true ;
        } 

        System.err.println("SignInSession$authenticate: Proceeding with LDAP authentication for " + username) ;
            
        LdapAuth ldap = new LdapAuth();

        if( ldap.authentify(username, password) ) {
            userid = username ;
            userfullname = ldap.getUserFullName() ;
            email = ldap.getUserEMail() ;
            System.err.println("LDAP authentication OK for " + username );
            return true ;
        } else {
            System.out.println("LDAP authentication Failed for " + username) ;
            return false ;
        }
    }

	/** aka destructor */
	@Override
	protected void finalize() throws Throwable {
		System.err.println("Destroying SignInSession");
	}
    
    private String getPreAuthUser(final Request request){
        if( request instanceof WebRequest ){
            final WebRequest wr = (WebRequest) request;
            // this is the real thing
			Object ttt = wr.getContainerRequest();
			if ( ttt instanceof javax.servlet.http.HttpServletRequest ) {
                javax.servlet.http.HttpServletRequest hsr = (javax.servlet.http.HttpServletRequest)ttt ;

                final String forwusergroups = hsr.getHeader("X-Forwarded-Groups") ; // set by new CERN SSO, see https://paas.docs.cern.ch/4._CERN_Authentication/2-deploy-sso-proxy/#which-http-headers-are-passed-to-my-application
				if ( forwusergroups != null ) {
                    System.err.println("SignSession: HTTP Request Forwarded Groups: " + forwusergroups) ;
					groups = new ArrayList<String>(java.util.Arrays.asList(forwusergroups.split("\\s*,\\s*"))) ;
					// then you can deside basing on the SSO group info, what is permitted for this user
                }

				// either Andrei.Kazarov@cern.ch from CERN IT or akazarov from atlasop
                String forwlogin = hsr.getHeader("X-Forwarded-login") ;// akazarov from atlasop
				if ( forwlogin == null ) { // CERN new SSO
					forwlogin = hsr.getHeader("X-Forwarded-User") ;
					email = hsr.getHeader("X-Forwarded-Email") ;
				}
                
                final String access_token = hsr.getHeader("X-Forwarded-Access-Token") ; // X-Forwarded-Access-Token
                if ( access_token == null ) {
                    System.err.println("SignSession: No X-Forwarded-Access-Token in the headers") ;
                } else {
                    // System.err.println("SignSession: Got X-Forwarded-Access-Token:" + access_token) ;
                    ers.Logger.log("Got X-Forwarded-Access-Token") ;
                    ers.Logger.log("TDAQ token enabled: " + JWToken.enabled());
                }
                final String forwusername = hsr.getHeader("X-Forwarded-fullname") ;// Andrei Kazarov TODO get it from access_token as it seems not forwarded from SSO proxy
                if ( forwlogin != null ) {
                    userid = forwlogin ;
                    userfullname = ( forwusername == null ? forwlogin : forwusername ) ;
                    System.err.println("SignSession: HTTP Request Forwarded User: " + userid + "(" + userfullname + ")") ;
                    return forwlogin ;
                }

                return hsr.getHeader("X-Remote-User") ;
		    }
        System.err.println("Failed to get WebRequest from Request :(");
        }
    return null;
    }
}
