package tdaq.webrc ;

import java.util.Map ;
import java.util.List ;
import java.util.concurrent.ConcurrentHashMap ;
import java.util.concurrent.CopyOnWriteArrayList ;
import java.util.concurrent.ExecutorService;

import pmg.PMGPublishedProcessDataNamed;

/**
 static shared receiver of RC IS updates
 single subscription per partition
 keep list of all subscribers, notifies them once IS callback is recieved */
public interface ISReciever {

final static String rc_is_server_name = "RunCtrl" ;
final static String pmg_is_server_name = "PMG" ;
final static String rc_appinfo_prefix = "Supervision." ;
final static int rc_is_server_name_size = 7 ; // want rc_is_server_name.length
final static is.Criteria rc_is_server_subsc = new is.Criteria( java.util.regex.Pattern.compile(".*") );

static Map<String, List<RCTreeNodeProvider>> receivers = new ConcurrentHashMap<String, List<RCTreeNodeProvider>>() ;
static Map<String, is.InfoWatcher> watchers = new ConcurrentHashMap<String, is.InfoWatcher>() ; // holder of IS watchers

static ThreadLocal<rc.DAQApplicationInfo> DAQ_INFO = new ThreadLocal<rc.DAQApplicationInfo>() {
    @Override protected rc.DAQApplicationInfo initialValue() {
        return new rc.DAQApplicationInfo() ;
    };
} ;
static ThreadLocal<rc.RCStateInfo> RC_INFO = new ThreadLocal<rc.RCStateInfo>() {
    @Override protected rc.RCStateInfo initialValue() {
        return new rc.RCStateInfo() ;
    };
} ;

// thread pool to handle tasks of getting IS callbacks and updating RCNodes
final static ExecutorService executor = java.util.concurrent.Executors.newFixedThreadPool(Configuration.getInt("is.executor.threads")) ;

static public rc.DAQApplicationInfo getDAQAppInfo(String partition, String appame) {
    rc.DAQApplicationInfo info = new rc.DAQApplicationInfo() ;
    try {
        new is.Repository(new ipc.Partition(partition)).getValue(rc_is_server_name + "." + rc_appinfo_prefix + appame, info) ;
    }
    catch ( final is.InfoNotFoundException ex ) {
        // OK
        ers.Logger.debug(1, ex);
        return null ;  // caller must threat this as no application info published, i.e. it is not running = ABSENT
    }
    catch ( final is.RepositoryNotFoundException ex ) {
        // OK, no partition: need to empty RC tree
        // but also may be just a glich, so better to ignore...
        ers.Logger.debug(1, "Failed to get DAQ info from IS: " + ex.getMessage()) ;
        // RCNodesHolder.clearPartition(partition) ;
    }
    catch ( final Exception ex ) { // RepositoryNotFoundException InfoNotFoundException InfoNotCompatibleException
        ex.printStackTrace(); // better?
    }
    return info ;
}

static public rc.RCStateInfo getRCInfo(String partition, String rcappame) {
    rc.RCStateInfo info = new rc.RCStateInfo() ;
    try {
        new is.Repository(new ipc.Partition(partition)).getValue(rc_is_server_name + "." + rcappame, info) ;
    }
    catch ( final is.InfoNotFoundException ex ) {
        ers.Logger.debug(1, "There is no RCCtrl IS yet in partition " + partition + ": " + ex) ;
        // OK, application may have exited, return default info
    }
    catch ( final is.RepositoryNotFoundException ex ) {
        // OK, no partition: need to empty RC tree

        ers.Logger.debug(1, ex.getMessage() + ": Partition " + partition + " seems to be gone? Expect callback from PMG for RootController");
        
        // this is moved to Home page which subscribes in PMG for RootController to exit
        /*
        RCNodesHolder.clearPartition(partition) ;
        List<RCTreeNodeProvider> list = receivers.get(partition) ;
        if ( list != null ) {
            list.forEach(r -> r.partitionGone() ) ;
        } */ 
    }
    catch ( final Exception ex ) { // RepositoryNotFoundException InfoNotFoundException InfoNotCompatibleException
        ex.printStackTrace(); // better?
    }
    return info ;
}

// if subscription in this partition already exists, add to the Map
static void subscribe(final String partname, final RCTreeNodeProvider receiver)
throws is.RepositoryNotFoundException, is.InvalidCriteriaException, is.AlreadySubscribedException {

    List<RCTreeNodeProvider> newlist = new CopyOnWriteArrayList<RCTreeNodeProvider>()  ; // maybe
    List<RCTreeNodeProvider> exists = receivers.putIfAbsent(partname, newlist) ;
    if ( exists != null) { 
        exists.add(receiver) ;
        return ;
    }

   // subscribe in IS, store new is.Watcher
    is.InfoWatcher iswatcher =
    new is.InfoWatcher(new ipc.Partition(partname), rc_is_server_name, rc_is_server_subsc) {
        String partition = new String(partname) ;
        @Override
        public void infoSubscribed(final is.InfoEvent info) {
            //System.err.println("InfoSubscribed: " + info.getName());
            ISReciever._isInfoUpdated(info, partition, false);
        }

        @Override
        public void infoCreated(final is.InfoEvent info) {
            //System.err.println("InfoCreated: " + info.getName());
            ISReciever._isInfoUpdated(info, partition, false);
        }

        @Override
        public void infoDeleted(final is.InfoEvent info) {
            ISReciever._isInfoUpdated(info, partition, true);
        }

        @Override
        public void infoUpdated(final is.InfoEvent info) {
            //System.err.println("InfoUpdated: " + info.getName());
            ISReciever._isInfoUpdated(info, partition, false);
        }
    } ;
    //System.err.println("Subscribing in IS " + partname + ":" + rc_is_server_name) ;
    iswatcher.subscribe() ;
    newlist.add(receiver) ;
    watchers.put(partname, iswatcher) ;
    ers.Logger.debug(0, "Subscribed in IS " + partname + ":" + rc_is_server_name +
    				", total subscribers: " + receivers.get(partname).size()) ;

} // subscribe

static void unsubscribe(final String partname, final RCTreeNodeProvider receiver) {
    boolean success = false ;
    try {
        List<RCTreeNodeProvider> recs = receivers.get(partname) ;
        if ( recs == null ) {
            System.err.println("No IS receivers registered in partition " + partname) ;
            return ;
        }
        ers.Logger.log("Receivers in partition " + partname + ":" + recs.size()) ;
        for ( RCTreeNodeProvider r: recs ) {
            if ( r==receiver ) {
                ers.Logger.debug(0, "ISReciever found in list") ;
                if ( recs.remove(r) ) {
                    ers.Logger.debug(0, "ISReciever removed from list") ;
                    success = true ;
                    if ( recs.size() == 0 ) {
                        try {
                            watchers.get(partname).unsubscribe() ;
                        } catch ( final is.RepositoryNotFoundException ex ) { }
                        watchers.remove(partname) ;
                        receivers.remove(partname) ;
                        ers.Logger.debug(0, "No more receivers, unsubscribed in IS in partition " + partname) ;
                    } else {
                        ers.Logger.debug(0, recs.size() + " subscribers left in " + partname) ;
                    }
                } else {
                    throw new Exception("Unexpected: failed to remove from ArrayList") ;
                }
            }
        }
        if (!success) {
            ers.Logger.log("Did not find receiver to unsubscribe in partition " + partname) ;
        }
        
    } catch ( final Exception ex ) {
        ex.printStackTrace();
    }
}

public static PMGPublishedProcessDataNamed getProcessInfo(final String partname, final String host, final String appname)
throws is.InfoNotFoundException, is.RepositoryNotFoundException {
    final PMGPublishedProcessDataNamed processInfo =
    new PMGPublishedProcessDataNamed(new ipc.Partition(partname), pmg_is_server_name + "." + host + "|" + appname) ;
    try {
        processInfo.checkout();
    } catch ( final is.InfoNotFoundException | is.RepositoryNotFoundException ex ) {
        throw ex ;
    } 
    catch( final Exception ex ) {
        ex.printStackTrace() ;
    }
    ers.Logger.debug(2,"Got PMGPublishedProcessData for " + partname + pmg_is_server_name + "." + host + "|" + appname) ;
    ers.Logger.debug(2,"std err for " + processInfo.app_name + ":" + processInfo.std_err) ;
    ers.Logger.debug(2,"std out for " + processInfo.app_name + ":" + processInfo.std_out) ;
    return processInfo;
}

static void exitCleanup() {
    System.err.println("Cleanin up IS subscriptions");
    for ( is.InfoWatcher watch: watchers.values() ) {
        try {
            watch.unsubscribe() ; 
        }
        catch ( final is.RepositoryNotFoundException ex ) {}
        catch ( final is.SubscriptionNotFoundException ex ) {}
        catch ( final Exception ex ) {
            ex.printStackTrace() ;
        }        
    }
}

// called from IS callbacks, in turn call real subscribers
// update global NodesHolder
// call setUpdated for each in RCTreeNodeProvider List
static void _isInfoUpdated(final is.InfoEvent info, final String partition, boolean deleted) {

    executor.execute( () -> {
    try {
        final String fullInfoName = info.getName();
        final is.Type infoType = info.getType();
        RCNode node = null ; 
        
        if( infoType.equals(rc.DAQApplicationInfo.type) ) {
            final rc.DAQApplicationInfo daqApplicationInfo = DAQ_INFO.get();            
            // rc.DAQApplicationInfo daqApplicationInfo = new rc.DAQApplicationInfo() ;
            info.getValue(daqApplicationInfo);
            node = RCNodesHolder.findNode(partition, daqApplicationInfo.applicationName) ;
            if ( node == null ) {
                return ; // not loaded, update info irrelevant
            }
            if ( deleted ) {
                node.updateState(null, null, 0L) ;
            } else {
                node.updateState(daqApplicationInfo, null, info.getTime().getTimeMicro()) ;
            }
        } else if( infoType.equals(rc.RCStateInfo.type) ) {
            final String controllerName = fullInfoName.substring(rc_is_server_name_size + 1);
            node = RCNodesHolder.findNode(partition, controllerName) ;
            if ( node == null ) return ; // not loaded, update info irrelevant          
            if ( deleted ) {
                node.updateState(null, null, 0L) ;
            } else {
                // rc.RCStateInfo rcInfo = new rc.RCStateInfo() ;
                final rc.RCStateInfo rcInfo = RC_INFO.get() ;
                info.getValue(rcInfo) ;
                boolean res = node.updateState(null, rcInfo, info.getTime().getTimeMicro()) ;

                if ( res && controllerName.equals("RootController") ) {
                    // System.err.println("RootController state updated for partition " + partition);
                    updateRoots(partition, rcInfo.state, rcInfo.busy) ;

                    // this is done now in RCTreeProvider in callback from PMG client for RootController
                    /* if ( rcInfo.lastCmdName.equals("EXIT") ) {
                        ers.Logger.log("Partition is shutting down: " + partition) ;
                        ConfigProvider.clearConfig(partition) ;
                        RCNodesHolder.clearPartition(partition) ;
                    } */
                } 
            }
            //System.err.println("RCState updated for " + controllerName);
        }
        else {
            // System.err.println("Unexpected IS type from RunCtrl: " + infoType.getName());
            return ;
        }

        final List<RCTreeNodeProvider> list = receivers.get(partition) ;
        if ( list == null ) return ; // possible?
        list.forEach(r -> r.setUpdated()) ;
        //for (final RCTreeNodeProvider prov: receivers.get(partition)) {
        //   prov.nodeUpdated(node) ;
        //}
    } catch (final Exception ex) {
        ex.printStackTrace() ;
    }
    }) ;
}

static void updateRoots(final String partition, final String rcstate, boolean busy) {
    List<RCTreeNodeProvider> list = receivers.get(partition) ;
    if ( list == null ) return ;
    list.forEach(r -> r.rootStateChanged(rcstate, busy)) ;
}


} // class