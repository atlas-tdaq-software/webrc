package tdaq.webrc ;

import java.util.Vector ;
import java.util.List ;
import java.util.function.BiPredicate ;
import java.util.stream.Stream ;
import java.util.stream.Collectors ;
import java.nio.file.Path ;
import java.nio.file.Paths ;
import java.nio.file.Files ;
import java.nio.file.attribute.BasicFileAttributes ;

import ipc.*;

/** 
 * IPC gets initial reference from $TDAQ_IPC_INIT_REF and from System.getProperty(
	    "tdaq.ipc.init.ref") - we will set this property in main to say
corbaloc:iiop:10.193.6.75:43609/%ffipc/partition%00initial/ipc/partition/initial
which is get from 
 ipc_mk_ref -H 10.193.6.75 -P 43609 
 (for tbed tdaq-08-03-01), for P1 is different
*/
class Partitions
{
    private Partitions() {
    }
    
    private final static String super_partition = "ATLAS" ;
    private static int hash = 0 ;

    public static List<String> getDBPartitions() {
        
        List<String> ret = new Vector<String>() ;
        final String dbpath = Configuration.get("dbroot.path") ;// e.g. /tbed/git/oks/tdaq-11-02-00
        if ( dbpath == null ) {
            ret.add("No path found in property dbroot.path") ;
            return ret ;
        }

        //final String[] paths = dbpath.split("[,;:]") ;
        
        BiPredicate<Path,BasicFileAttributes> part_filter = (path, attrs) -> { return path.toString().matches(".+\\/partitions\\/.+\\.data\\.xml") && attrs.isRegularFile() ; } ;

        //RCCommandSender.setToken();
        try ( Stream<Path> walk = Files.find(Paths.get(dbpath), 3, part_filter) ) {
            ret = walk.map(x -> x.toString()).map(y -> y.substring(dbpath.length()+1)).collect(Collectors.toList());
        }
        catch ( final java.io.IOException | java.lang.SecurityException ex ) {
            ers.Logger.error(new ers.Issue("Check dbroot.path property, can not read partitions DB list", ex)) ;
            // ex.printStackTrace();
        }

        //RCCommandSender.clearToken() ;
        ers.Logger.debug(0, "Got partitions list: " + ret) ;
        return ret;
    }

    public static List<String> getIPCPartitionNames(boolean partitions_only) {
   
        List<String> ret = new Vector<String>() ;
        PartitionEnumeration partitions_enum ;
        try {
            partitions_enum = new PartitionEnumeration() ;
        }
        catch ( final InvalidPartitionException ex ) {
            ret.add(ex.message()) ;
            return ret ;
        }
    
        while ( partitions_enum.hasMoreElements() ) {
            final ipc.Partition p = partitions_enum.nextElement() ;
            if ( p.isValid() ) {
                try {
                    final String owner = HomePage.getPartitionOwner(p.getName()) ;
                    final String toadd ;
                    if ( !partitions_only ) {
                        toadd = p.getName() + " [" + owner + "]" ;
                    } else {
                        toadd = p.getName() ;
                    }
                    if ( p.getName().equals(super_partition) ) {
                        ret.add(0, toadd) ;
                    } else {
                        ret.add(toadd) ;
                    }

                } catch ( final Exception ex ) {
                    continue ; // RootControler is not yet there, do not add such incomlete partition
                }               
            }
        }

        String joined = String.join("", ret ) ;
        hash = joined.hashCode() ; 
        
        ret.add("initial");
        return ret ;
    }

    public static int hash() {
        return hash ;
    }
}