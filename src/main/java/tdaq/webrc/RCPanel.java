package tdaq.webrc ;

import org.apache.wicket.request.mapper.parameter.PageParameters ;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.extensions.markup.html.repeater.tree.DefaultNestedTree;
import org.apache.wicket.extensions.markup.html.repeater.tree.NestedTree;
import org.apache.wicket.ajax.AbstractAjaxTimerBehavior;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.util.time.Duration;

public class RCPanel extends WebPage implements IHomePage { 

	final String partition ;
	final String default_partition = "ATLAS" ;
	final int default_update_interval = 2 ; // seconds

	String gone_partition ;
	int partitionsHash = 0 ;

	RCTreeNodeProvider rctreeprov = null ;
	NestedTree<RCNode> tree = null ;

	public RCPanel(final PageParameters parameters) {
		super(parameters) ;

		setVersioned(false) ;

		add(new org.apache.wicket.ajax.AjaxNewWindowNotifyingBehavior() {

			private static final long serialVersionUID = 4564564564L;

			@Override
			protected void onNewWindow(AjaxRequestTarget target) {
				System.err.println("The same page rendering " + getPageClass().toString()) ;
				target.appendJavaScript(";alert('You opened a new browser tab to share the same WebRC session: this is UNSUPPORTED.\\n" +
				"Please close this tab and start a new Private browser window.');");
				// setResponsePage(getPageClass(), getPageParameters()) ;
			}

		}) ;

		partition = parameters.get("partition").toString(default_partition) ;
		gone_partition = "" ;
		rctreeprov = new RCTreeNodeProvider(this) ;
		try {
			ConfigProvider.loadDB(partition) ;
			System.err.println("Configuration updated, tree is to be reloaded");

			rctreeprov.partitionChanged(partition);

		} catch ( final Exception ex ) {
			ex.printStackTrace() ;
		}

		tree = new DefaultNestedTree<RCNode>("rctree", rctreeprov) {

			private static final long serialVersionUID = 456455664561L;

			@Override
			protected Component newContentComponent(String id, IModel<RCNode> node) {
				
				AbstractLink ret = new AjaxLink<RCNode>(id, node) {
					
					private static final long serialVersionUID = 16574567567L;
					private transient StringBuffer body ;
					
					@Override
					public void onComponentTagBody(org.apache.wicket.markup.MarkupStream markupStream, org.apache.wicket.markup.ComponentTag openTag) {

						final String busy = node.getObject().getCTPBusy(CTPBusyInfo.BUSY_TYPE.INST) ;
						final String name = node.getObject().getName() ;
						if ( name.equals("HLT") ) {
							body = new StringBuffer(getDefaultModelObjectAsString());
							body.append("<span class=\"farmavailability\" title=\"Farm availability (%) N of available/free/total PUs\">")
							.append(busy).append("</span>") ;
							replaceComponentTagBody(markupStream, openTag, body) ;
						} else
/* 						if ( name.contains("HLTMPPU") ) {
							body = new StringBuffer(getDefaultModelObjectAsString());
							body.append("<span class=\"farmavailability\" title=\"N of active/exited PUs\">")
							.append(busy).append("</span>") ;
							replaceComponentTagBody(markupStream, openTag, body) ;
						} else */
						if ( name.startsWith("HLT-") ) {
							body = new StringBuffer(getDefaultModelObjectAsString());
							body.append("<span class=\"farmavailability\" title=\"Rack occupancy: N of events inside/in output\">")
							.append(busy).append("</span>") ;
							replaceComponentTagBody(markupStream, openTag, body) ;
						} else
						if ( name.equals("HLTSV") ) {
							body = new StringBuffer(getDefaultModelObjectAsString());
							body.append("<span class=\"farmavailability\" title=\"HTLSV occupancy: % of used RobinNP pages (max over all channels)\">")
							.append(busy).append("</span>") ;
							replaceComponentTagBody(markupStream, openTag, body) ;
						} else
						if ( name.startsWith("SFO-") ) {
							body = new StringBuffer(getDefaultModelObjectAsString());
							body.append("<span class=\"sfooccupancy\" title=\"SFO occupancy: N of events expected/inside\">")
							.append(busy).append("</span>") ;
							replaceComponentTagBody(markupStream, openTag, body) ;
						} else
						if ( ! busy.equals("0.00") ) {
							body = new StringBuffer(getDefaultModelObjectAsString());
							if ( node.getObject().isRoot(name) ) {
								if ( busy.startsWith("ON HOLD") && node.getObject().getRCState().equals("RUNNING") ) {
									body.append("<span class=\"ctpbusy-onhold\" title=\"CTP total busy (Simple/Complex deadtime)\">") ;
								}
								body.append("<span class=\"ctpbusy\" title=\"CTP total busy (Simple/Complex deadtime)\">") ;
							} else {
								body.append("<span class=\"subdetbusy\" title=\"Subdetector busy\">") ;
							}
							body.append(busy).append("</span>") ;
							replaceComponentTagBody(markupStream, openTag, body) ;
						} else {
							super.onComponentTagBody(markupStream, openTag) ;
						}
					}
				
					@Override
					public void onClick(AjaxRequestTarget target) { };
				} ;
				
				// fill the Label with text (node state/name)
				ret.setBody(new Model<String>(node.getObject().toString())) ;

				final StringBuffer tooltip = new StringBuffer();
				
				if ( node.getObject().isSelected() ) {
					ret.add(new AttributeAppender("class", Model.of(" selected "))) ;
				}
							
				if ( node.getObject().isRCApp() && !node.getObject().getRCState().equals("") ) {
					tooltip.append("RC STATE:\t") ;
					if ( !node.getObject().getAppMembership() ) {
						tooltip.append("OUT [").append(node.getObject().getRCState()).append("]") ;
					} else {
						tooltip.append(node.getObject().getRCState()) ;
					}
					tooltip.append("\nAPP STATUS:\t").append(node.getObject().getAppState()) ;
					tooltip.append("\nHOST:\t\t").append(node.getObject().getHost()) ;

					ret.add(new AttributeModifier("rc-controller", node.getObject().getRCState())) ;
					ret.add(new AttributeAppender("class", Model.of(" rc-controller "))) ;
					ret.add(new AttributeModifier("rc-application", node.getObject().getAppState())) ;
					//ret.add(new AttributeAppender("class", Model.of(" " +node.getObject().getRCState() + " "))) ;
					//ret.add(new AttributeAppender("class", Model.of(" " +node.getObject().getAppState() + " "))) ;
					if ( node.getObject().getRCError() )
						{ ret.add(new AttributeAppender("class", Model.of(" FAULT "))) ; }
					if ( node.getObject().getRCBusy() )
						{ ret.add(new AttributeAppender("class", Model.of(" BUSY "))) ; }
					if ( !node.getObject().getAppMembership() )
						{ ret.add(new AttributeAppender("class", Model.of(" OUT "))) ; } ;
				} else if ( !node.getObject().getAppState().equals("") ) {
					tooltip.append("APP STATUS:\t") ;
					if ( !node.getObject().getAppMembership() ) {
						tooltip.append("OUT [").append(node.getObject().getAppState()).append("]") ; ;
					} else {
						tooltip.append(node.getObject().getAppState()) ;
					}
					
					tooltip.append("\nHOST:\t\t").append(node.getObject().getHost()) ;
					ret.add(new AttributeAppender("class", Model.of(" rc-application "))) ;
					ret.add(new AttributeModifier("rc-application", node.getObject().getAppState())) ;
					
					//ret.add(new AttributeAppender("class", Model.of(" " + node.getObject().getAppState() + " "))) ;
					
					if ( node.getObject().getAppState().equals("FAILED") ) {
						tooltip.append("\nEXIT_CODE:\t").append(node.getObject().getExitCode()) ;
						tooltip.append("\nFAILURE:\t").append(node.getObject().getFailureReason()) ;
					}
					//if ( node.getObject().getAppState().startsWith("DIED") ) {
					//	ret.add(new AttributeModifier("DIED", Model.of("TRUE"))) ;	
					//}
					
					if ( !node.getObject().getAppMembership() )
						{ ret.add(new AttributeAppender("class", Model.of(" OUT "))) ; } ;
				} else {
					// System.err.println("Neither RC nor App state available for " + node.getObject().getName());
				};

				if ( !node.getObject().isEnabled() ) { // disabled component
					ret.add(new AttributeModifier("enabled", Model.of("FALSE"))) ;
				} else {
					ret.add(new AttributeModifier("enabled", Model.of("TRUE"))) ;
				}

				if ( !node.getObject().isCommitted() ) { // disabled component
					ret.add(new AttributeModifier("committed", Model.of("FALSE"))) ;
				}

				if ( node.getObject().isSegment() ) { 
					ret.add(new AttributeAppender("class", Model.of(" segment "))) ;
				}
				
				if ( !node.getObject().getRCFailReason().equals("") ) {
					tooltip.append("\nERROR:\n" +  node.getObject().getRCFailReason() );
				}
						
				if ( tooltip.length() > 0 ) {
					ret.add(AttributeModifier.replace("title", tooltip.toString() ));
				}

				return ret ;
			} ;
		};

		tree.setOutputMarkupId(true);
		
		// check if there were fresh IS updates and re-render if so
		int updateinterval = parameters.get("refresh").toInt(default_update_interval*1000) ;

		tree.add(new AbstractAjaxTimerBehavior(Duration.milliseconds(updateinterval)) {
			@Override
			protected void onTimer(AjaxRequestTarget target) {
				//rctreeprov.updateTree(tree, target) ;
				if ( rctreeprov.checkUpdated() ) { // refresh only if there were updates in tree elements
					target.add(tree) ; 
				}
			}
		}) ; 

		add(tree);

		AbstractAjaxTimerBehavior updateBeh = new AbstractAjaxTimerBehavior(Duration.milliseconds(15000)) {
            private static final long serialVersionUID = 5680069957499209316L;

            @Override
            protected void onTimer(AjaxRequestTarget target) {
                java.util.List<String> list = Partitions.getIPCPartitionNames(true) ; //refresh hash
                int newhash = Partitions.hash() ;

                if ( partitionsHash != newhash ) {
                    if ( list.contains(gone_partition) ) {
                        rc.RCStateInfo root = ISReciever.getRCInfo( gone_partition, gone_partition.equals("initial") ? HomePage.default_rc_app_name : HomePage.rc_app_name) ;
						if ( !(root.state.equals("NONE") || root.state.equals("INITIAL")) )  {
                            ers.Logger.log("Partition not yet ready " + gone_partition + ": " + root.state) ;
                            return ;
                        }
                        ers.Logger.log("Partition re-appeared: " + gone_partition + " in state " + root.state) ;
                    	try {
							ConfigProvider.loadDB(gone_partition) ;
							rctreeprov.partitionChanged(gone_partition) ;	
							gone_partition = "" ;
						} catch ( final Exception ex ) {
							ex.printStackTrace() ;
						}					
                    }
                    partitionsHash = newhash ;
                    target.add(tree) ;
                } 
            }
        } ;	

		add(updateBeh);

	}

	@Override
	public synchronized void partitionGone(final String partition) {
		System.err.println("Partition gone: " + partition);
		gone_partition = partition ;
	}

}
