package tdaq.webrc ;

import java.util.Map;
import java.util.Iterator;
import java.util.stream.Stream; 
import java.util.concurrent.ConcurrentHashMap ;
import java.util.concurrent.ExecutorService ;

import org.jfree.data.time.Second;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesDataItem;
import org.jfree.data.time.TimeSeriesCollection;

import is.AnyInfo ;

//  shared by all clients, keeps rates/busy time series per partition
public class TrigRatesProvider {


    final static String hltrate_source = Configuration.get("hltrate.source") ; //"DF.TopMIG-IS:HLT.info" 
    final static int hltrate_index = Configuration.getInt("hltrate.index") ; // 6 - DCM.OutRate, double
    final static String recrate_source = Configuration.get("recrate.source") ; // "DF.TopMIG-IS:HLT.Counters.Global.WritingEventRate" ;
    // TBED "DF_IS:HLT.SFO-1.Counters.Global" ;
    final static int recrate_index = Configuration.getInt("recrate.index") ; // 12 SFOngCounters.WritingEventRate, float
    final static String lvl1rate_source = Configuration.get("l1rate.source") ; // DF.HLTSV.Events
    final static int lvl1rate_index = Configuration.getInt("l1rate.index") ; // 7
    final static String busy_source = Configuration.get("l1rate.source") ; // DF.HLTSV.Events
    final static int busy_index = Configuration.getInt("l1rate.index") ; // 7

    static Map<String, TimeSeriesCollection> rates_timeseries_seconds = new ConcurrentHashMap<String, TimeSeriesCollection>() ;
    static Map<String, TimeSeriesCollection> busy_timeseries_seconds = new ConcurrentHashMap<String, TimeSeriesCollection>() ;
    static Map<String, TimeSeriesCollection> rates_timeseries_minutes = new ConcurrentHashMap<String, TimeSeriesCollection>() ;
    static Map<String, TimeSeriesCollection> busy_timeseries_minutes = new ConcurrentHashMap<String, TimeSeriesCollection>() ;

    private static ExecutorService executor = java.util.concurrent.Executors.newSingleThreadExecutor(); 

static public TimeSeriesCollection getRatesSeriesSeconds(final String partition) {
    TimeSeriesCollection tserierscoll = new TimeSeriesCollection() ;
    TimeSeriesCollection existing = rates_timeseries_seconds.putIfAbsent(partition, tserierscoll) ;
    if ( existing == null ) { // new partition
        System.err.println("Creating new partition for TrigRates: " + partition);
        final TimeSeries l1rate_series = new TimeSeries("L1 Rate");
        final TimeSeries l1rate_last = new TimeSeries("L1 Rate Last");
		final TimeSeries hltrate_series = new TimeSeries("HLT Rate");
		final TimeSeries hltrate_last = new TimeSeries("HLT Rate Last");
		final TimeSeries recrate_series = new TimeSeries("Recording Rate");
		final TimeSeries recrate_last = new TimeSeries("Recording Rate Last");
        l1rate_series.setMaximumItemAge(10*60) ; // seconds
        l1rate_last.setMaximumItemCount(1) ;
        hltrate_last.setMaximumItemCount(1) ;
        recrate_last.setMaximumItemCount(1) ;
        hltrate_series.setMaximumItemAge(10*60) ;
        recrate_series.setMaximumItemAge(10*60) ;
        tserierscoll.addSeries(l1rate_series) ;
        tserierscoll.addSeries(hltrate_series) ;
        tserierscoll.addSeries(recrate_series) ;
        tserierscoll.addSeries(l1rate_last) ;
        tserierscoll.addSeries(hltrate_last) ;
        tserierscoll.addSeries(recrate_last) ;
        return tserierscoll ;
    } else {
        return existing ;
    }
}

static public TimeSeriesCollection getRatesSeriesMinutes(final String partition) {
    TimeSeriesCollection tserierscoll = new TimeSeriesCollection() ;
    TimeSeriesCollection existing = rates_timeseries_minutes.putIfAbsent(partition, tserierscoll) ;
    if ( existing == null ) { // new partition
        final TimeSeries l1rate_series = new TimeSeries("L1 Rate");
		final TimeSeries hltrate_series = new TimeSeries("HLT Rate");
		final TimeSeries recrate_series = new TimeSeries("Recording Rate");
		l1rate_series.setMaximumItemAge(12*60) ; // minutes
        hltrate_series.setMaximumItemAge(12*60) ;
        recrate_series.setMaximumItemAge(12*60) ;
        tserierscoll.addSeries(l1rate_series) ;
        tserierscoll.addSeries(hltrate_series) ;
        tserierscoll.addSeries(recrate_series) ;
        return tserierscoll ;
    } else {
        return existing ;
    }
}

static public TimeSeriesCollection getBusySeriesSeconds(final String partition) {
    TimeSeriesCollection tserierscoll = new TimeSeriesCollection() ;
    TimeSeriesCollection existing = busy_timeseries_seconds.putIfAbsent(partition, tserierscoll) ;
    if ( existing == null ) { // new partition
    	final TimeSeries busy_series = new TimeSeries("Busy");
	    busy_series.setMaximumItemAge(10*60) ; // seconds
        tserierscoll.addSeries(busy_series) ;
        return tserierscoll ;
    } else {
        return existing ;
    }
}

static public TimeSeriesCollection getBusySeriesMinutes(final String partition) {
    TimeSeriesCollection tserierscoll = new TimeSeriesCollection() ;
    TimeSeriesCollection existing = busy_timeseries_minutes.putIfAbsent(partition, tserierscoll) ;
    if ( existing == null ) { // new partition
    	final TimeSeries busy_series = new TimeSeries("Busy");
	    busy_series.setMaximumItemAge(12*60) ; // minutes
        tserierscoll.addSeries(busy_series) ;
        //System.err.println("Returning new Busy series size: " + tserierscoll.getSeries(0).getItemCount() );
        return tserierscoll ;
    } else {
        //System.err.println("Returning existing Busy series size: " + existing.getSeries(0).getItemCount() );
        return existing ;
    }
}

static public synchronized void removePartition(final String partition) {

    ers.Logger.info("Removing partition " + partition);
    
    rates_timeseries_minutes.remove(partition) ;
    rates_timeseries_seconds.remove(partition) ;
    busy_timeseries_minutes.remove(partition) ;
    busy_timeseries_seconds.remove(partition) ;
}

static {
    
executor.execute( () -> {
    int i = 0 ;
    while (true) {
        try { Thread.sleep(1000) ; } catch (final InterruptedException ex) {} ;
        i++ ;
        Second now = new Second(new java.util.Date()) ;
        is.AnyInfo info = new is.AnyInfo() ;

		synchronized( TrigRatesProvider.class ) {
		Iterator<String> partitions = rates_timeseries_seconds.keySet().iterator() ;
        while  ( partitions.hasNext() ) {
			final String partition = partitions.next() ;
            is.Repository repo = new is.Repository(new ipc.Partition(partition)) ;
            double l1rate = 0, hltrate = 0, recrate = 0, busy = 0 ;
            try {
                repo.getValue(lvl1rate_source, info) ;
                l1rate = Double.parseDouble(info.getAttribute(lvl1rate_index).toString()) ;
                info = new is.AnyInfo() ;
                repo.getValue(hltrate_source, info) ;
                hltrate = Double.parseDouble(info.getAttribute(hltrate_index).toString()) ;
                info = new is.AnyInfo() ;
                repo.getValue(recrate_source, info) ;
                recrate = ((Float)info.getAttribute(recrate_index)).floatValue() ;
                // get total CTP busy from L1CT.CTP.Instantaneous.BusyFractions 
                // ctpcore_objects {type: 'CtpBusyInfo'}
                //  e.g. attributes('ctpcore_objects').wrapObjectArray[9].attributes('fraction').float as totalFraction
                try {
                    repo.getValue(Configuration.get("busy.source"), info) ;
                    AnyInfo[] ctpcore_objects = (AnyInfo[])info.getAttribute(Configuration.getInt("busy.index")) ;
                    busy = 100.0*Double.parseDouble(ctpcore_objects[Configuration.getInt("busy.ctpcore.total.index")].getAttribute(Configuration.getInt("busy.ctpcore.total.fraction.index")).toString()) ;    
                } catch ( final Exception ex ) {
                    if ( partition.equals("ATLAS") ) {
                        ers.Logger.debug(1, "Failed to get Busy from ATLAS L1CT: " + ex.getMessage() ) ;
                    }                   
                }

            } catch ( final is.RepositoryNotFoundException ex ) {
                ers.Logger.log("IS server has gone in partition " + partition);
				partitions.remove() ;
				rates_timeseries_minutes.remove(partition) ;
				busy_timeseries_minutes.remove(partition) ;
				busy_timeseries_seconds.remove(partition) ;
                // removePartition(partition) ;
                continue ;
            } catch ( final is.InfoNotFoundException ex ) {
                if ( partition.equals("ATLAS") ) {
                    ers.Logger.debug(1, "Failed to get rates from ATLAS DF IS: " + ex.getMessage() );
                }
                // removePartition(partition) ;
                continue ;
            }
            catch ( final Exception ex ) {
                ex.printStackTrace() ;
            }

            rates_timeseries_seconds.get(partition).getSeries(0).addOrUpdate(now, l1rate);
            rates_timeseries_seconds.get(partition).getSeries(1).addOrUpdate(now, hltrate);
            rates_timeseries_seconds.get(partition).getSeries(2).addOrUpdate(now, recrate);
            rates_timeseries_seconds.get(partition).getSeries(3).addOrUpdate(now, l1rate);
            rates_timeseries_seconds.get(partition).getSeries(4).addOrUpdate(now, hltrate);
            rates_timeseries_seconds.get(partition).getSeries(5).addOrUpdate(now, recrate);
            busy_timeseries_seconds.get(partition).getSeries(0).addOrUpdate(now, busy);

            if ( i % 60 == 0 ) { // average every minute
                Minute now_min = new Minute(new java.util.Date()) ;
                for ( final String part: rates_timeseries_seconds.keySet() ) {
                    Stream<TimeSeriesDataItem> str = rates_timeseries_seconds.get(part).getSeries(0).getItems().stream() ;
                    double avl1 = str.mapToInt( (x)->x.getValue().intValue() ).average().orElse(0.0) ;
                    
                    str = rates_timeseries_seconds.get(part).getSeries(1).getItems().stream() ;
                    double avhlt = str.mapToInt( (x)->x.getValue().intValue() ).average().orElse(0.0) ;
    
                    str = rates_timeseries_seconds.get(part).getSeries(2).getItems().stream() ;
                    double avrec = str.mapToInt( (x)->x.getValue().intValue() ).average().orElse(0.0) ;
    
                    str = busy_timeseries_seconds.get(part).getSeries(0).getItems().stream() ;
                    double avbusy = str.mapToInt( (x)->x.getValue().intValue() ).average().orElse(0.0) ;
    
                    rates_timeseries_minutes.get(part).getSeries(0).addOrUpdate(now_min, avl1) ;
                    rates_timeseries_minutes.get(part).getSeries(1).addOrUpdate(now_min, avhlt) ;
                    rates_timeseries_minutes.get(part).getSeries(2).addOrUpdate(now_min, avrec) ;
                    busy_timeseries_minutes.get(part).getSeries(0).addOrUpdate(now_min, avbusy) ;
                }
            }
    
        }
		}
	}

} );
/*
executor.execute( () -> {
    int i = 0 ;
    while (true) {
        try { Thread.sleep(1000) ; } catch (final InterruptedException ex) {} ;
        i++ ;
        int l1rate = java.util.concurrent.ThreadLocalRandom.current().nextInt(70000, 90000); 
        int hltrate =  java.util.concurrent.ThreadLocalRandom.current().nextInt(65000, 85000);
        // int hltrate = l1rate ;
        int recrate = java.util.concurrent.ThreadLocalRandom.current().nextInt(1500, 3000);
        int busy = java.util.concurrent.ThreadLocalRandom.current().nextInt(2, 5);
        Second now = new Second(new java.util.Date()) ;
        for  ( TimeSeriesCollection tscoll: rates_timeseries_seconds.values() ) {
            tscoll.getSeries(0).add(now, l1rate);
            tscoll.getSeries(1).add(now, hltrate);
            tscoll.getSeries(2).add(now, recrate);
            tscoll.getSeries(3).add(now, l1rate);
            tscoll.getSeries(4).add(now, hltrate);
            tscoll.getSeries(5).add(now, recrate);
        }
        for  ( TimeSeriesCollection tscoll: busy_timeseries_seconds.values() ) {
            tscoll.getSeries(0).add(now, busy);
            // System.err.println("Busy series size: " + tscoll.getSeries(0).getItemCount() );
        }
        if ( i % 60 == 0 ) { // every minute
            Minute now_min = new Minute(new java.util.Date()) ;
            for ( final String part: rates_timeseries_seconds.keySet() ) {
                Stream<TimeSeriesDataItem> str = rates_timeseries_seconds.get(part).getSeries(0).getItems().stream() ;
                double avl1 = str.mapToInt( (x)->x.getValue().intValue() ).average().getAsDouble() ;
                
                str = rates_timeseries_seconds.get(part).getSeries(1).getItems().stream() ;
                double avhlt = str.mapToInt( (x)->x.getValue().intValue() ).average().getAsDouble() ;

                str = rates_timeseries_seconds.get(part).getSeries(2).getItems().stream() ;
                double avrec = str.mapToInt( (x)->x.getValue().intValue() ).average().getAsDouble() ;

                str = busy_timeseries_seconds.get(part).getSeries(0).getItems().stream() ;
                double avbusy = str.mapToInt( (x)->x.getValue().intValue() ).average().getAsDouble() ;

                rates_timeseries_minutes.get(part).getSeries(0).add(now_min, avl1) ;
                rates_timeseries_minutes.get(part).getSeries(1).add(now_min, avhlt) ;
                rates_timeseries_minutes.get(part).getSeries(2).add(now_min, avrec) ;
                busy_timeseries_minutes.get(part).getSeries(0).add(now_min, avbusy) ;
            }
        }
    }
} ) ;
*/
}

}
