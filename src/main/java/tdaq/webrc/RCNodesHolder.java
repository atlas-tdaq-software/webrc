package tdaq.webrc ;

import java.util.Map ;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap ;

/**
 * A holder for RC trees for all partitions. 
 * It is meant to be static, shared by all HomePages, partitions and sessions, and not be
 * serializable, unlike the model (see {@link tdaq.webrc.RCTreeNodeProvider} - a provider of the Tree of RCNodes for particular partition)
 * 
 * @see RCTreeNodeProvider
 */
public class RCNodesHolder {
    /** static collection of all nodes for all partitions */
    private static Map<String, Map<String, RCNode>> allNodes = new ConcurrentHashMap<String, Map<String, RCNode>>() ;

    /** static collection of all RCTreeNodeProviders */
    private static Map<String, List<RCTreeNodeProvider>> allProviders = new ConcurrentHashMap<String, List<RCTreeNodeProvider>>() ;
    
    /** creates new holder for this partition and returns null,
        unless it is already exist, in this case adds provider to the list returns partitions root node
        
        @see RCNode
    */ 
    public static synchronized RCNode newPartition(final String partition, final RCTreeNodeProvider provider) {
        ArrayList<RCTreeNodeProvider> list = new ArrayList<RCTreeNodeProvider>() ;
        list.add(provider) ;
        List<RCTreeNodeProvider> existing_list = allProviders.putIfAbsent(partition, list ) ;
        if ( existing_list != null ) { 
			if ( !existing_list.contains(provider) ) {
				existing_list.add (provider) ;
				ers.Logger.debug(0, "RCNodes provider for Partition " + partition + " added to RCNodesHolder, total providers: " + existing_list.size()) ;
			}
		}	
		else {
			ers.Logger.debug(0, "New RCNodes Holder for Partition " + partition + " created") ;
		};
        
        Map<String, RCNode> existing_partition = allNodes.putIfAbsent(partition, new ConcurrentHashMap<String, RCNode>() ) ;
        
        if ( existing_partition != null ) {
            ers.Logger.info("Partition " + partition + " already loaded in RCNodesHolder") ;
            return existing_partition.get(partition.equals("initial") ? "DefaultRootController" : "RootController") ;
        } else {
            try { 
                //subscribeRootExit(partition) ;
				PartitionExitMonitor.subscribe(partition, new PartitionExitMonitor.ISubscriber() {
					@Override
					public void partitionExited(final String partition) {
						clearPartition(partition) ;
					}
				});
            } catch ( final Exception ex ) {
                ex.printStackTrace() ;
            }
            ers.Logger.info("Partition " + partition + " added to Map in RCNodesHolder and subscribed in PartitionExitMonitor") ;
            return null ; 
        }
    }

    /** called from Configuration callback when an existing Partition Configuration reloaded
     *  notifies all Providers, clears nodes tree and creates new one from ConfigProvider
     * 
     *  @see ConfigProvider
     * */ 
    public static synchronized void reloadPartition(final String partition) {

        // notify providers that nothing is yet ready to display
        
        allProviders.get(partition).forEach( p -> p.partitionReloading() );
        allNodes.get(partition).get(partition.equals("initial") ? "DefaultRootController" : "RootController").clearChildren() ;
        allNodes.get(partition).clear() ;
        ers.Logger.log("all RCNodes cleared for partition " + partition);
        new RCNode(ConfigProvider.getPartition(partition)) ;
        ers.Logger.log("Partition configuration updated, new RC tree loaded in partition " + partition);
        allProviders.get(partition).forEach(p -> p.partitionReloaded() );
    }
	
    /**
     * called from PMG client singleton callback when a RootController of a Partition exits
    */ 
    public static void clearPartition(final String partition) {
        synchronized(ConfigProvider.class) {
            ers.Logger.log("Partition " + partition + " is gone") ;
            if ( allNodes.get(partition) == null ) { return ; }
            if ( allNodes.get(partition).get("RootController") == null ) { return ; }
            // allNodes.get(partition).get("RootController").clearChildren() ;
            
            // notify all RCTree clients of the partition
            ers.Logger.log("Notifying " + allProviders.get(partition).size() + " providers that partition has gone: " + partition) ;
            allProviders.get(partition).forEach(p -> p.partitionGone());

            allNodes.get(partition).clear() ; // remove references to Nodes, eventually to be garbage-collected
            allNodes.remove(partition) ;
            allProviders.get(partition).clear() ;
            allProviders.remove(partition) ;
        } // synch
        ers.Logger.log("all RCNodes and RCProviders cleared for partition " + partition) ;

        ConfigProvider.clearConfig(partition) ;
        ers.Logger.log("Configuration cleared for partition " + partition) ;
    }
    
    public static void addNode(final String partition, final String name, final RCNode node) {
        if ( allNodes.get(partition) == null ) return ;
        if ( null == allNodes.get(partition).putIfAbsent(name, node) ) {
            //System.err.println("Node " + name + " was added in partition " + partition) ;
        }
        else {
            //System.err.println("Node " + name + " already existed in partition " + partition + "!") ;
        }
    }

    public static RCNode findNode(final String partition, final String id) {
        if ( allNodes.get(partition) != null ) {
            return allNodes.get(partition).get(id) ;  
        }
        else { return null ; }
    }

	/** called when HomePage and RCTreeNodeProvider gets destroyed */ 
	public static synchronized void removeProvider(final String partition, final RCTreeNodeProvider provider) {
        List<RCTreeNodeProvider> providers = allProviders.get(partition) ;
        if ( providers != null ) { 
			if ( providers.remove(provider) ) {
				ers.Logger.info("RCNodes provider for partition " + partition + " removed from RCNodesHolder, total providers left: " + providers.size()) ;
			} else {
				ers.Logger.info("Failed to remove provider for partition " + partition + ": not found in the list") ;
			}
		}
		else {
			ers.Logger.info("Failed to remove provider for partition " + partition + ": list not found") ;
		} ;
	}

}