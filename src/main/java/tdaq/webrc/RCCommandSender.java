package tdaq.webrc ;

import java.lang.reflect.Field ;
import java.util.Map ;
import java.io.BufferedReader ;
import java.io.IOException ;

import com.sun.jna.Library ;
import com.sun.jna.Native ;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession ;

import daq.rc.CommandSender ;
import daq.rc.RCException ; 
import dal.MasterTrigger ;
import dal.Partition ;
import TRIGGER.TriggerCommander ;

import daq.tokens.JWToken ;
import daq.tokens.AcquireTokenException ;

/**
 * Uses daq.rc.CommandSender interface to send commands either to individual controllers (e.g. IGNORE_ERROR) or FSM transition commands to the RootController of a partition.
 * Before sending a command, sets the tdaq token BEARER_TOKEN environment to the current user session atlas_tdaq_token.
 * 
 * @see daq.rc.CommandSender
 */
public class RCCommandSender {

static final java.util.concurrent.locks.Lock envlock = new java.util.concurrent.locks.ReentrantLock() ;
static String token = new String() ;

// syncronous, throws exceptions
public static synchronized void
sendCommand(final String partition, final String controllerName, final daq.rc.Command command) 
throws Exception {

    CommandSender sender = new daq.rc.CommandSender(partition, "WEB RC");
    ers.Logger.info("Sending the command " + command.toString() + " to " + controllerName);
    // ers.Logger.info("Got daq token: " + JWToken.acquire(JWToken.MODE.REUSE)) ;
    
    setToken() ;
    try {
        sender.executeCommand(controllerName, command) ;
    }
    finally {
        clearToken() ;
    }
}

public static synchronized void
sendRootTransitionCommand(final String partition, final daq.rc.Command.FSMCommands command)
throws Exception {
    CommandSender sender = new daq.rc.CommandSender(partition, "WEB RC");
    ers.Logger.info("Sending the transition command " + command.toString() + " to RootController") ;
    setToken() ;
    try {
        sender.makeTransition("RootController", command) ;
    }
    finally {
        clearToken() ;
    }
}

public static boolean HoldTrigger(final String partition) {
 
    TriggerCommander trgCommander = getMasterTrigger(partition) ;
 
    if ( trgCommander != null ) {
        try {
            trgCommander.hold() ;
            return true ;
        } catch ( final TRIGGER.CommandExecutionFailed ex ) {
            ers.Logger.error(ex) ;
        }
    }
    return false ;
}

public static synchronized boolean ResumeTrigger(final String partition) {

    TriggerCommander trgCommander = getMasterTrigger(partition) ;

    if ( trgCommander != null ) {
        try {
            trgCommander.resume() ;
            return true ;
        } catch ( final TRIGGER.CommandExecutionFailed ex ) {
            ers.Logger.error(ex) ;
        }
   }
    return false ;
}

private static TriggerCommander getMasterTrigger(final String partition) {
    String mtControllerName = null;

    try {
        final MasterTrigger mt = ConfigProvider.getPartition(partition).get_MasterTrigger( );
        if(mt != null) {
            final dal.RunControlApplicationBase rc = mt.get_Controller();
            if(rc != null) {
                mtControllerName = rc.UID();
                ers.Logger.info("The partition Master Trigger is " + mtControllerName);
            } else {
                ers.Logger.warning(new RuntimeException("The Master Trigger does not define a controller; it will not be possible to hold or resume the trigger"));
            }
        } else {
            ers.Logger.warning(new RuntimeException("The Master Trigger is not defined in the partition; it will not be possible to hold or resume the trigger"));
        }
    }
    catch( final Exception ex ) {
        ers.Logger.error(new RuntimeException("Error looking for the partition Master Trigger: " + ex, ex));
    }

    if ( mtControllerName != null ) {
        return new TriggerCommander(partition, mtControllerName);
    } else {
        return null;
    }
}

public static synchronized void setToken () {
    token = ((SignInSession)AuthenticatedWebSession.get()).getTdaqToken() ;

    if ( token != null ) {
        envlock.lock() ;
        ers.Logger.debug(1, "Acquired daq token from SSO") ;
        setTokenEnv(token) ;
    }
    else {
        try {
            final String tok = daq.tokens.JWToken.acquire(daq.tokens.JWToken.MODE.REUSE) ; // why?
            ers.Logger.info("Acquired daq token: " + tok) ;
        } catch (final AcquireTokenException ex) {
            ers.Logger.error(ex);
        }
        ers.Logger.info("Back to default tocken behavior") ;
    }
}
    
public static synchronized void clearToken () {   
    if ( token != null ) {
        ers.Logger.debug(1, "Clearing BEARER_TOKEN") ;
        setTokenEnv("") ;
        token = null ;
        envlock.unlock();
    }
}

// JNA magic
public interface LibC extends Library {
    static LibC INSTANCE = (LibC) Native.load("c", LibC.class);
    public int setenv(String name, String value, int overwrite);
    public int unsetenv(String name);
}

/** Sets BEARER_TOKEN for JNI environment using native libc setenv call */
private static void setTokenEnv(final String token) {

    if ( token == null ) { // we do not have token from a SSO session, so fallback to default behavior, be it local, gssapi etc
        ers.Logger.log("No token available, fallback to default behavior") ;
        return ;
    }
/* This does not work for the environment accessed fom JNI calls like done in daq_token library, so we need JNI/JNA way to call the native "libc" setenv
try {
    Map<String, String> env = System.getenv();
    Class<?> cl = env.getClass();
    Field field = cl.getDeclaredField("m");
    field.setAccessible(true);
    Map<String, String> writableEnv = (Map<String, String>) field.get(env);
    writableEnv.put("BEARER_TOKEN", token);
    ers.Logger.info("set BEARER_TOKEN env:\n" + token) ;
} catch (Exception e) {
    throw new IllegalStateException("Failed to set environment variable", e);
}
 */
    if ( token.isEmpty() ) {
        int ret1 = LibC.INSTANCE.unsetenv("TDAQ_TOKEN_ACQUIRE") ;
        int ret2= LibC.INSTANCE.unsetenv("BEARER_TOKEN") ;
        if ( ret1 != 0 || ret2 !=0 ) {
            ers.Logger.error(new Exception("Failed to unset TDAQ_TOKEN_ACQUIRE with JNA: " + ret1)) ;
        } else {
            ers.Logger.log("OK unsetenv TDAQ_TOKEN_ACQUIRE") ;
        }
        return ;
    }
    
    int ret = LibC.INSTANCE.setenv("TDAQ_TOKEN_ACQUIRE", "env", 1) ;
    if ( ret != 0 ) {
        ers.Logger.error(new Exception("Failed to setenv TDAQ_TOKEN_ACQUIRE with JNA:" + ret)) ;
    } else {
        ers.Logger.debug(2, "OK setenv TDAQ_TOKEN_ACQUIRE=env with JNA") ;
    }

    ret = LibC.INSTANCE.setenv("BEARER_TOKEN", token, 1) ;
    if ( ret != 0 ) {
        ers.Logger.error(new Exception("Failed to setenv BEARER_TOKEN with JNA:" + ret)) ;
    } else {
        ers.Logger.debug(0, "OK setenv BEARER_TOKEN with JNA") ;
    }

}

}
