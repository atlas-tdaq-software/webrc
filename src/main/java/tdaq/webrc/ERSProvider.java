package tdaq.webrc;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap ;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.ExecutorService;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;

import ers.BadStreamConfiguration;

/**  needed in DataTable, keeps a Queue (LinkedList) of ERSIssues, provides
necessary methods to access data 
Factory: New instance is created each time subscription/partition is changed
It holds a map of <prtition+subscription, WebIssueReceiver> receivers which holds the real data
*/ 
class ERSProvider extends SortableDataProvider<ERSIssue, String> {

private static final long serialVersionUID = 145635673324L;

private static int pausedSize = 0 ; //
private WebIssueReceiver receiver; // actual shared received, gets and holds issues from ERS
private AtomicBoolean updated = new AtomicBoolean(true) ;
private AtomicBoolean paused = new AtomicBoolean(false) ;
private AtomicBoolean showChained = new AtomicBoolean(false) ;
private AtomicBoolean filterByTree = new AtomicBoolean(false) ;
private AtomicBoolean acrFilter = new AtomicBoolean(false) ;
private String selected_node = null ;
// private Pattern ersFilterRegexp = Pattern.compile(".*") ;

private ERSIssueFilter filter ;

private static Map<String, WebIssueReceiver> ersReceivers = new ConcurrentHashMap<String, WebIssueReceiver>() ;

private static ExecutorService executor = java.util.concurrent.Executors.newFixedThreadPool(Configuration.getInt("is.executor.threads")) ; // newCachedThreadPool()

public static class BadAcrFilterConfig extends Exception {
        BadAcrFilterConfig(final Exception ex) {                              
            super ("Failed to load ACR filter from OKS " + Configuration.get("erstable.acrfilter.location"), ex) ;
        } 
}

/**
 * Provider for ERS table, one instance per HomePage. Subscribes in ERS and applies additional filtering over received messages.
 * @param filt
 */
ERSProvider(final ERSIssueFilter filt) {
    // deque = new CopyOnWriteArrayList<ERSIssue>();
    // deque = java.util.Collections.synchronizedList(new ArrayList<ERSIssue>()) ; // better for many-writes use cases
    //deque = java.util.concurrent.ArrayBlockingQueue(maxSize) ;
    receiver = new WebIssueReceiver("") ; // empty until subscribed
    filter = filt ;
}

public void setSelectedNode(final String name) {
    selected_node = name ;
}

/**
 *  creates unique shared receiver for a combination partition+subscription 
 *  removes receivers only when application exits AND when partition exits
 * 
 */
public synchronized void subscribe(final String partition, final String subscription) throws BadStreamConfiguration, BadAcrFilterConfig {
        String acrfilter = "" ;
        
        if ( acrFilter.get() ) {                                             
            ErsFilterLoader floader = new ErsFilterLoader(Configuration.get("erstable.acrfilter.location")) ;
            ers.Logger.info("Loading ERS filter from OKS file " + Configuration.get("erstable.acrfilter.location")) ;
            try {
                acrfilter = floader.getSubscriptionString(partition) ;
            } catch ( final Exception ex ) {
                throw new BadAcrFilterConfig(ex) ;
            }
        }
        final String key = partition + "$" + subscription + acrfilter ;
        
        if ( ersReceivers.containsKey(key) ) {
            receiver = ersReceivers.get(key) ;
            ers.Logger.log("Using existing shared MTS ERS receiver for " + partition + "/" + subscription);
        } else {
            receiver = new WebIssueReceiver(partition) ;
            String subs = subscription ;
            if ( acrFilter.get() ) {
                subs = "("+ subscription + ") and (" + acrfilter  + ")";
                ers.Logger.debug(0, "Subscription with ACR filter: " + subs) ;
            }
            ers.StreamManager.instance().add_receiver(receiver, "mts", partition, subs) ;    // throw       
            ersReceivers.put(key, receiver) ;
            ers.Logger.log("Created new shared MTS ERS receiver for " + partition + "/" + subs ) ;
        }
}

// when partition infrastructue is gone, need remove all entries for it and to re-subscribe later
public synchronized void partitionGone(final String partition) {
    selected_node = "" ;
    for ( final String partsubs: ersReceivers.keySet() ) {
        if ( partsubs.startsWith(partition + "$") ) {
            try {
                if ( ersReceivers.remove(partsubs) != null ) {
                    ers.Logger.log("Removed shared MTS ERS receiver for " + partsubs) ;
                } else {
                    ers.Logger.log("UNEXPECTED Map did not contain shared MTS ERS receiver for " + partsubs) ;
                }
                /*
				if ( receiver != null ) {
                    ers.StreamManager.instance().remove_receiver(receiver) ;
                	ers.Logger.log("Unsubscribed from MTS in " + partition) ;
                } */ // no need to try to nsubscribe, all is gone! 
            } catch ( final Exception ex ) {
                ex.printStackTrace() ;
            }
        }
    }
    
}

/* Not possible with shared deque?
public synchronized void clear_data() {
    // deque.clear() ;
    pausedSize = 0 ;
    updated.set(true) ;
} */

public void setPause(boolean flag) {
    paused.set(flag);
    if ( paused.get() ) {
        pausedSize = receiver.deque.size() ;
    }
}

@Override
protected void finalize() throws Throwable {
//    if ( receiver!=null ) { ers.StreamManager.instance().remove_receiver(receiver) ; } ;
//    super.finalize();
    System.err.println("ERS Provider removed") ;
}

static void cleanUp() {
    for ( WebIssueReceiver rec: ersReceivers.values() ) {
        try {
            ers.StreamManager.instance().remove_receiver(rec) ;
        } catch ( final ers.ReceiverNotFound ex ) { }
		catch ( final Exception ex ) {
            ex.printStackTrace() ;
        } 
    }
    System.err.println("All ERS providers unsubscribed") ;
}

    // interface to ERS/MTS, stores received issues to ERSIssueProvider (after
    // converting them to local ERSIssue)
    static class WebIssueReceiver implements ers.IssueReceiver, java.io.Serializable {
 
        private static final long serialVersionUID = -8396955075756359748L;
        private List<ERSIssue> deque ; // syncronized wrapper over _deque 
        private String partition ; // syncronized wrapper over _deque 
        private AtomicBoolean full = new AtomicBoolean(false) ;
        private AtomicBoolean updated = new AtomicBoolean(true) ;
        private static final int maxSize = Configuration.getInt("erstable.maxmessages") ;
        private static final int threshold = 90 ; // % of the queue to keep

        WebIssueReceiver(final String part) {
            partition = part ;
            deque = java.util.Collections.synchronizedList(new ArrayList<ERSIssue>()) ;
        }

        public void receive(ers.Issue issue) {

            executor.execute( () -> {
            if ( issue.cause() != null ) {
                List<ERSIssue> ret = new ArrayList<ERSIssue>() ; 
                ers.Issue next = issue.cause() ;   
                while ( next != null ) {
                    ret.add(new ERSIssue(issue, next.getId(), next.message())) ;
                    next = next.cause() ;
                }
                add(ret) ;
            }

            add(new ERSIssue(issue));
            updated.set(true) ;
            });
        }
        // synchronized
        void add(final ERSIssue issue) {
            deque.add(issue) ;
            updated.set(true) ;
            if ( deque.size() > maxSize /* && !paused.get() */ ) {
                if ( full.compareAndSet(true, true) ) { return ; }

                for (int i=0; i < maxSize*(100-threshold)/100 ; i++ )  {
                    try {
                        deque.remove(i) ;
                    } catch ( final IndexOutOfBoundsException ex ) {
                        break ;
                    }
                }
                full.set(false) ;
            }
        }

        // synchronized 
        void add(final List<ERSIssue> issues) {
            deque.addAll(issues);
        }

        boolean isUpdated() {
            return updated.get() ;
        }
        
        List<ERSIssue> getMessages() {
            return deque ;
        } 
        
        String getPartition() {
            return partition ;
        }
    } ;
 

boolean checkUpdated() {
    updated.set(receiver.isUpdated()) ;
    return updated.getAndSet(false) ;
}

public void showChained(boolean flag) {
    showChained.set(flag) ;
}

public void filterByTree(boolean flag) {
    filterByTree.set(flag) ;
}

public void acrFilter(boolean flag) {
    acrFilter.set(flag) ;

}
// called by the tree when it need to display a page (e.g. 10 Issues)
// get a sublist reverse from the back of the deque
// filter when reteiving 
@Override
public Iterator<ERSIssue> iterator(long first, long count) {
    //System.err.println("Iterator requested: " + first + " to " + (first+count) + " out of " + deque.size());
    List<ERSIssue> ret = new ArrayList<ERSIssue>() ;
    for ( int i = 0, j = 0 ; j < count ; i++ ) {
            ERSIssue candidate ;
            try {
                int head = paused.get() ? pausedSize : receiver.getMessages().size() ;
                candidate = receiver.getMessages().get(head-1-(int)first-i)  ;

 //           if ( ersFilterRegexp.matcher(candidate.getId()).matches() ) {
            if ( candidate.getChained() && !showChained.get() ) { continue ; }
            if ( filter.match(candidate) ) {
                try {
                if ( filterByTree.get() && selected_node !=null && !selected_node.isEmpty() && !ConfigProvider.hasParent(receiver.getPartition(), candidate.getApplication(), selected_node) && !selected_node.equals(candidate.getApplication()) ) {
                    // System.err.println("Selected node "  + selected_node + " is not in the parents list of " + candidate.getApplication());
                    continue ;
                }
                } catch ( final Exception ex ) {
                    System.err.println("FATAL Selected node "  + selected_node + ", message from " + candidate.getApplication()) ;
                    ex.printStackTrace() ;
                }
            ret.add( candidate ) ;
                j++ ;
            } else {
                // System.err.println("Message ID " + candidate.getId() + " does NOT match " + ersFilterRegexp.pattern() );
            }
            } catch ( final ArrayIndexOutOfBoundsException ex ) {
                break ;
            } catch ( final IndexOutOfBoundsException ex ) {
                break ;
            }
            catch ( final java.util.regex.PatternSyntaxException ex ) {
                ex.printStackTrace();
            };
        }
    return ret.iterator() ;
    
}

@Override
public synchronized long size() {
    if ( !paused.get() ) {
        return receiver.getMessages().size() ;
    }
    return pausedSize ;
}

@Override
public IModel<ERSIssue> model(ERSIssue object) {
    return new Model<ERSIssue>(object);
}


}
