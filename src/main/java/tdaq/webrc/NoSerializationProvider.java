package tdaq.webrc ;

import java.io.Serializable ;

import org.apache.wicket.page.IManageablePage;
import org.apache.wicket.Application;
import org.apache.wicket.pageStore.IDataStore ;
import org.apache.wicket.pageStore.IPageStore ;

// avoid serialization of the WebPage (may be expensive as it deep-copies datastructures)
// taken from http://maciej-miklas.blogspot.com/2013/09/wicket-6-disable-page-serialization.html

public class NoSerializationProvider extends org.apache.wicket.DefaultPageManagerProvider {

    public NoSerializationProvider(Application application) {
        super(application); 
    } 

    @Override 
    protected IPageStore newPageStore(IDataStore dataStore) { 
        return new IPageStore() {
            @Override 
            public void destroy() {
            } 
            
            @Override
            public boolean canBeAsynchronous() {
                return true ;
            }

            @Override 
            public IManageablePage getPage(String sessionId, int pageId) {
                return null;
            } 

            @Override 
            public void removePage(String sessionId, int pageId) {
            } 

            @Override 
            public void storePage(String sessionId, IManageablePage page) {
            } 

            @Override 
            public void unbind(String sessionId) {
            } 

            @Override 
            public Serializable prepareForSerialization(String sessionId, Serializable page) {
                return null;
            } 

            @Override 
            public Object restoreAfterSerialization(Serializable serializable) {
                return null;
            } 

            @Override 
            public IManageablePage convertToPage(Object page) {
                return null;
            } 
        }; 

    } 
}