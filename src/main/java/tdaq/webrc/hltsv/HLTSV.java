package tdaq.webrc.hltsv;


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * 
 * @author  produced by the IS generator
 */

public class HLTSV extends is.Info {
    public static final is.Type type = new is.Type( new HLTSV( ) );


    /**
     * Number of LVL1 events
     */
    public long                LVL1Events;

    /**
     * Number of assigned events
     */
    public long                AssignedEvents;

    /**
     * Number of reassigned events.
     */
    public long                ReassignedEvents;

    /**
     * Number of processed events
     */
    public long                ProcessedEvents;

    /**
     * The number of currently available cores on the DCMs.
     */
    public int                 AvailableCores;

    /**
     * The fraction of available cores on average between samples
     */
    public float               FracAvailable;

    /**
     * The maximum number of available cores in the DCMs
     */
    public int                 MaxAvailable;

    /**
     * Instantaneous Rate (Hz)
     */
    public float               Rate;

    /**
     * Recent Level 1 ID
     */
    public int                 Recent_LVL1_ID;

    /**
     * Recent Global Event ID
     */
    public long                Recent_Global_ID;

    /**
     * Busy fraction received from HLT farm.
     */
    public float               Busy;

    /**
     * Number of DCM Sessions seen by HLTSV.
     */
    public int                 DCMSessions;

    /**
     * Number of failed LVL1 decodings
     */
    public long                LVL1DecodingErrors;

    /**
     * RoIB Mask of Active Channels
     */
    public int                 RoIB_Active_Mask;

    /**
     * Bandwidth per channel (MBytes/s)
     */
    public double[]            RoIB_Bandwidth;

    /**
     * Number of events received but not built yet due to missing ROLs
     */
    public long                RoIB_Pending_RNP;

    /**
     * Number of events built but waiting for the supervisor
     */
    public long                RoIB_Pending_DAQ;

    /**
     * Time to complete building an event in microseconds
     */
    public long                RoIB_time_Build;

    /**
     * Time to complete processing an event in microseconds
     */
    public long                RoIB_time_Process;

    /**
     * Time elapsed for timeouts in microseconds
     */
    public long                RoIB_timeout;

    /**
     * RobinNP free pages
     */
    public long[]              RNP_Free_pages;

    /**
     * RobinNP used pages
     */
    public long[]              RNP_Used_pages;

    /**
     * RobinNP most recent ID
     */
    public int[]               RNP_Most_Recent_ID;

    /**
     * RobinNP XOFF state
     */
    public int[]               RNP_XOFF_state;

    /**
     * RobinNP XOFF percentage (%)
     */
    public double[]            RNP_XOFF_per;

    /**
     * RobinNP XOFF count
     */
    public long[]              RNP_XOFF_count;

    /**
     * RobinNP current status of ROL-down
     */
    public int[]               RNP_Down_stat;

    /**
     * Current status of bufferFull
     */
    public int[]               RNP_bufferFull;

    /**
     * # of ROL Link-Down events
     */
    public short[]             RNP_numberOfLdowns;


    public HLTSV() {
	this( "HLTSV" );
    }

    protected HLTSV( String type ) {
	super( type );
	LVL1Events = 0;
	AssignedEvents = 0;
	ReassignedEvents = 0;
	ProcessedEvents = 0;
	AvailableCores = 0;
	FracAvailable = 0.0f;
	MaxAvailable = 0;
	Rate = 0.0f;
	Recent_LVL1_ID = 0;
	Recent_Global_ID = 0;
	Busy = 0.0f;
	DCMSessions = 0;
	LVL1DecodingErrors = 0;
	RoIB_Active_Mask = 0;
	RoIB_Bandwidth = new double[1];
	RoIB_Bandwidth[0] = 0;
	RoIB_Pending_RNP = 0;
	RoIB_Pending_DAQ = 0;
	RoIB_time_Build = 0;
	RoIB_time_Process = 0;
	RoIB_timeout = 0;
	RNP_Free_pages = new long[1];
	RNP_Free_pages[0] = 0;
	RNP_Used_pages = new long[1];
	RNP_Used_pages[0] = 0;
	RNP_Most_Recent_ID = new int[1];
	RNP_Most_Recent_ID[0] = 0;
	RNP_XOFF_state = new int[1];
	RNP_XOFF_state[0] = 0;
	RNP_XOFF_per = new double[1];
	RNP_XOFF_per[0] = 0;
	RNP_XOFF_count = new long[1];
	RNP_XOFF_count[0] = 0;
	RNP_Down_stat = new int[1];
	RNP_Down_stat[0] = 0;
	RNP_bufferFull = new int[0];
	RNP_numberOfLdowns = new short[0];

// <<BeginUserCode>>

// <<EndUserCode>>
    }

    public void publishGuts( is.Ostream out ) {
	super.publishGuts( out );
	out.put( LVL1Events, false ).put( AssignedEvents, false ).put( ReassignedEvents, false );
	out.put( ProcessedEvents, false ).put( AvailableCores, false ).put( FracAvailable );
	out.put( MaxAvailable, false ).put( Rate ).put( Recent_LVL1_ID, false ).put( Recent_Global_ID, false );
	out.put( Busy ).put( DCMSessions, false ).put( LVL1DecodingErrors, false ).put( RoIB_Active_Mask, false );
	out.put( RoIB_Bandwidth ).put( RoIB_Pending_RNP, false ).put( RoIB_Pending_DAQ, false );
	out.put( RoIB_time_Build, false ).put( RoIB_time_Process, false ).put( RoIB_timeout, false );
	out.put( RNP_Free_pages, false ).put( RNP_Used_pages, false ).put( RNP_Most_Recent_ID, false );
	out.put( RNP_XOFF_state, false ).put( RNP_XOFF_per ).put( RNP_XOFF_count, false );
	out.put( RNP_Down_stat, false ).put( RNP_bufferFull, false ).put( RNP_numberOfLdowns, false );
    }

    public void refreshGuts( is.Istream in ) {
	super.refreshGuts( in );
	LVL1Events = in.getLong(  );
	AssignedEvents = in.getLong(  );
	ReassignedEvents = in.getLong(  );
	ProcessedEvents = in.getLong(  );
	AvailableCores = in.getInt(  );
	FracAvailable = in.getFloat(  );
	MaxAvailable = in.getInt(  );
	Rate = in.getFloat(  );
	Recent_LVL1_ID = in.getInt(  );
	Recent_Global_ID = in.getLong(  );
	Busy = in.getFloat(  );
	DCMSessions = in.getInt(  );
	LVL1DecodingErrors = in.getLong(  );
	RoIB_Active_Mask = in.getInt(  );
	RoIB_Bandwidth = in.getDoubleArray(  );
	RoIB_Pending_RNP = in.getLong(  );
	RoIB_Pending_DAQ = in.getLong(  );
	RoIB_time_Build = in.getLong(  );
	RoIB_time_Process = in.getLong(  );
	RoIB_timeout = in.getLong(  );
	RNP_Free_pages = in.getLongArray(  );
	RNP_Used_pages = in.getLongArray(  );
	RNP_Most_Recent_ID = in.getIntArray(  );
	RNP_XOFF_state = in.getIntArray(  );
	RNP_XOFF_per = in.getDoubleArray(  );
	RNP_XOFF_count = in.getLongArray(  );
	RNP_Down_stat = in.getIntArray(  );
	RNP_bufferFull = in.getIntArray(  );
	RNP_numberOfLdowns = in.getShortArray(  );
    }


// <<BeginUserCode>>

// <<EndUserCode>>
}

// <<BeginUserCode>>

// <<EndUserCode>>

