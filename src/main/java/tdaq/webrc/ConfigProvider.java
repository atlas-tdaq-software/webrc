package tdaq.webrc ;

import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow ;
//import org.hibernate.validator.internal.util.logging.Log;
import org.apache.wicket.ajax.AjaxRequestTarget;

import java.util.Map ;
import java.util.Set ;
import java.util.HashSet ;
import java.util.List ;
import java.util.ArrayList ;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap ;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import config.Change;
import config.ConfigObject;
import daq.tokens.JWToken;
import rdb.RDBRepositoryVersion;

import java.util.AbstractMap.SimpleEntry ;

/** Class providing static methods to access Configuration (RDB) like getting Segment or Partition by ID.
    Used when building a RC tree in {@link RCTreeNodeProvider}. Keeps a collection of all Configuratons, {@link dal.Partition} objects and subscribers.
*/
public class ConfigProvider {

    final static String def_config_name = "rdbconfig:RDB" ;
    final static String def_initial_config_name = "rdbconfig:RDB_INITIAL" ;
    final static String def_rwconfig_name = "rdbconfig:RDB_RW" ;
    final static String def_server_name = "RDB" ;
    final static String def_rwserver_name = "RDB_RW" ;

    static Map<String, SimpleEntry<config.Configuration, dal.Partition>> allconfigs = new ConcurrentHashMap<String, SimpleEntry<config.Configuration, dal.Partition>>() ;
    static Map<String, config.Configuration> allrdbrws = new ConcurrentHashMap<String, config.Configuration>() ;
    static Map<String, config.Subscription> allsubsc = new ConcurrentHashMap<String, config.Subscription>() ;

    /** A per-partition map of appId to set of it's parent segments, used in ERS filter-by-tree */
    static Map<String, ConcurrentHashMap<String, Set<String>>> parents_map = new ConcurrentHashMap<String, ConcurrentHashMap<String, Set<String>>>() ;
  
    static final Lock rdbrwlock = new ReentrantLock() ;
    static final ReadWriteLock partrwlock = new ReentrantReadWriteLock() ;

    static synchronized void loadDB(final String partition_name) throws config.ConfigException
    {
        SimpleEntry<config.Configuration, dal.Partition> existing = allconfigs.get(partition_name) ;
        if ( existing != null ) {
            //existing.getKey().subscribe(subs) ;
			ers.Logger.debug(0, "Configuration already loaded for partition " + partition_name) ;
			return ;
        }

        final config.Configuration conf = new config.Configuration(partition_name.equals("initial") ? def_initial_config_name : def_config_name + "@" + partition_name) ;	
        final dal.Partition dalpart = dal.Algorithms.get_partition(conf, partition_name) ;
        final SimpleEntry<config.Configuration, dal.Partition> newentry =
            new SimpleEntry<config.Configuration, dal.Partition>(conf, dalpart) ;
        allconfigs.put(partition_name, newentry) ;
        
        parents_map.put(partition_name, new ConcurrentHashMap<String, Set<String>>()) ;

        config.Subscription localsubs = new config.Subscription(new config.Callback(){
        
            @Override
            public void process_changes(Change[] arg0, Object arg1) {
                ers.Logger.log("Configuration updated for partition " + partition_name) ;
				synchronized(ConfigProvider.class) {
                    final config.Configuration updatedconf = allconfigs.get(partition_name).getKey() ;
                    dal.Partition dalpart ;                
                    try {
                        dalpart = dal.Algorithms.get_partition(updatedconf, partition_name) ;
                    } catch ( final config.ConfigException ex ) {
                        ers.Logger.log("DB reload callback: failed to get partition: " + ex.getMessage() ) ;
                        return ;
                    }

                    // partrwlock.writeLock().lock() ;
                    allconfigs.get(partition_name).setValue(dalpart) ;
                    // partrwlock.writeLock().unlock() ;
                    /*
                    config.Configuration rdbrwconfig = getRdbRw(partition_name) ;
                    if ( rdbrwconfig != null ) {
                        try {
                            rdbrwlock.lock() ;
                            System.err.println("Reseting RDB RW cache in partition " + partition_name) ;
                            rdbrwconfig.unread_all_objects(true);
                        } finally {
                                    rdbrwlock.unlock() ;
                        }
                    }
                    */
                    parents_map.get(partition_name).clear() ;
                }
                RCNodesHolder.reloadPartition(partition_name) ;
                ers.Logger.log("DB reload callback: Partition reloaded") ;
            }
        }, null) ;

        try {
            ers.Logger.debug(0, "Subscribing in RDB in partition " + partition_name);
            RCCommandSender.setToken() ;
            conf.subscribe(localsubs) ; // throw
            ers.Logger.log("Subscribed in RDB in partition " + partition_name);
        } finally {
            RCCommandSender.clearToken() ; 
        } ;
        	
        if ( allsubsc.put(partition_name, localsubs) != null ) {
			ers.Logger.warning(new Exception("Already contained Subscription for partition " + partition_name));
		} ;
        ers.Logger.log("Partition " + partition_name + " loaded in Configuration holder");
    }

    /**
     * Returns existing or creates new Configuration for RDB RW, used to disable components. Invokes a remote call.
     * @param partition_name
     * @return config.Configuration
     * @throws config.SystemException
     */
    static synchronized config.Configuration getRdbRw(final String partition_name) throws config.SystemException {
        config.Configuration ret = allrdbrws.get(partition_name) ;
        if ( ret == null ) {
            try {
                RCCommandSender.setToken() ;
                ret = new config.Configuration(def_rwconfig_name + "@" + partition_name) ; // remote access to RDB_RW
                ers.Logger.log("Created new RDB RW Configuration for partition " + partition_name) ;
            }
            catch ( final config.SystemException ex ) {
                ers.Logger.error(ex) ;
                throw ex ; 
            }
            finally {
                RCCommandSender.clearToken() ; 
            }

            if ( allrdbrws.put(partition_name, ret) != null ) {
                ers.Logger.log("IMPOSSIBLE: Failed to store new RDB RW Configuration for partition " + partition_name) ;
            };
        }
        else {
            ers.Logger.log("Returning existing RDB RW Configuration for partition " + partition_name) ;
        }
        return ret ;
    }

    static synchronized void unsubscribe(final String partition_name) throws config.SystemException {
        SimpleEntry<config.Configuration, dal.Partition> existing = allconfigs.get(partition_name) ;
        config.Subscription subs = allsubsc.get(partition_name) ;
        if ( existing != null && subs != null ) {
            existing.getKey().unsubscribe(subs) ;
            ers.Logger.log(subs.toString() + " unsubscribed in RDB in partition " + partition_name) ;
        } 
    }

    static void exitCleanup() {
        ers.Logger.debug(0, "Cleaning up RDB subscriptions");
        for ( String partname: allconfigs.keySet() ) {
            try {
                unsubscribe(partname) ; 
            } catch ( final Exception ex ) {
                ex.printStackTrace() ;
            }        
        }
        ers.Logger.debug(0, "Unloading RDB RW sessions") ;
        for (config.Configuration cfg: allrdbrws.values() ) {
            try {
                cfg.unload() ;
            } catch ( final config.SystemException ex ) {
            }
            catch ( final Exception ex ) {
                ex.printStackTrace() ;
            }      
        }
    }

	/**
     * called when partition RootController exits, all RCNodes removed and clients notified
     * here we call unload and remove Configuration object from collection
     * synchronized method
     */ 
    static synchronized void clearConfig(final String partition_name) {
        ers.Logger.info("cleaning old Configuration objects in partition " + partition_name) ;
		SimpleEntry<config.Configuration, dal.Partition> config = allconfigs.remove(partition_name) ;
		if ( config != null ) {
			try {
				//config.getKey().unsubscribe() ;
				//ers.Logger.debug(0, "Unsubscribed Configuration in " + partition_name) ;
			} catch ( final Exception ex ) {
				ex.printStackTrace() ;
			}
			try {
				config.getKey().unload() ;
				ers.Logger.debug(0, "Unloaded Configuration in " + partition_name) ;
			} catch ( final Exception ex ) {
				ex.printStackTrace() ;
			}
		}
        //allconfigs.remove(partition_name) ;
        allrdbrws.remove(partition_name) ;
		allsubsc.remove(partition_name) ;
        parents_map.remove(partition_name) ;
    }

    static synchronized dal.Partition getPartition(final String partition_name) throws NoSuchElementException {
        try { 
//            partrwlock.readLock().lock() ;
			if ( allconfigs.containsKey(partition_name) ) {
				return allconfigs.get(partition_name).getValue() ;
			}
			else 
				throw new NoSuchElementException() ;
        }
        finally {
//            partrwlock.readLock().unlock() ;
        }
    }
    
    static synchronized dal.Segment getSegment(final String partition_name, final String segment_id) throws NoSuchElementException, config.ConfigException {
        try {
            partrwlock.readLock().lock() ;
            return allconfigs.get(partition_name).getValue().get_segment(segment_id) ;
        } 
        finally {
            partrwlock.readLock().unlock() ;
        }
    }
    
    static void addParents(final String partition, final String application, final Set<String> parents) {
        if ( !parents_map.containsKey(partition) ) {
            return ;
        }
        parents_map.get(partition).put(application, parents) ;
    }

    /** recursively fill the all_parents list */ 
    static void getParentSegments(final ConfigObject segment, List<ConfigObject> all_parents) {
        List<ConfigObject> parents = new ArrayList<ConfigObject>() ;
        try {
            segment.referenced_by(parents, "Segments", false) ;
        } catch ( final Exception ex ) {
            ex.printStackTrace();
            return ; 
        }
        for ( final ConfigObject pseg: parents ) {
            all_parents.add(pseg) ;
            getParentSegments(pseg, all_parents) ;
        }
    }
    
    /** Used in ERSProvider to filter ERS messages from applications by currently selected segment in the tree */
    public static boolean hasParent(final String partition, final String name, final String parent_name) {
        if ( !parents_map.containsKey(partition) ) {
            ers.Logger.debug(0, "ERS message from app "+ name + " that is not in loaded partition " + partition);
            return true ;
        }
        if ( parents_map.get(partition).containsKey(name) ) { return parents_map.get(partition).get(name).contains(parent_name) ; }
                
        // long start =  System.nanoTime() ; // DEBUG

        List<ConfigObject> parents = new ArrayList<ConfigObject>() ;            
        Set<String> parent_ids = new HashSet<String>() ;
        try {
            dal.BaseApplication app = dal.BaseApplication_Helper.get(allconfigs.get(partition).getKey(), name) ;
            dal.Segment seg = app.get_segment() ;
            parent_ids.add(seg.UID()) ;       
            ConfigObject confobj = seg.config_object() ;
            getParentSegments(confobj, parents) ;
            for ( final ConfigObject co_seg: parents ) { parent_ids.add(co_seg.UID() ) ; } ;  
            ers.Logger.debug(1,"parents of " + seg.UID() + ": " + parent_ids) ; // DEBUG
            
        } catch ( final config.NotFoundException ex ) { // not in this partition, some ghost...
            ers.Logger.debug(0, "ERS message from app "+ name + " that is not found in partition " + partition);
            return false ;
        }
        catch ( final Exception ex ) {
            ex.printStackTrace() ;
            return false ; 
        } 
        parents_map.get(partition).put(name, parent_ids) ;
        // System.err.println("Get parents took " + (System.nanoTime()-start)/1000 + "ms") ; // DEBUG
        return parent_ids.contains(parent_name) ;
    }   

    static synchronized java.util.List<RDBRepositoryVersion> getModifiedDbFiles(final String partition_name) 
    throws ipc.InvalidPartitionException, ipc.InvalidObjectException, rdb.CannotProceed, Exception {
        java.util.List<RDBRepositoryVersion> modifiedFiles = new java.util.ArrayList<RDBRepositoryVersion>();
        final rdb.cursor rdbServer = new ipc.Partition(partition_name).lookup(rdb.cursor.class, def_server_name);

        for (final RDBRepositoryVersion v: rdbServer.get_changes()) {
           // modifiedFiles.add(v.id + ": [" + v.user + "] - " + v.comment ) ;
            modifiedFiles.add(v) ;
        }        
        ////final rdb.writer rdbRWServer = new ipc.Partition(partition_name).lookup(rdb.writer.class, def_rwserver_name);
        //final rdb.RDBNameListHolder externallyUpdatedDataFiles = new rdb.RDBNameListHolder();
        //final rdb.RDBNameListHolder externallyRemovedDataFiles = new rdb.RDBNameListHolder();
        // rdbServer.get_modified_databases(externallyUpdatedDataFiles, externallyRemovedDataFiles);

        //Collections.addAll(modifiedFiles, externallyUpdatedDataFiles.value) ;
        //Collections.addAll(modifiedFiles, externallyRemovedDataFiles.value) ;
        
        return modifiedFiles ;
    }
    
    static boolean dbModified(final String partition_name) {
        try {
            java.util.List<RDBRepositoryVersion> modifiedFiles =  getModifiedDbFiles( partition_name) ;
            if ( !modifiedFiles.isEmpty() ) { return true ; }
        } catch ( final Exception ex ) {
            ex.printStackTrace() ;
        }
        return false ;
    }

    /**
     * reload all changed files on main RDB server
     * called after some Enable/Disable changes were made on RW RDB server
     * by user clicking on RELOAD_DB 
     * @throws different IPC, RDB exceptions
    */
    static void reloadDb(final String partition_name, AjaxRequestTarget target, ModalWindow modalWindow,
        final HomePage page)
    throws ipc.InvalidPartitionException, ipc.InvalidObjectException, rdb.CannotProceed, Exception {
        java.util.List<RDBRepositoryVersion> changes = getModifiedDbFiles(partition_name) ;
        if ( changes.size() == 0 ) {
            throw new RuntimeException("No modified files are known to main RDB server") ;
        }
        
        java.util.List<String> modifiedFiles = new java.util.ArrayList<String>();
        
        for (final RDBRepositoryVersion v: changes) {
            modifiedFiles.add(v.id + ": [" + v.user + "] - " + v.comment ) ;
        }   
        
        ers.Logger.log("Database files to reload: " + modifiedFiles);
        
        modalWindow.setContent(new DBReloadPanel(modalWindow.getContentId(), modifiedFiles.toString(), modalWindow, page) );
        modalWindow.showUnloadConfirmation(false) ;

        modalWindow.setCloseButtonCallback( trg -> {
            page.setDbReloadOk(false) ;
            return true ;
        } );

        modalWindow.setWindowClosedCallback( trg -> {
            if ( !page.getDbReloadOk() ) {
                ers.Logger.log("DB reload dialogue cancelled");
                return ;
            }
            // final String[] f = modifiedFiles.toArray(new String[modifiedFiles.size()]);
            rdbrwlock.lock() ; // hold processing of reload callback from main RDB
            try { 
                //final rdb.cursor rdbServer = new ipc.Partition(partition_name).lookup(rdb.cursor.class, def_server_name) ;
                //final rdb.writer rdbRWServer = new ipc.Partition(partition_name).lookup(rdb.writer.class, def_rwserver_name);
                
                    // ers.Logger.info("Got daq token: " + JWToken.acquire(JWToken.MODE.REUSE)) ;
                    // ers.Logger.log("Committing to RDB_RW...") ;
                ers.Logger.log("Versions to be reloaded: " + changes.get(0).id);    
                
                try {
                    RCCommandSender.setToken() ;
                    getPartition(partition_name).set_config_version("hash:" + changes.get(0).id, true) ;
                    ers.Logger.log("Finished DB reload, RC tree to be updated");
                } catch ( final Throwable ex ) {
                    ers.Logger.error(new Exception(ex)) ;
                    page.setException(ex.getMessage() , trg) ;
                } 
                finally {
                    RCCommandSender.clearToken() ;
                }
            
                // rdbServer.reload_database(f);
                // .getDalPartition().set_config_version(configVersion, true); setConfigurationAndReload("hash:" + versionToReload);
                //ers.Logger.log("Files were reloaded on RDB server");
                //rdbRWServer.reload_database(f);
                // ers.Logger.log("Files were reloaded on RDB_RW server");
                
                page.setDbReloadNeeded(false) ;
                page.dbReloaded(trg);
                page.getTreeProvider().setUpdated() ;
                trg.add(page.getRcTree()) ;
            } catch ( final org.omg.CORBA.TIMEOUT ex ) {
                final String ermsg = "Timeout to reload on RDB/RDB_RW server: " + ex.getMessage() ;
                page.setException(ermsg, trg) ;
            } catch ( final Exception ex ) {
                page.setException(ex.getMessage() , trg) ;
            }
            finally {
                rdbrwlock.unlock() ;
            }
            
            ers.Logger.debug(0, "Modal window setWindowClosedCallback finished") ;
            return ;
        } ) ;

        modalWindow.show(target) ;
    }

}
