package tdaq.webrc;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication ;
import org.apache.wicket.util.lang.Bytes;

/**
 * Application object for your web application.
 * If you want to run this application without deploying, run the Start class.
 * 
 * @see tdaq.Start#main(String[])
 */
public class WebRCApplicationRC extends WebApplication
{
	// either define System property tdaq.ipc.init.ref, $TDAQ_IPC_INIT_REF
	final static String ipcref_property = "tdaq.ipc.init.ref" ;
	final static String def_ipc_init = "corbaloc:iiop:pc-tbed-onl-01.cern.ch:14052/%ffipc/partition%00initial/ipc/partition/initial" ;

	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends WebPage> getHomePage()
	{
		return RCPanel.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init()
	{
		super.init();
		
		System.err.println("Initializing webrc RC application");

		getStoreSettings().setMaxSizePerSession(Bytes.kilobytes(5000));
		
		// avoid serialization
		setPageManagerProvider(new NoSerializationProvider(this));
	}
	
	@Override
	public void sessionUnbound(String sessionId) {
		super.sessionUnbound(sessionId);
		System.err.println("Session unbound: " + sessionId);
		//restartResponseAtSignInPage();
	}    
	
	@Override
	public void onDestroy() {
		super.onDestroy() ;
		ISReciever.exitCleanup() ;
		ConfigProvider.exitCleanup();
		System.err.println("Application destroyed");
	} 

}
