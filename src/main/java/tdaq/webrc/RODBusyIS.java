package tdaq.webrc ;

import java.util.HashMap;
import java.util.Map;

import tdaq.webrc.alti.*;
/**
 * Class providing access to ROD Busy busy published per detector segment in Monitoring IS server.
 * This has better granularity then the CTP busy data.
*/

public class RODBusyIS {

    // mapping of segments names to IS object names of class ROSBusyIS
    // https://atlasop.cern.ch/info/current/ATLAS/is/Monitoring/Monitoring.TileEBA_RODBusyModule/RODBusy
    // ROSBusyIS.ChannelName[0..15] (index 1) - some name, corresponding to segment
    // ROSBusyIS.BusyEnabled[0..15] (index 2) - must be True
    // ROSBusyIS.BusyFraction[0..15] (index 13) - Double value

    // more correct getting via ALTI:
    // https://atlasop.cern.ch/info/current/ATLAS/is/Monitoring/Monitoring.TileEBA_ALTI_Slave_CTPin.AltiMonitoring

    static final String prefix = "Monitoring." ;
    // static final String suffix = "_RODBusyModule/RODBusy" ;

    // "TRTBarrelA_Alti.AltiMonitoring"
    // static final String suffix = "_ALTI_Slave_CTPin.AltiMonitoring" ;
    static final Map<String, String> ROD_BUSY = new HashMap<String, String>() {
        private static final long serialVersionUID = -9657546744090L;
        {
        put("TileLBA", "TileLBA_ALTI_Slave_CTPin.AltiMonitoring") ;
        put("TileEBA", "TileEBA_ALTI_Slave_CTPin.AltiMonitoring") ; 
        put("TileLBC", "TileLBC_ALTI_Slave_CTPin.AltiMonitoring") ; 
        put("TileEBC", "TileEBC_ALTI_Slave_CTPin.AltiMonitoring") ; 
        put("TileLaser", "TileLaser_ALTI_Slave_CTPin.AltiMonitoring") ; 
		
		put("EMBA", "Alti_EMBA_CtpSlave.AltiMonitoring") ; 
		put("EMBC", "Alti_EMBC_CtpSlave.AltiMonitoring") ; 
		put("EMECA", "Alti_EMECA_CtpSlave.AltiMonitoring") ; 
		put("EMECC", "Alti_EMECC_CtpSlave.AltiMonitoring") ; 
		put("Digital_Trigger_A", "Alti_Digital_Trigger_A_CtpSlave.AltiMonitoring") ; 
		put("Digital_Trigger_C", "Alti_Digital_Trigger_C_CtpSlave.AltiMonitoring") ; 
		
		put("MDTBarrelA", "MDT-Alti-BA.AltiMonitoring") ;
		put("MDTBarrelC", "MDT-Alti-BC.AltiMonitoring") ;	
		put("MDTEndcapA", "MDT-Alti-EA.AltiMonitoring") ;
		put("MDTEndcapC", "MDT-Alti-EC.AltiMonitoring") ;

        put("TRTBarrelA", "TRTBarrelA_Alti.AltiMonitoring") ; 
		put("TRTBarrelC", "TRTBarrelC_Alti.AltiMonitoring") ; 
        put("TRTEndcapA", "TRTEndcapA_Alti.AltiMonitoring") ; 
        put("TRTEndcapC", "TRTEndcapC_Alti.AltiMonitoring") ; 
        put("SCT_Combined", "SCT_Alti_Combined.AltiMonitoring") ; 
        put("RCDPixelTTC", "Pixel_Alti.AltiMonitoring") ; 
        put("RCDIblTTC", "IBL_Alti.AltiMonitoring") ; 
//        put("L1Calo", "lvl1calo-alti2-forCombined.AltiMonitoring") ; // there are 3 inputs, TODO
		put("L1CT_MUCTPI", "Alti_MUCTPI.AltiMonitoring") ;
    }} ;

    static public boolean isRODBusyAvailable(final String det) {
        return ROD_BUSY.containsKey(det) ;
    }

    static public float getDetBusyFraction(final String partition, final String det) {
       
        if ( !ROD_BUSY.containsKey(det) ) return 0.0f ;

        float busy = 0.0f ;

        try {
            final is.Repository repo = new is.Repository(new ipc.Partition(partition)) ;
            // final is.AnyInfo info = new is.AnyInfo() ;
            final AltiMonitoringIS2 alti_is = new AltiMonitoringIS2() ;
            repo.getValue(prefix + ROD_BUSY.get(det), alti_is) ;
            
            for ( BusyIS2 busyis: alti_is.busy_counters ) {
                if ( busyis.name.equals("FP") )  {
                    busy += busyis.fraction ;
                    break ;
                }
            }
        }
        catch ( final is.RepositoryNotFoundException | is.InfoNotFoundException ex ) {
        }
        catch ( final is.InfoNotCompatibleException ex ) {
            System.err.println("Failed to read AltiMonitoringIS from " + ROD_BUSY.get(det) + ": " + ex) ;
        }
        catch ( final org.omg.CORBA.MARSHAL ex ) {
            System.err.println("Failed to read AltiMonitoringIS from " + ROD_BUSY.get(det) + ": " + ex) ;
        } 
        catch ( final Exception ex ) {

            ex.printStackTrace() ;
        }

        return busy;
    }

}