package tdaq.webrc ;

import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow ;
import org.apache.wicket.ajax.AjaxRequestTarget;

public class PartitionControl {

    public static void startPartition(AjaxRequestTarget target, ModalWindow modalWindow,
        final String dbpath, final HomePage page) {

        modalWindow.setAutoSize(true) ;

        int start = dbpath.lastIndexOf('/') +1 ;
        int end = dbpath.indexOf('.', start) ;
        String partition_name = dbpath.substring( start, end ) ;
        modalWindow.setTitle("Partition infrastructure control: " + partition_name) ;
        
        // dbpath is "daq/partitions/test.data.xml"

        //String partname = dbpath.substring(dbpath.lastIndexOf('/')+1, dbpath.firstIndexOf('.')) ;
        
        final PartitionControlPanel panel = 
        new PartitionControlPanel(modalWindow.getContentId(),
        Configuration.get("tdaq.release.setup"),
        partition_name,
        // Configuration.get("dbroot.path") + "/" + dbpath,
        dbpath,
        modalWindow, page.getDbSelector() ) ;

        modalWindow.setContent(panel) ;
        modalWindow.showUnloadConfirmation(false) ;

        modalWindow.setCloseButtonCallback( trg -> {
            panel.cleanup() ;
            return true ;
        } );

        modalWindow.setWindowClosedCallback( trg -> { 
            if ( panel.launchStatus() ) {
                System.err.println("Connecting to just started partition " + partition_name) ;
                page.PartitionConnected(partition_name, trg);
            }
            return ;
        } );

        modalWindow.show(target) ;
        }
   }
