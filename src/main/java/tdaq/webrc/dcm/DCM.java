package tdaq.webrc.dcm;


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * 
 * @author  produced by the IS generator
 */

public class DCM extends is.Info {
    public static final is.Type type = new is.Type( new DCM( ) );


    /**
     * Instantaneous L1 rate [Hz]
     */
    public double              L1Rate;

    /**
     * Average L1 rate [Hz]
     */
    public double              L1RateAvg;

    /**
     * Instantaneous EB rate [Hz]
     */
    public double              EbRate;

    /**
     * Average EB rate [Hz]
     */
    public double              EbRateAvg;

    /**
     * Instantaneous DC request rate [Hz]
     */
    public double              DcReqRate;

    /**
     * Average DC request rate [Hz]
     */
    public double              DcReqRateAvg;

    /**
     * Instantaneous output rate [Hz]
     */
    public double              OutRate;

    /**
     * Average output rate [Hz]
     */
    public double              OutRateAvg;

    /**
     * Data Collector: received data bandwidth (B/s)
     */
    public double              DcDataBW;

    /**
     * Output: delivered data bandwidth (B/s)
     */
    public double              OutputDataBW;

    /**
     * L1Source: number of events requested
     */
    public long                L1SourceRequestedEvents;

    /**
     * L1Source: number of events received
     */
    public long                L1SourceReceivedEvents;

    /**
     * L1Source: number of events acknowledged as done
     */
    public long                L1SourceDoneEvents;

    /**
     * L1Source: number of connection timeouts
     */
    public long                L1SourceConnectionTimeouts;

    /**
     * L1Source: number of connection errors
     */
    public long                L1SourceConnectionErrors;

    /**
     * Processor: requested events
     */
    public long                ProxReqEvents;

    /**
     * Processor: fetched L1 events
     */
    public long                ProxL1Events;

    /**
     * Processor: rejected events 
     */
    public long                ProxRejEvents;

    /**
     * Processor: accepted events 
     */
    public long                ProxAccEvents;

    /**
     * Processor: number of busy PUs
     */
    public long                ProxBusyPUs;

    /**
     * Processor: number of free PUs
     */
    public long                ProxFreePUs;

    /**
     * Processor: number of PUs waiting for L1R
     */
    public long                ProxIdlePUs;

    /**
     * Processor: done events
     */
    public long                ProxDoneEvents;

    /**
     * Processor: number of hltpu processing timeouts
     */
    public long                ProxHltpuTimeouts;

    /**
     * Processor: number of hltpu crashes detected by the dcm
     */
    public long                ProxHltpuCrashes;

    /**
     * Number of events owned by the dcm
     */
    public int                 EventsInside;

    /**
     * Number of events waiting to be delivered to SFOs
     */
    public int                 EventsOnOutputQueue;

    /**
     * Set to 1 if all the HLTPUs are busy processing
     */
    public float               BusyProcessing;

    /**
     * Set to 1 if the output queue is full
     */
    public float               BusyFromOutput;

    /**
     * Data Collector: number of ROB requests
     */
    public long                DcROBRequests;

    /**
     * Data Collector: pending transactions
     */
    public long                DcPendingRequests;

    /**
     * Data Collector: available traffic shaping credits
     */
    public long                DcTrafficShapingCredits;

    /**
     * Data Collector: received ROB data (B)
     */
    public long                DcReceivedData;

    /**
     * Data Collector: number of active connections
     */
    public long                DcActiveConnections;

    /**
     * Data Collector: number of connection timeouts
     */
    public long                DcConnectionTimeouts;

    /**
     * Data Collector: number of connection errors
     */
    public long                DcConnectionErrors;

    /**
     * EB: events built
     */
    public long                EbEvents;

    /**
     * EB: events sampled via emon
     */
    public long                SampledEvents;

    /**
     * Output: events received by EB
     */
    public long                OutputEvents;

    /**
     * Output: events successfully delivered or saved
     */
    public long                OutputDeliveredEvents;

    /**
     * Output: delivered data (B)
     */
    public long                OutputDeliveredData;

    /**
     * Output: number of active connections
     */
    public long                OutputActiveConnections;

    /**
     * Output: number of connection timeouts
     */
    public long                OutputConnectionTimeouts;

    /**
     * Output: number of connection errors
     */
    public long                OutputConnectionErrors;

    /**
     * SBA file fullpath
     */
    public String              SbaFile;

    /**
     * Occupancy of the most loaded block [%]
     */
    public float               BlockOccupancy;

    /**
     * dcm cpu usage [%]
     */
    public float               cpuUsageProc;

    /**
     * node cpu usage [%]
     */
    public float               cpuUsageNode;

    /**
     * Bytes this process caused to be sent to storage layer
     */
    public long                DiskWrBytes;

    /**
     * Bytes rate sent to storage layer
     */
    public float               DiskWrBytesRate;


    public DCM() {
	this( "DCM" );
    }

    protected DCM( String type ) {
	super( type );
	L1Rate = 0;
	L1RateAvg = 0;
	EbRate = 0;
	EbRateAvg = 0;
	DcReqRate = 0;
	DcReqRateAvg = 0;
	OutRate = 0;
	OutRateAvg = 0;
	DcDataBW = 0;
	OutputDataBW = 0;
	L1SourceRequestedEvents = 0;
	L1SourceReceivedEvents = 0;
	L1SourceDoneEvents = 0;
	L1SourceConnectionTimeouts = 0;
	L1SourceConnectionErrors = 0;
	ProxReqEvents = 0;
	ProxL1Events = 0;
	ProxRejEvents = 0;
	ProxAccEvents = 0;
	ProxBusyPUs = 0;
	ProxFreePUs = 0;
	ProxIdlePUs = 0;
	ProxDoneEvents = 0;
	ProxHltpuTimeouts = 0;
	ProxHltpuCrashes = 0;
	EventsInside = 0;
	EventsOnOutputQueue = 0;
	BusyProcessing = 0f;
	BusyFromOutput = 0f;
	DcROBRequests = 0;
	DcPendingRequests = 0;
	DcTrafficShapingCredits = 0;
	DcReceivedData = 0;
	DcActiveConnections = 0;
	DcConnectionTimeouts = 0;
	DcConnectionErrors = 0;
	EbEvents = 0;
	SampledEvents = 0;
	OutputEvents = 0;
	OutputDeliveredEvents = 0;
	OutputDeliveredData = 0;
	OutputActiveConnections = 0;
	OutputConnectionTimeouts = 0;
	OutputConnectionErrors = 0;
	SbaFile = "";
	BlockOccupancy = 0f;
	cpuUsageProc = 0f;
	cpuUsageNode = 0f;
	DiskWrBytes = 0;
	DiskWrBytesRate = 0f;

// <<BeginUserCode>>

// <<EndUserCode>>
    }

    public void publishGuts( is.Ostream out ) {
	super.publishGuts( out );
	out.put( L1Rate ).put( L1RateAvg ).put( EbRate ).put( EbRateAvg ).put( DcReqRate );
	out.put( DcReqRateAvg ).put( OutRate ).put( OutRateAvg ).put( DcDataBW ).put( OutputDataBW );
	out.put( L1SourceRequestedEvents, false ).put( L1SourceReceivedEvents, false ).put( L1SourceDoneEvents, false );
	out.put( L1SourceConnectionTimeouts, false ).put( L1SourceConnectionErrors, false );
	out.put( ProxReqEvents, false ).put( ProxL1Events, false ).put( ProxRejEvents, false );
	out.put( ProxAccEvents, false ).put( ProxBusyPUs, false ).put( ProxFreePUs, false );
	out.put( ProxIdlePUs, false ).put( ProxDoneEvents, false ).put( ProxHltpuTimeouts, false );
	out.put( ProxHltpuCrashes, false ).put( EventsInside, false ).put( EventsOnOutputQueue, false );
	out.put( BusyProcessing ).put( BusyFromOutput ).put( DcROBRequests, false ).put( DcPendingRequests, false );
	out.put( DcTrafficShapingCredits, false ).put( DcReceivedData, false ).put( DcActiveConnections, false );
	out.put( DcConnectionTimeouts, false ).put( DcConnectionErrors, false ).put( EbEvents, false );
	out.put( SampledEvents, false ).put( OutputEvents, false ).put( OutputDeliveredEvents, false );
	out.put( OutputDeliveredData, false ).put( OutputActiveConnections, false ).put( OutputConnectionTimeouts, false );
	out.put( OutputConnectionErrors, false ).put( SbaFile ).put( BlockOccupancy ).put( cpuUsageProc );
	out.put( cpuUsageNode ).put( DiskWrBytes, false ).put( DiskWrBytesRate );
    }

    public void refreshGuts( is.Istream in ) {
	super.refreshGuts( in );
	L1Rate = in.getDouble(  );
	L1RateAvg = in.getDouble(  );
	EbRate = in.getDouble(  );
	EbRateAvg = in.getDouble(  );
	DcReqRate = in.getDouble(  );
	DcReqRateAvg = in.getDouble(  );
	OutRate = in.getDouble(  );
	OutRateAvg = in.getDouble(  );
	DcDataBW = in.getDouble(  );
	OutputDataBW = in.getDouble(  );
	L1SourceRequestedEvents = in.getLong(  );
	L1SourceReceivedEvents = in.getLong(  );
	L1SourceDoneEvents = in.getLong(  );
	L1SourceConnectionTimeouts = in.getLong(  );
	L1SourceConnectionErrors = in.getLong(  );
	ProxReqEvents = in.getLong(  );
	ProxL1Events = in.getLong(  );
	ProxRejEvents = in.getLong(  );
	ProxAccEvents = in.getLong(  );
	ProxBusyPUs = in.getLong(  );
	ProxFreePUs = in.getLong(  );
	ProxIdlePUs = in.getLong(  );
	ProxDoneEvents = in.getLong(  );
	ProxHltpuTimeouts = in.getLong(  );
	ProxHltpuCrashes = in.getLong(  );
	EventsInside = in.getInt(  );
	EventsOnOutputQueue = in.getInt(  );
	BusyProcessing = in.getFloat(  );
	BusyFromOutput = in.getFloat(  );
	DcROBRequests = in.getLong(  );
	DcPendingRequests = in.getLong(  );
	DcTrafficShapingCredits = in.getLong(  );
	DcReceivedData = in.getLong(  );
	DcActiveConnections = in.getLong(  );
	DcConnectionTimeouts = in.getLong(  );
	DcConnectionErrors = in.getLong(  );
	EbEvents = in.getLong(  );
	SampledEvents = in.getLong(  );
	OutputEvents = in.getLong(  );
	OutputDeliveredEvents = in.getLong(  );
	OutputDeliveredData = in.getLong(  );
	OutputActiveConnections = in.getLong(  );
	OutputConnectionTimeouts = in.getLong(  );
	OutputConnectionErrors = in.getLong(  );
	SbaFile = in.getString(  );
	BlockOccupancy = in.getFloat(  );
	cpuUsageProc = in.getFloat(  );
	cpuUsageNode = in.getFloat(  );
	DiskWrBytes = in.getLong(  );
	DiskWrBytesRate = in.getFloat(  );
    }


// <<BeginUserCode>>

// <<EndUserCode>>
}

// <<BeginUserCode>>

// <<EndUserCode>>

