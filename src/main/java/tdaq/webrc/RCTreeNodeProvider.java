package tdaq.webrc; 

import java.util.Iterator;
import java.util.List;
import java.lang.ref.WeakReference;
// import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.wicket.extensions.markup.html.repeater.tree.ITreeProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

/**
 * A provider of RCNodes for Tree of a particular partition, implementing ITreeProvider interface for each HomePage (i.e. session).
 * RCNodes instances are held in RCNodesHolder once per partition, thus shared by all HomePages and sessions looking to the same partition.
 * IS callbacks are thus also shared, i.e. there is a single instance of RCNode created and maintained per partitition.
 * 
 * @see RCNodesHolder
 * @see RCNode
 */
public class RCTreeNodeProvider implements ITreeProvider<RCNode> {

    private static final long serialVersionUID = 1345678535L;
   
    private RCNode emptyRoot = new RCNode("Partition not selected", "") ;
    private AtomicBoolean partitionLoaded = new AtomicBoolean(false) ;
    private String partition = new String() ;
    private AtomicBoolean updated = new AtomicBoolean(true) ;
    private WeakReference<IHomePage> homePage = null ;

    // private List<RCNode> updatedNodes = new ArrayList<RCNode>() ;
    private RCNode selectedNode = null ;

    public RCTreeNodeProvider(final IHomePage hp) {
        homePage = new WeakReference<IHomePage>(hp) ;
    }

	/** destructor, called when HomePage is destroyed (e.g. web page closed or disconnected), need to update global objects */
    public void unregister() {
        ers.Logger.log("Unregistering RCTreeNodeProvider in partition " + partition);
        unsubscribe(partition) ;
		RCNodesHolder.removeProvider(partition, this) ;
	}

    /** can be called asyncronously from Configuration reload callback -> RCNodesHolder  */
    public synchronized void partitionReloading() {
        partitionLoaded.set(false) ;
        emptyRoot = new RCNode("Partition reloading...", "") ;
        setUpdated() ;
    }
    
    public synchronized void partitionReloaded() {
        partitionLoaded.set(true) ;
        setUpdated() ;
    }
    
    /**
     * called from PMG callback when detecting that RootController exited
     * here we empty the RC tree and notify HomePage
    */ 
    public synchronized void partitionGone() {
        if ( !partitionLoaded.get() ) { return ; } 
        partitionLoaded.set(false) ;
        ers.Logger.log("RCNodeProvider: partition " + partition + " has gone, emptying RCTree");
        emptyRoot = new RCNode("Partition " + partition + " has gone, select one from the list", "") ;
        setUpdated() ;
	//	unsubscribe(partition) ;
        IHomePage hp = homePage.get() ; if ( hp != null ) { hp.partitionGone(partition) ; } ;
    }

    /** called when user selects another partition in the dropdown choice */
    public void partitionChanged(final String new_partition) throws is.RepositoryNotFoundException, is.InvalidCriteriaException, is.AlreadySubscribedException {
        final String old_partition = partition ;

        if ( !old_partition.isEmpty() ) {
            ers.Logger.log("No longer providing partition " + old_partition + ", unregistering in RCNodesHolder");
			RCNodesHolder.removeProvider(old_partition, this) ; //
			unsubscribe(old_partition) ; // IS subscription
		} ; 

        partition = new_partition ; //ConfigProvider.getPartition().UID() ;
        if ( null == RCNodesHolder.newPartition(partition, this) ){
            ers.Logger.log("Loading new RC tree for " + partition);
            new RCNode(ConfigProvider.getPartition(partition)) ;
            ers.Logger.debug(0, "New RC tree loaded");
        } else {
            ers.Logger.debug(0, "RC tree is already loaded in the holder for partition " + partition);
        }
        
        subscribe(new_partition) ;
        partitionLoaded.set(true) ;
    }

    /** subscribe for IS changes in shared ISReciever
     * @see ISReciever
     */
    void subscribe(final String partname) throws is.RepositoryNotFoundException, is.InvalidCriteriaException, is.AlreadySubscribedException {
//        if ( Configuration.get("is.mode.pull").equals("false") ) {
            ISReciever.subscribe(partname, this) ; // throw
//        }
    }

    /** signals that RC tree was updated and needs re-rendering */
    public void setUpdated() { 
        updated.set(true);
    }

    /** mark the last clicked node as selected for applying particular CSS when rendering */
    public void setSelectedNode(final RCNode node) {
        updated.set(true) ;
        if ( node == null ) {
            selectedNode.setSelected(false) ;
            return ;
        }        
        if ( selectedNode != null ) {
            selectedNode.setSelected(false) ;
        }
        selectedNode = node ;
        node.setSelected(true);
    }

    public boolean checkUpdated() { // needs re-rendering
        return updated.getAndSet(false) ;
    }

    /** Notifies the parent Home page holding this tree that the partition (root) state changed */ 
    public void rootStateChanged(final String rcstate, boolean busy) {
        IHomePage hp = homePage.get() ; if ( hp == null ) { return ; } ;
        hp.globalRCStateChanged(rcstate, busy) ;
        updated.set(true) ;
    }

    /** unsubscribes from IS changes in shared ISReciever
     * 
     * @see ISReciever
    */
    void unsubscribe(final String oldpart) {
//        if ( Configuration.get("is.mode.pull").equals("false") ) {
            try {
                ISReciever.unsubscribe(oldpart, this) ;    
                ers.Logger.log("IS receiver unsubscribed in partition " + oldpart) ;    
            } catch ( final Exception ex ) {
                ex.printStackTrace();
            } 
            //try { super.finalize() ; }   // why that was there? GC should do the thing
            //catch ( final Throwable ex ) {}
//        }
    }

    /** returns the partition root node from RCNodesHolder, implementation of the interface */
    @Override
    public Iterator<RCNode> getRoots()
    {
        // System.err.println("getRoots requested for " + partition);
        List<RCNode> ret = new CopyOnWriteArrayList<RCNode>() ;
        if ( partitionLoaded.get() ) {
            RCNode root = RCNodesHolder.findNode(partition, partition.equals("initial") ? "DefaultRootController" : "RootController") ;
            if ( root != null && root.isLoaded() ) {
                ret.add(root) ;
            } else {
                ret.add(emptyRoot) ;
            }
        } else {
            ret.add(emptyRoot) ; 
        }
        return ret.iterator();
    }

    /** implements a logic of hasChildren of the interface */
    @Override
    public boolean hasChildren(RCNode node)
    {
        //System.err.println("hasChildren requested for Node " + node.getName() + ":" + node.isSegment());
        if ( !partitionLoaded.get() ) return false ;
        if ( node == null ) return false ;
        return node.isSegment() && node.isEnabled() && node.hasChildren();
    }

    /** implements access to RCNode children of the interface */
    @Override
    public Iterator<RCNode> getChildren(final RCNode node)
    {
        //System.err.println("getChildren requested for Node " + node.getName());
        if ( partitionLoaded.get() ) {
            IHomePage hp = homePage.get() ;
            if ( hp != null ) {  
                return node.getChildren(hp.getGlobalRCState(), hp.getMode()).iterator() ;
            }
        }
        return new java.util.concurrent.CopyOnWriteArrayList<RCNode>().iterator() ;
    }

    /** implements lazy-loading of children of the interface */
    @Override
    public IModel<RCNode> model(RCNode node)
    {
        if ( partitionLoaded.get() ) {
            try {
                // System.err.println("Tree accessing " + node.getName());
            	if ( node.getParent() != null ) { node.loadChildren(0) ; } ; // requested to display node, need to load children
                if ( Configuration.get("is.mode.pull").equals("true") && 
                        !node.getName().endsWith(":infrastructure") && !node.getName().equals("setup") ) {
                    node.pullAppState() ; // poll from IS, no callbacks
                    node.pullRCState() ;
                }
                return new RCNodeModel(node, partition);
            } catch ( final Exception ex ) {
                ex.printStackTrace() ;
                return new RCNodeModel(new RCNode(ex.getMessage(), ""), partition) ;
            }
        }
        else {
            return new RCNodeModel(emptyRoot, partition) ;
        }
    }

    @Override
    public void detach()
    {
    }

    private static class RCNodeModel extends LoadableDetachableModel<RCNode>
    {
        private static final long serialVersionUID = 3464563456L ;
        private final String id ;
        private final String partition ;
        // private final RCTreeNodeProvider prov ;

        public RCNodeModel(RCNode node, final String part )
        {
            super(node) ;
            id = node.getName() ;
            partition = part ;
        }

        @Override
        protected RCNode load()
        {
            return RCNodesHolder.findNode(partition, id);
        }

        @Override
        public boolean equals(Object obj)
        {
            if (obj instanceof RCNodeModel)
            {
                return ((RCNodeModel)obj).id.equals(id);
            }
            return false;
        }

        @Override
        public int hashCode()
        {
            return (id+partition).hashCode() ;
        }
    } 
}