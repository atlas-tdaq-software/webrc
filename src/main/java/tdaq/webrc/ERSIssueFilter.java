package tdaq.webrc ;

public class ERSIssueFilter {

    enum SELECTOR { ALL('*'), CONTAINS, EQUAL('='), NOT('!'), BEGIN('~') ;
        public char character = ' ' ;
        SELECTOR(final char ch) {
            character = ch ;
        }
        SELECTOR() { }
        public boolean select(final String pattern) {
            if ( pattern == null ) return false ;
            if ( pattern.isEmpty() ) return false ;
            return pattern.charAt(0) == character ;
        }
    } ; 

    private String severity = "*";
    private SELECTOR severity_sel = SELECTOR.ALL ;
    private String time = "*";
    private SELECTOR time_sel = SELECTOR.ALL ;
    private String id = "*";
    private SELECTOR id_sel = SELECTOR.ALL ;
    private String app = "*";
    private SELECTOR app_sel = SELECTOR.ALL ;
    private String message = "*";
    private SELECTOR message_sel = SELECTOR.ALL ;

    public void setTime(final String pattern) {
        if ( pattern == null ) return ;
        time_sel = getSelector(pattern) ;
        if ( time_sel == SELECTOR.CONTAINS ) {
            time = pattern ;
        } else {
            time = pattern.substring(1) ;
        }
    }

    public void setSeverity(final String pattern) {
        if ( pattern == null ) return ;
        severity_sel = getSelector(pattern) ;
        if ( severity_sel == SELECTOR.CONTAINS ) {
            severity = pattern ;
        } else {
            severity = pattern.substring(1) ;
        }
    }

    public void setId(final String pattern) {
        if ( pattern == null ) return ;
        id_sel = getSelector(pattern) ;
        if ( id_sel == SELECTOR.CONTAINS ) {
            id = pattern ;
        } else {
            id = pattern.substring(1) ;
        }
    }

    public void setApp(final String pattern) {
        if ( pattern == null ) return ;
        app_sel = getSelector(pattern) ;
        if ( app_sel == SELECTOR.CONTAINS ) {
            app = pattern ;
        } else {
            app = pattern.substring(1) ;
        }
    }

    public void setMessage(final String pattern) {
        if ( pattern == null ) return ;
        message_sel = getSelector(pattern) ;
        if ( message_sel == SELECTOR.CONTAINS ) {
            message = pattern ;
        } else {
            message = pattern.substring(1) ;
        }
    }

    public boolean match(final ERSIssue issue) {
        return  match_string(id, id_sel, issue.getId() ) &&
                match_string(time, time_sel, issue.getTime()) &&
                match_string(message, message_sel, issue.getMessage()) &&
                match_string(app, app_sel, issue.getApplicationParent()) &&
                match_string(severity, severity_sel, issue.getSeverityParent() ) ;
    }

    private SELECTOR getSelector(final String pattern) {
        if ( SELECTOR.ALL.select(pattern) ) {
            return SELECTOR.ALL ;
        }
        if ( SELECTOR.EQUAL.select(pattern) ) {
            return SELECTOR.EQUAL ;
        }
        if ( SELECTOR.NOT.select(pattern) ) {
            return SELECTOR.NOT ;
        }
        if ( SELECTOR.BEGIN.select(pattern) ) {
            return SELECTOR.BEGIN ;
        }
        return SELECTOR.CONTAINS ;
    }

    private boolean match_string(final String pattern, final SELECTOR sel, final String value) {
        switch ( sel ) {
            case ALL:      return true ;
            case CONTAINS: return value.contains(pattern) ;
            case NOT:      return !value.contains(pattern) ;
            case EQUAL:    return value.equals(pattern) ;
            case BEGIN:    return value.startsWith(pattern) ;
            default:       return true ;
        }
    }
}