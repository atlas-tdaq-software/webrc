package tdaq.webrc ;

import java.util.List ;
import java.lang.ref.WeakReference ;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.ajax.AbstractAjaxTimerBehavior;
import org.apache.wicket.ajax.AjaxEventBehavior ;
import org.apache.wicket.util.time.Duration;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.IAjaxIndicatorAware ;
import org.apache.wicket.extensions.ajax.markup.html.AjaxIndicatorAppender ;


public class PartitionsChoice
    extends DropDownChoice<String>
    implements IAjaxIndicatorAware {

    private final String super_partition = "ATLAS" ;
    private static final long serialVersionUID = 156456456L;
    private final AbstractAjaxTimerBehavior updateBeh ;
    private final AjaxIndicatorAppender indicatorAppender ;
    private int partitionsHash = 0 ;
    private String gonePartition ;
    private String selectedPartition = "Select partition!" ;
    private IModel<String> selection = new PropertyModel<String>(this, "selectedPartition") ;
    private WeakReference<HomePage> homePage ;

    public PartitionsChoice(final String id, final HomePage page) {
        super(id, () -> Partitions.getIPCPartitionNames(false)) ;
        List<String> list = Partitions.getIPCPartitionNames(true) ;
        if ( Partitions.getIPCPartitionNames(true).contains(super_partition) ) {
           // setModel(Model.of("part_hlt_akazarov")) ;
            selectedPartition = super_partition ;
            System.err.println("Default partition: " + super_partition) ;
          
            final AbstractAjaxTimerBehavior initial_update =
            new AbstractAjaxTimerBehavior(Duration.milliseconds(1000)) {
                @Override
                protected void onTimer(AjaxRequestTarget target) {
                    final HomePage hp = homePage.get() ;
                    if ( hp != null ) {    
                        hp.PartitionConnected(super_partition, target) ;
                        stop(target) ;
                    }
                }
            };
           add(initial_update) ;
        }

        setDefaultModel(selection) ;
        homePage = new WeakReference<HomePage>(page) ;
        indicatorAppender = new AjaxIndicatorAppender();
        add(indicatorAppender) ;

        updateBeh = new AbstractAjaxTimerBehavior(Duration.milliseconds(15000)) {
            private static final long serialVersionUID = 5680069957499209316L;

            @Override
            protected void onTimer(AjaxRequestTarget target) {
                List<String> list = Partitions.getIPCPartitionNames(true) ; //refresh hash
                int newhash = Partitions.hash() ;

                if ( partitionsHash != newhash ) {
                    if ( list.contains(gonePartition) ) {
                        rc.RCStateInfo root = ISReciever.getRCInfo( gonePartition, gonePartition.equals("initial") ? HomePage.default_rc_app_name : HomePage.rc_app_name) ;
                        if ( !root.state.equals("NONE") ) {
                            // ers.Logger.log("Partition not yet ready " + gonePartition + ": " + root.state) ;
                            return ;
                        }
                    
                        ers.Logger.log("Partition re-appeared: " + gonePartition) ;
                        final HomePage hp = homePage.get() ;
                        if ( hp != null ) { 
                            hp.PartitionConnected(gonePartition, target) ;
                            gonePartition = "" ;
                        }
                    }
                    partitionsHash = newhash ;
                    target.add(PartitionsChoice.this) ;
                } 
            }
        } ;	

		add(updateBeh);

		add(new AjaxEventBehavior("focus") {
			private static final long serialVersionUID = 7494244169801461459L;

            protected void onEvent(AjaxRequestTarget target) {
				updateBeh.stop(target);
			}; } ) ; 
		
		add(new AjaxEventBehavior("blur") {
			private static final long serialVersionUID = 1827653164937430104L;

            protected void onEvent(AjaxRequestTarget target) {
				updateBeh.restart(target);
			}
			} ) ; 

    }

    public String getSelectedPartition() { return selectedPartition ; } ;
    public void setSelectedPartition(final String part) { selectedPartition = part ; } ;

    @Override
    public String getAjaxIndicatorMarkupId() {
        return "waiting" ;
    } 

    @Override
    protected String getNullKeyDisplayValue() {
        System.err.println("Getting getNullKeyDisplayValue") ;           
        return "Select partition" ;
    }
    
    @Override
    protected void onModelChanged() {
        System.err.println("Model changed, current model: " + getModelObject()) ;           
    }


    @Override
    protected CharSequence getDefaultChoice(String selectedValue) {
        // System.err.println("getDefaultChoice " + selectedValue + ": " + super.getDefaultChoice(selectedValue)) ;           
        
        CharSequence ret = super.getDefaultChoice(selectedValue) ;
        if ( ret.length() == 0 && !selectedPartition.equals(super_partition) )
            { return "<option selected=\"selected\" value=\"\">Select partition</option>" ; }
        return ret ;
    }

    public void partitionGone(final String partition) {
        gonePartition = partition ;
        if ( partition.equals("") ) { return ; }
        ers.Logger.log("Partiton " + partition + " has gone") ;
        Partitions.getIPCPartitionNames(false) ;
        clearInput() ;
    }

    /* this does not work, as CSS can not be (easily) applied to select/option elements 
    Seems to be working on: Firefox/Windows, Chromium/Linux, Firefox/MacOs
    Not working: Firefox 82.0.3/Linux (CC7, Ubuntu), Safari/MacOs
    */
    @Override
    protected void setOptionAttributes(org.apache.wicket.util.string.AppendingStringBuffer buffer, String choice, int index, String selected) {
        super.setOptionAttributes(buffer, choice, index, selected) ;
        final int pos = choice.indexOf(" [") ;
		final String partition = (pos == -1 ? choice : choice.substring(0, pos)) ;
		
        rc.RCStateInfo root = ISReciever.getRCInfo( partition,
            partition.equals("initial") ? HomePage.default_rc_app_name : HomePage.rc_app_name) ;
		buffer.append(" state=\"" + root.state + "\"");
    }
   
}