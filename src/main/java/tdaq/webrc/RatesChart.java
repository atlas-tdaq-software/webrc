package tdaq.webrc;

import java.awt.Font;
import java.awt.image.BufferedImage ;
import java.awt.Color ;

import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.request.resource.DynamicImageResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.request.resource.AbstractResource ;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.time.Duration ;
import org.apache.wicket.request.http.WebResponse.CacheScope ;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.ui.TextAnchor;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.renderer.xy.XYAreaRenderer ;
import org.jfree.chart.labels.StandardXYItemLabelGenerator ;
import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.axis.NumberTickUnit;

// instance of this Chart and corresponding Plot created for each HomePage
// what is shared by all clients @see TrigRatesProvider
public class RatesChart extends Image {

    // prevent JFreeChart constructor to connect to X11 to get some defaults (in case DISPLAY is set)
    static {
        System.setProperty("java.awt.headless", "true");
    }
    
    private static final long serialVersionUID = -4330498695319845253L;
    private final int width;
    private final int height;
    private XYPlot ratesPlot ;
    private JFreeChart chart = null ; 

    public RatesChart(String id, int width, int height){
        super(id);
   
        this.width = width;
        this.height = height;
    }

    private void Construct() {
        
        LogarithmicAxis yAxisRates = new org.jfree.chart.axis.LogarithmicAxis("Rates, Hz");
        LogarithmicAxis yAxisBusy = new org.jfree.chart.axis.LogarithmicAxis("Busy fraction, %");
        ratesPlot = new XYPlot() ;
        
        ratesPlot.setDomainAxis(new org.jfree.chart.axis.DateAxis());
        ratesPlot.setRangeAxis(0, yAxisRates) ;
        ratesPlot.setRangeAxis(1, yAxisBusy) ;
        // yAxisBusy.autoAdjustRange();
        // yAxisBusy.setInverted(true) ;
        yAxisBusy.setRange(0,100) ;
        yAxisBusy.setTickLabelsVisible(true) ;
        yAxisBusy.setMinorTickMarksVisible(true) ;
        yAxisBusy.setTickUnit(new NumberTickUnit(10));
        //yAxisRates.setAutoRange(true);

        yAxisRates.setRange(-3, 200000);
        
        XYLineAndShapeRenderer rates_rend = new XYLineAndShapeRenderer(true, false) ;
        rates_rend.setDefaultItemLabelGenerator(new StandardXYItemLabelGenerator()) ;
        rates_rend.setSeriesItemLabelFont(3, new java.awt.Font("Open Sans" ,Font.BOLD, 14)) ;
        rates_rend.setSeriesItemLabelPaint(3, Color.RED);
        rates_rend.setSeriesItemLabelsVisible(3, true) ;
        rates_rend.setSeriesVisibleInLegend(3, false) ;
        rates_rend.setSeriesItemLabelFont(4, new java.awt.Font("Open Sans" ,Font.PLAIN, 14)) ;
        rates_rend.setSeriesItemLabelPaint(4, Color.BLUE);
        rates_rend.setSeriesItemLabelsVisible(4, true) ;
        rates_rend.setSeriesVisibleInLegend(4, false) ;
        rates_rend.setSeriesItemLabelFont(5, new java.awt.Font("Open Sans" ,Font.BOLD, 14)) ;
        rates_rend.setSeriesItemLabelPaint(5, Color.GREEN);
        rates_rend.setSeriesItemLabelsVisible(5, true) ;
        rates_rend.setSeriesVisibleInLegend(5, false) ;
        
        rates_rend.setSeriesPositiveItemLabelPosition(3, new ItemLabelPosition(ItemLabelAnchor.INSIDE1, TextAnchor.BOTTOM_CENTER)) ;
        rates_rend.setSeriesPositiveItemLabelPosition(4, new ItemLabelPosition(ItemLabelAnchor.INSIDE1, TextAnchor.TOP_CENTER)) ;
        rates_rend.setSeriesPositiveItemLabelPosition(5, new ItemLabelPosition(ItemLabelAnchor.INSIDE7, TextAnchor.BOTTOM_CENTER)) ;
        ratesPlot.setRenderer(0, rates_rend);
 
        XYAreaRenderer busyrend = new XYAreaRenderer(XYAreaRenderer.AREA) ;
        // busyrend.setSeriesPaint(0, new java.awt.Color(255,233,236)) ;
        busyrend.setSeriesPaint(0, 
            new java.awt.GradientPaint(0.0f, 0.0f, Color.RED, 0.0f, 0.0f, new Color(233, 255, 236))) ;
           // new float[]{.0f, .03f, .1f}, new Color[] {Color.GREEN, new Color(255, 233, 236), Color.RED} ));
        ratesPlot.setRenderer(1, busyrend);
        ratesPlot.mapDatasetToRangeAxis(0, 0);
        ratesPlot.mapDatasetToRangeAxis(1, 1);

        chart = new JFreeChart(ratesPlot) ;
         // // chart.addLegend(new org.jfree.chart.title.LegendTitle(plot)) ;
        chart.setBackgroundPaint(new java.awt.Color(0, 0, 0, 0));

        setDefaultModel(new Model<>(chart)) ;
    }
 
    public void partitionChanged(final String currentPartition) {
        if ( chart == null ) {
            ers.Logger.log("Lazy constructing Chart") ;
            Construct() ;
        }
		if ( currentPartition.isEmpty() ) {
			ratesPlot.setDataset(0, null) ;
			ratesPlot.setDataset(1, null) ;
			return ;
		}
		try {
            if ( getId().equals("ratesplotseconds") ) {
                ratesPlot.setDataset(0, TrigRatesProvider.getRatesSeriesSeconds(currentPartition)) ;
                ratesPlot.setDataset(1, TrigRatesProvider.getBusySeriesSeconds(currentPartition)) ;    
            } else
            if ( getId().equals("ratesplotminutes") ) {
                ratesPlot.setDataset(0, TrigRatesProvider.getRatesSeriesMinutes(currentPartition)) ;
                ratesPlot.setDataset(1, TrigRatesProvider.getBusySeriesMinutes(currentPartition)) ; 
            }
        } catch ( final Exception ex ) {
            ers.Logger.error(ex) ;
        }
    }

    
    @Override
    protected DynamicImageResource getImageResource() {

        return new DynamicImageResource(){
            
            
            private static final long serialVersionUID = -7632810865011999986L;
            
            //private byte[] rastr = new byte[0] ;

            @Override
            protected byte[] getImageData(IResource.Attributes attributes) {
        
                JFreeChart chart = (JFreeChart)getDefaultModelObject();
                byte[] ret = {} ;
                if ( chart == null ) {
                    ers.Logger.log("Chart is not constructed") ;
                    return ret ;
                }
                try {
                    BufferedImage img = chart.createBufferedImage(width, height) ;
                    ret = toImageData(img) ;
                } catch ( final Exception ex ) {
                    ex.printStackTrace() ;
                } 
                return ret ;
            }
            
            @Override
            protected void setResponseHeaders(AbstractResource.ResourceResponse response, IResource.Attributes attributes) {
                response.disableCaching() ;
                response.setCacheDuration(Duration.NONE) ;
                response.setCacheScope(CacheScope.PRIVATE) ;    
            }
        };
    }
 
}