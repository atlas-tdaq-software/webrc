package tdaq.webrc;

import javax.naming.*;
import javax.naming.directory.*;

import java.util.Hashtable;


public class LdapAuth {

    /* from SPRING configuration
ldap.urls = ldap://cerndc.cern.ch:389/
ldap.base.dn = dc=cern,dc=ch
ldap.username = cn=atlog,ou=users,OU=Organic Units,dc=cern,dc=ch
ldap.password =
ldap.group.search.base = ou=users,OU=Organic Units
ldap.user.dn.pattern = CN={0},OU=Users,OU=Organic Units
*/

/* CERN
Name: CERN Address Book
Hostname: ldap.cern.ch 
Base DN: o=cern,c=ch​
Port Number: 636
Bind DN: cn=my-NICElogin,ou=users,o=cern,c=ch (replace my-NICElogin with your own login)
*/

    String LDAPBaseDirectory = "dc=cern,dc=ch";
    String LDAP_SERVER_ADDRESS = "cerndc.cern.ch";
    String LDAP_SERVER_PORT = "389";
    String LDAP_USER_DOMAIN = ",ou=users,OU=Organic Units,dc=cern,dc=ch";

    DirContext context ;
    SearchResult srLdapUser ;
    String eMail ;
    String userFullName ;  // displayName, FirstName FamilyName
    String principalName ; // userPrincipalName, andrei.akazarov@cern.ch, get this from SSO
    String userId ;        // sAMAccountName, akazarov

    public LdapAuth() { 
    }

    public String getUserFullName() {
        return userFullName ;
    }

    public String getUserId() {
        return userId ;
    }
 
    public String getUserPrincipalName() {
        return principalName ;
    }

    public String getUserEMail() {
        return eMail ;
    }

    // in case SSO pre-authentication, we have only andrei.akazarov@cern.ch as user ID, need to query LDAP for other things
    // sAMAccountName : akazarov
    // displayName : Andrei Kazarov
    // userPrincipalName : andrei.akazarov@cern.ch
    public void getFromPrincipalName(final String principalName) {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        String ldapURL = Configuration.get("ldap.url.anonymous");
        env.put(Context.PROVIDER_URL, ldapURL);
        env.put(Context.SECURITY_AUTHENTICATION, "none") ;

        try {
            context = new InitialDirContext(env);
            
            String searchFilter = "(&(objectClass=user)(userPrincipalName=" + principalName + "))";
            SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            final NamingEnumeration<SearchResult> results = context.search(Configuration.get("ldap.userdomain"), searchFilter, searchControls);
            final SearchResult srLdapUser = (SearchResult)results.nextElement() ;   

            final Attributes attrs = srLdapUser.getAttributes() ;
            System.out.println("mail: " + attrs.get("mail")) ;
            eMail = new String(attrs.get("mail").get().toString()) ;
            System.out.println("Full Name: " + attrs.get("displayName")) ;
            userFullName = new String(attrs.get("displayName").get().toString()) ;
            System.out.println("User ID: " + attrs.get("sAMAccountName")) ;
            userId = new String(attrs.get("sAMAccountName").get().toString()) ;
        } catch ( final Exception ex ) {
            ex.printStackTrace(); 
        }
    }

    public boolean authentify(final String userName, final String userPassword) {

        //LDAP responses with "true" if password == null
        if( userPassword.equals("") ) {
            return false;
        }

        try {
            System.out.println("Trying LDAP");
            // Set up the environment for creating the initial context
            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            String ldapURL = Configuration.get("ldap.url");
            System.out.println("URL: "+ ldapURL);
            env.put(Context.PROVIDER_URL, ldapURL);
            // 
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, "cn=" + userName + "," + Configuration.get("ldap.userdomain")) ;
            System.out.println("Principal: " + userName ); //DEBUG
            env.put(Context.SECURITY_CREDENTIALS, userPassword);
           
            context = new InitialDirContext(env);
            boolean result = (context != null);

            String searchFilter = "(&(objectClass=user)(sAMAccountName=" + userName + "))";
            SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
    
            NamingEnumeration<SearchResult> results = context.search(Configuration.get("ldap.userdomain"), searchFilter, searchControls);
            System.out.println("Search context: " +  Configuration.get("ldap.userdomain")) ;
            SearchResult srLdapUser = (SearchResult)results.nextElement() ;
            
            Attributes attrs = srLdapUser.getAttributes() ;
            System.out.println("mail: " + attrs.get("mail")) ;
            eMail = new String(attrs.get("mail").get().toString()) ;
            System.out.println("Full Name: " + attrs.get("displayName")) ;
            userFullName = new String(attrs.get("displayName").get().toString()) ;
           
            context.close();
            System.out.println("Authentication result: " + result);

            return result;     
        }
        catch (final Exception e) {          
            System.out.println(e.getStackTrace());
            e.printStackTrace();
            return false;
        }
    }
}