package tdaq.webrc ;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow ;

public class DBReloadPanel extends Panel
{
    private static final long serialVersionUID = -7484792636892353920L;

    public DBReloadPanel(final String id, final String message, final ModalWindow window, final HomePage homepage) {
        super(id);
        add(new Label("modifiedfiles", message)) ;

        add(new AjaxLink<Void>("dbreloadokbutton") {
            private static final long serialVersionUID = -1082819314481540767L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                homepage.setDbReloadOk(true) ;
                window.close(target);
            }
        });
        
        add(new AjaxLink<Void>("dbreloadcancelbutton") {
            private static final long serialVersionUID = -8262069597464283694L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                homepage.setDbReloadOk(false) ;
                window.close(target) ;
            }
        });
    }
}