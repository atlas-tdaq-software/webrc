package tdaq.webrc ;

import java.util.concurrent.ConcurrentHashMap ;
import java.util.Map ;
import java.util.List ;
import java.util.ArrayList ;

/**
 * Single-instance class which monitors (via direct PMG subscription) exit status of RootController in a partition
 * and calls ISubscriber interface method on subscribed clients
 */
public class PartitionExitMonitor {

	/** Interface to be implemented by clients interested in notification on partition exit event */
	interface ISubscriber {
		/** called from PMG callback when RootController gets EXIT or SIGNALED */
		void partitionExited(final String partition) ; 
	}

//	static pmgClient.PmgClient pmgClient = null ;

	/**  collection of RootController PMG Process handles per partition, needed to track Partition state (exit) */
    static Map<String, pmgClient.Process> rootControllerProcs = new ConcurrentHashMap<String, pmgClient.Process>() ;
	/** collection of subscribers per partition  */
    static Map<String, List<ISubscriber>> subscribers = new ConcurrentHashMap<String, List<ISubscriber>>() ;

/*     static {
        try { 
			pmgClient = new pmgClient.PmgClient() ;
		} catch ( final pmgClient.ConfigurationException ex ) {
			ers.Logger.error(ex) ; 
			System.exit(1) ;
		}
    } */

	/**  called by clients (ISubscribers), creates if necessary new PMG subscription and adds subscriber to the list for notification */
	static synchronized void subscribe(final String partition, final ISubscriber subscriber)
		throws pmgClient.PmgClientException, is.InfoNotFoundException, is.RepositoryNotFoundException {

		if ( rootControllerProcs.containsKey(partition) ) {
			subscribers.get(partition).add(subscriber) ;
			ers.Logger.log("Added subscriber to an existing RootController handle for partition " + partition) ; 
			return ;
		}

		final String root_name = partition.equals("initial") ? HomePage.default_rc_app_name : HomePage.rc_app_name ;
		final rc.DAQApplicationInfo root = ISReciever.getDAQAppInfo( partition, root_name) ;
		final pmgClient.Handle handle = HomePage.pmgclient.lookup(root_name, partition, root.host) ;
		if ( handle == null ) { 
			ers.Logger.error(new ers.Issue("Failed to lookup " + root_name + " on host " + root.host + " in parition " + partition));
			return ; 
		} ;
		
		pmgClient.Process root_process = HomePage.pmgclient.getProcess(handle) ;
		ers.Logger.log("Got Root Controller Process in partition " + partition + " process: " + root_process) ; 
        boolean res = root_process.link(new pmgClient.Callback() {
			public void callbackFunction(pmgClient.Process process) {
				ers.Logger.log("Partition " + partition + " Root controller state change: " + process.getProcessStatusInfo().state ) ;
				switch ( process.getProcessStatusInfo().state.value() ) {
					case pmgpub.ProcessState._EXITED :
					case pmgpub.ProcessState._SIGNALED :
						ers.Logger.info("Root controller has gone, need to clean up this partition " + partition) ;
						clearPartition(partition);
						return ;
					default:
						ers.Logger.error(new Exception("Unexpected process status of RootController in partition " + partition + ": " + process.getProcessStatusInfo().state.value())) ;
				}
			}
		}) ;

        if ( res ) {
			ers.Logger.log("Subscribed in PMG to Root Controller status in partition " + partition) ; 
			ers.Logger.log("Root Controller in partition " + partition + " is linked: " + root_process.isLinked()) ;
            pmgClient.Process before = rootControllerProcs.put(partition, root_process) ;
			if ( before != null ) {
				ers.Logger.error(new Exception("Already had Root Controller process in the map!")) ;
			}
			List<ISubscriber> newlist = new ArrayList<ISubscriber>() ;
			newlist.add(subscriber) ;
			subscribers.put(partition, newlist) ;
		} else {
			ers.Logger.error(new Exception("FAILED to subscribe in PMG to Root Controller in partition " + partition)) ;
		}

		return ;
    }

	// need synchronization with subscribe, as it may take time and partition may be restarted meanwhile
	static synchronized void clearPartition(final String partition) {
		ers.Logger.debug(0, "Clearing partition " + partition + ". # Subscribers: " + subscribers.get(partition).size()) ; 
		rootControllerProcs.remove(partition) ;
		for (ISubscriber subscr : subscribers.get(partition) ) {	
			subscr.partitionExited(partition) ;
		}
		subscribers.remove(partition) ;
		TrigRatesProvider.removePartition(partition );
		ers.Logger.debug(0, "Cleared partition " + partition) ; 
	}

}
