package tdaq.webrc ;

import java.util.HashMap;
import java.util.Map;

import is.AnyInfo ;

/**
 * Class providing access to CTP busy published in L1CT IS server
 * 
*/

public class CTPBusyInfo {

    // mapping of segments names to CTP indexes in ctpout_cables attribute of
    // https://atlasop.cern.ch/info/current/ATLAS/is/L1CT/L1CT.CTP.Instantaneous.BusyFractions

	/* 
	0 False 0.0 False 0.0 0.0 IBL 12 0 False
	  False 1.0 False 0.0 0.0 BCM 12 1 False
	  False 1.0 False 0.0 0.0 Pixel 12 2 False
	  False 1.0 False 0.0 0.0 SCT 12 3 False
	  False 0.0 False 0.0 0.0 TRT 12 4 False
	  True 0.0 False 0.0 0.0 LAr EMB 13 0 True
	  True 0.0 False 0.0 0.0 LAr EC 13 1 True
	  True 0.0 False 0.0 0.0 LAr DT 13 2 True
	  False 0.0 False 0.0 0.0 Tile LB 13 3 False
	  False 0.0 False 0.0 0.0 Tile EB 13 4 False
	  True 0.0 False 0.0 0.0 L1Calo 14 0 True
	  True 0.0 False 0.0 0.0 MUCTPI 14 1 True
	  False 1.0 False 0.0 0.0 RPC 14 2 False
	  False 1.0 False 0.0 0.0 TGC 14 3 False
	  False 0.0 False 0.0 0.0 BIS 7/8 14 4 False
	  False 0.0 False 0.0 0.0 MDT B 15 0 False
	  False 0.0 False 0.0 0.0 MDT EC 15 1 False
	  False 0.0 False 0.0 0.0 NSW-C 15 2 False
	  False 1.0 False 0.0 0.0 ALFA 15 3 False
	  False 0.0 False 0.0 0.0 LUCID 15 4 False
	  False 1.0 False 0.0 0.0 NSW-A 17 0 False
	  False 0.0 False 0.0 0.0 - 17 1 False
	  False 0.0 False 0.0 0.0 AFP 17 2 False
	  False 0.0 False 0.0 0.0 LHCf 17 3 False
   24 False 0.0 False 0.0 0.0 ZDC 17 4 False 
    */

    static final Map<String, Integer> DET_BUSY = new HashMap<String, Integer>() {
        /**
        *
        */
        private static final long serialVersionUID = -9174531802992744090L;

        {
        put("TTC_IBL", 0) ;
        put("BCM", 1) ;
        put("TTC_Pixel", 2) ;
        put("SCT_Combined", 3) ;
        put("TRT_Segment", 4) ;
        put("EMB", 5) ; // LAr
        put("EMEC", 6) ;
        put("Digital_Trigger", 7) ;
        put("TileLBA", 8) ; // Tile A
        put("TileEBA", 9) ; 
        put("TileLBC", 8) ; // Tile C (the same a A)
        put("TileEBC", 9) ; 
        put("L1Calo", 10) ;
        put("L1CT_MUCTPI", 11) ; // under L1CentralTrigger
        put("RPC", 12) ;
        put("TGC", 13) ;
        put("-", 14) ; // MMegas?
        put("MDTBarrelA", 15) ;
        put("MDTEndcapA", 16) ;
        put("NSWEndcapC", 17) ;
        put("ALFA", 18) ;
        put("LUCID", 19) ;
        put("NSWEndcapA", 20) ;
        put("-", 21) ;
        put("AFP", 22) ;
        put("LHCf", 22) ;
        put("ZDC", 22) ;
    }} ;

    public enum BUSY_TYPE { INST, RUN } ;

    /**
     * checks if this subsegment represnts a CTP busy source
     * @param detector name
     * @return true if detector is a busy contributor
     */
    static public boolean isDetBusyAvailable(final String detector) {
        return DET_BUSY.containsKey(detector) ;
    }

    /**
     * 
     * @param partition
     * @param detector name
     * @param type INST or RUN
     * @return float percent of busy, either instantaneous or per-run as requested in type
     */
    static public float getDetBusyFraction(final String partition, final String det, BUSY_TYPE type) {
        // get inst CTP busy from L1CT.CTP.Instantaneous.BusyFractions 
        // get accum in run CTP busy from L1CT.CTP.PerRun.BusyFractions 
        // ctpcore_objects {type: 'CtpBusyInfo'}
        //  e.g. attributes('ctpcore_objects').wrapObjectArray[9].attributes('fraction').float as totalFraction
        // ctpcore_objects index: 3
        // ctpout_cables index: 5
        // CtpBusyInfo.in_partition index: 0
        // CtpBusyInfo.enabled index: 8
        // CtpBusyInfo.fraction index: 1
       
                if ( !DET_BUSY.containsKey(det) ) return 0.0f ;

                try {
                    is.Repository repo = new is.Repository(new ipc.Partition(partition)) ;
                    is.AnyInfo info = new is.AnyInfo() ;
                    repo.getValue(Configuration.get(type == BUSY_TYPE.INST ? "busy.source.inst" : "busy.source.run"), info) ;
                    AnyInfo[] ctpout_cables = (AnyInfo[])info.getAttribute(Configuration.getInt("busy.cables.index")) ;
                    if ( ! ((Boolean)ctpout_cables[DET_BUSY.get(det)].getAttribute(8)).booleanValue() ) return 0.0f ; // disabled
                    float busy = 100*((Float)ctpout_cables[DET_BUSY.get(det)].getAttribute(1)).floatValue() ;
                    // return String.format("%.2f", busy);           
                    return busy ;
                }
                catch ( final is.RepositoryNotFoundException | is.InfoNotFoundException ex ) {
                }
                catch ( final Exception ex ) {
                    ex.printStackTrace() ;
                }
                return 0.0f;
    }

    static public String getTotalBusyFraction(final String partition) {
        try {
            is.Repository repo = new is.Repository(new ipc.Partition(partition)) ;
            is.AnyInfo info = new is.AnyInfo() ;
            repo.getValue(Configuration.get("busy.source"), info) ;
            AnyInfo[] ctpcore_objects = (AnyInfo[])info.getAttribute(Configuration.getInt("busy.index")) ;
            double busy = 100*Double.parseDouble(ctpcore_objects[Configuration.getInt("busy.ctpcore.total.index")].getAttribute(Configuration.getInt("busy.ctpcore.total.fraction.index")).toString()) ;    
            double simple = 100*Double.parseDouble(ctpcore_objects[Configuration.getInt("busy.ctpcore.simple.index")].getAttribute(Configuration.getInt("busy.ctpcore.total.fraction.index")).toString()) ;    
            double complex = 100*Double.parseDouble(ctpcore_objects[Configuration.getInt("busy.ctpcore.complex.index")].getAttribute(Configuration.getInt("busy.ctpcore.total.fraction.index")).toString()) ;    
            return String.format("%.1f (%.2f/%.2f)", busy, simple, complex);       
        }
        catch ( final is.RepositoryNotFoundException | is.InfoNotFoundException ex ) {
        }
        catch ( final Exception ex ) {
            ex.printStackTrace() ;                 
        }
        return "";
    }

}