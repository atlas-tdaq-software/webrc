package tdaq.webrc ;

import java.io.Serializable ;

class ERSIssue implements Serializable {

    private static final long serialVersionUID = 4925256882416957595L;
    
    private static final String time_format = Configuration.get("erstable.timeformat") ; // "yyyy-MM-dd HH:mm:ss z"

    public ERSIssue(final ers.Issue issue) {
        time = issue.ltime() ;
        // String time() as Mon Nov 25 11:01:33 CET 2019
        severity = issue.severity().toString();
        application = issue.context().application_name();
        id = issue.getId();
        message = issue.message();
        chained = false ;
        parent_application = "" ;
        parent_severity = "" ;
        ers.AbstractContext ctx = issue.context() ;
        context = "HOST: " + ctx.host_name() + "\nPID: " + ctx.process_id() + "\nUSER: " + ctx.user_name()
            + "\nPACKAGE: "+ ctx.package_name() + "\nFUNCTION: " + ctx.function_name() + "\nSOURCE:"
            + ctx.file_name() + "@" + ctx.line_number() + "\nCWD: " + ctx.cwd() ;
    }
    
    // chained: time, severity and application are empty (inherited from parent when displayed in table)
    // keep parent severity and application for column filtering
    public ERSIssue(final ers.Issue parent, final String _id, final String _msg) {
        application = "" ;
        severity = "" ;
        time = 0 ;
        id = _id ;
        message = _msg ;
        context = "" ;
        chained = true ;
        parent_severity = parent.severity().toString() ;
        parent_application = parent.context().application_name() ;
    }
    
    // return more compact representation
    public String getTime() {
        if ( time == 0 ) return "" ; // nested

        java.util.Date date = new java.util.Date(time); 
        // the format of your date
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(time_format); 
        // give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("Europe/Paris")); 

        return sdf.format(date);
    }

    public String getSeverity() {
        return severity;
    }

    public String getSeverityParent() {
        return chained ? parent_severity : severity;
    }

    public String getApplication() {
        return application;
    }

    public String getApplicationParent() {
        return chained ? parent_application : application ;
    }

    public String getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String getContext() {
        return context;
    }

    public boolean getChained() {
        return chained;
    }

    public String toString() {
        return time+ ": " + severity + ": " + message ;
    }

    final long time;
    final String severity;
    final String application;
    final String parent_severity;
    final String parent_application;
    final String id;
    final String message;
    final String context;
    final boolean chained;
}