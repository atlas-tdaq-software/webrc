package tdaq.webrc ;

import org.apache.wicket.request.mapper.parameter.PageParameters ;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.extensions.markup.html.repeater.tree.NestedTree;
import org.apache.wicket.ajax.AbstractAjaxTimerBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.util.time.Duration;

public class ERSPanel extends WebPage implements IHomePage, PartitionExitMonitor.ISubscriber { 

	final String partition ;
	final String default_partition = "ATLAS" ;
	final int default_update_interval = 2 ; // seconds
	final String default_subscription = "sev=FATAL OR sev=ERROR OR sev=WARNING" ;
	final int default_rows = 10 ;

	String gone_partition, subscription ;
	int partitionsHash = 0 ;

	RCTreeNodeProvider rctreeprov = null ;
	NestedTree<RCNode> tree = null ;
	ERSProvider ersProvider ;
	ERSDataTable ersTable ;
	ERSIssueFilter ersFilter = new ERSIssueFilter() ;

	public ERSPanel(final PageParameters parameters) {
		super(parameters) ;
		
		//setStatelessHint(true) ;
		setVersioned(false) ;
		
		add(new org.apache.wicket.ajax.AjaxNewWindowNotifyingBehavior() {

			private static final long serialVersionUID = 4564564564L;

			@Override
			protected void onNewWindow(AjaxRequestTarget target) {
				System.err.println("The same page rendering " + getPageClass().toString()) ;
				target.appendJavaScript(";alert('You opened a new browser tab to share the same WebRC session: this is UNSUPPORTED.\\n" +
				"Please close this tab and start a new Private browser window.');");
				//setResponsePage(getPageClass(), getPageParameters()) ;
			}

		}) ;

		partition = parameters.get("partition").toString(default_partition) ;
		subscription = parameters.get("subscription").toString(default_subscription) ;
		int updateinterval = parameters.get("refresh").toInt(default_update_interval*1000) ;
		int rows = parameters.get("rows").toInt(default_rows) ;
		boolean chained = parameters.get("chained").toBoolean(true) ;

		ersProvider = new ERSProvider(ersFilter) ;
		ersProvider.showChained(chained) ;

		try {
			ersProvider.subscribe(partition, subscription) ;
			System.err.println("Subscribed in ERS with " + subscription);
			PartitionExitMonitor.subscribe(partition, this) ;
			gone_partition = "" ;
		}
		catch ( final ers.BadStreamConfiguration ex ) { // partition is not running at the moment we launched ERSPanel
			System.err.println("Chained to chained exception: " + ex.getCause().getCause());
			gone_partition = partition ; // will wait for partition to appear
		}
		catch ( final Exception ex ) {
			// TODO report somehow e.g. to JS console ?
			ex.printStackTrace();
			return ; 
		}

		ersProvider.showChained(true) ; // TODO configurable

		ersTable = new ERSDataTable("erstable", ersProvider, ersFilter, rows) ;
		add(ersTable.getTable()) ;

		AbstractAjaxTimerBehavior updateBeh = new AbstractAjaxTimerBehavior(Duration.milliseconds(15000)) {
            private static final long serialVersionUID = 5680069957499209316L;

            @Override
            protected void onTimer(AjaxRequestTarget target) {
                java.util.List<String> list = Partitions.getIPCPartitionNames(true) ; //refresh hash
                int newhash = Partitions.hash() ;

                if ( partitionsHash != newhash ) {
                    if ( list.contains(gone_partition) ) {
                        rc.RCStateInfo root = ISReciever.getRCInfo( gone_partition, gone_partition.equals("initial") ? HomePage.default_rc_app_name : HomePage.rc_app_name) ;
                        if ( !(root.state.equals("NONE") || root.state.equals("INITIAL")) )  {
                            ers.Logger.log("Partition not yet ready " + gone_partition + ": " + root.state) ;
                            return ;
                        }
                        ers.Logger.log("Partition re-appeared: " + gone_partition + " in state " + root.state) ;
                    	try {
							ersProvider.subscribe(partition, subscription) ;
							PartitionExitMonitor.subscribe(partition, ERSPanel.this);
							gone_partition = "" ;
						} catch ( final Exception ex ) {
							ex.printStackTrace() ;
						}					
                    }
                    partitionsHash = newhash ;
                    target.add(ersTable.getTable());
                } 
            }
        } ;	

		add(updateBeh) ;
	}

	@Override
	public void partitionExited(final String part) {
		System.err.println("ERS receiver Partition gone: " + partition);
		ersProvider.partitionGone(partition);
		gone_partition = partition ;
	}

}
