package tdaq.webrc ;

import java.util.List;
import java.util.ArrayList;

import org.apache.wicket.Component;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.IModel;
import org.apache.wicket.extensions.ajax.markup.html.AjaxEditableLabel;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.AbstractAjaxTimerBehavior;
import org.apache.wicket.util.time.Duration;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.behavior.AttributeAppender ;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.extensions.markup.html.repeater.data.table.export.ExportToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.table.export.CSVDataExporter;
import org.apache.wicket.extensions.markup.html.repeater.data.table.export.IDataExporter;

public class ERSDataTable  {

    final private AbstractAjaxTimerBehavior ersTableAjaxTimer ;
    final private ERSProvider ersProvider ;
    final private ERSIssueFilter ersFilter ;
    private List<IColumn<ERSIssue, String>> columns  ;
    private MyAjaxDataTable table ;
    private MyExportToolbar toolbar ;
    private boolean paused = false ;

public AjaxFallbackDefaultDataTable<ERSIssue, String> getTable() {
    return table ;
}

class MyAjaxDataTable extends AjaxFallbackDefaultDataTable<ERSIssue, String>
// implements IAjaxIndicatorAware
{
    //private AjaxIndicatorAppender indicatorAppender = new AjaxIndicatorAppender();

    /**
     *
     */
    private static final long serialVersionUID = 357401909151854212L;

    public MyAjaxDataTable(final String id, final List<IColumn<ERSIssue, String>> columns, final ERSProvider provider,
            final int rows) {
        super(id, columns, provider, rows);
    //    add(indicatorAppender);
    }
    
    /*
    public String getAjaxIndicatorMarkupId() {
        return indicatorAppender.getMarkupId();
    }  */
}

public ERSDataTable(final String id, final ERSProvider provider, final ERSIssueFilter filter, final int rows) {

ersProvider = provider ;
ersFilter = filter ;

table = new MyAjaxDataTable (id, constructColumns(), provider, rows) {
    private static final long serialVersionUID = 1L;

    @Override
    protected void onPageChanged() {
        if ( getCurrentPage() == 0 && !paused ) {
            ersTableAjaxTimer.restart(null);
        } else {
            ersTableAjaxTimer.stop(null);
        }
        super.onPageChanged() ;
    }
} ;

int updateinterval = 1000 ;
try {
    updateinterval = Integer.parseInt(Configuration.get("erstable.update.interval")) ;
} catch ( final Exception ex ) {
    ex.printStackTrace() ;
}

ersTableAjaxTimer = new AbstractAjaxTimerBehavior(Duration.milliseconds(updateinterval)) {
    /**
     *
     */
    private static final long serialVersionUID = 5656435623452345676L;

    @Override
    protected void onTimer(AjaxRequestTarget target) {
        if ( ersProvider.checkUpdated() ) { // refresh table only if new Issues were added meanwhile
            target.add(table) ;
        }   
}   
} ; 

table.add(ersTableAjaxTimer) ;

toolbar = new MyExportToolbar(table) ;

    // toolbar.addDataExporter(new CSVDataExporter() ) ;
    // table.addTopToolbar(toolbar) ;
}

public void pause(AjaxRequestTarget target) {
    paused = true ;
    ersProvider.setPause(true) ;
    ersTableAjaxTimer.stop(target);
}

public void resume(AjaxRequestTarget target) {
    paused = false ;
    ersProvider.setPause(false) ;
    ersTableAjaxTimer.restart(target);
}

public void headersHidden(boolean flag, AjaxRequestTarget target) {
    table.add(new AttributeModifier("header-hidden", Boolean.toString(flag))) ;
    target.add(table) ;
} 

public Component getExportLink() {
    return toolbar.createExport("exporters", new CSVDataExporter()) ;
}

private final class MyExportToolbar extends ExportToolbar {
    private static final long serialVersionUID = 6399403213022976483L;

    public MyExportToolbar(AjaxFallbackDefaultDataTable<ERSIssue, String> table) {
        super(table) ;
    }
    
    public Component createExport(String componentId, IDataExporter dataExporter) {
        return createExportLink(componentId, dataExporter) ;
    }
}

private List<IColumn<ERSIssue, String>> constructColumns() {
    columns = new ArrayList<>() ;

    final String time_filter = "*" ;
    IModel<String> time_model = new Model<String>(time_filter) ;
    columns.add(new PropertyColumn<ERSIssue, String>(time_model, "time") {
        private static final long serialVersionUID = 56737567567567L;
            
        @Override
        public Component getHeader(String componentId) {
            return new AjaxEditableLabel<String>(componentId, time_model) {
                private static final long serialVersionUID = 6754564566456L;

                @Override
                public void onEdit(AjaxRequestTarget target) {
                    ersTableAjaxTimer.stop(target);
                    super.onEdit(target);
                }

                @Override
                protected void onSubmit(AjaxRequestTarget target) {
                    try {
                        ersFilter.setTime(getModel().getObject()) ;
                        super.onSubmit(target);
                    } finally {
                        ersTableAjaxTimer.restart(target) ;
                        target.add(table) ;
                    }
                }
            } ;
        } 

        @Override
        public void populateItem(Item<ICellPopulator<ERSIssue>> item, String componentId, IModel<ERSIssue> rowModel) {
            super.populateItem(item, componentId, rowModel);
            item.add(new AttributeAppender("class", Model.of("ers-time "))) ;				
        }
    });	
		
    String sev_filter = "*" ;
    IModel<String> sev_model = new Model<String>(sev_filter) ;
    columns.add(new PropertyColumn<ERSIssue, String>(sev_model, "severity") {
        private static final long serialVersionUID = 1674567567L;
            
        @Override
        public Component getHeader(String componentId) {
            return new AjaxEditableLabel<String>(componentId, sev_model) {
                private static final long serialVersionUID = 154646456L;

                @Override
                public void onEdit(AjaxRequestTarget target) {
                    ersTableAjaxTimer.stop(target);
                    super.onEdit(target);
                }

                @Override
                protected void onSubmit(AjaxRequestTarget target) {
                    try {
                        ersFilter.setSeverity(getModel().getObject()) ;
                        super.onSubmit(target);
                    } finally {
                        ersTableAjaxTimer.restart(target) ;
                        target.add(table) ;
                    }
                }
            } ;
        } 

        @Override
        public void populateItem(Item<ICellPopulator<ERSIssue>> item, String componentId, IModel<ERSIssue> rowModel) {
            super.populateItem(item, componentId, rowModel);
            item.add(new AttributeAppender("class", Model.of("ers-severity "))) ;				
            item.add(new AttributeAppender("class", Model.of("ers-" + rowModel.getObject().getSeverity()))) ;				
        }
    });	

    String app_filter = "*" ;
    IModel<String> app_model = new Model<String>(app_filter) ;
    columns.add(new PropertyColumn<ERSIssue, String>(app_model, "application") {
        private static final long serialVersionUID = 1567456745657L;

        @Override
        public Component getHeader(String componentId) {
            return new AjaxEditableLabel<String>(componentId, app_model) {
                private static final long serialVersionUID = 15464644565456L;

                @Override
                public void onEdit(AjaxRequestTarget target) {
                    ersTableAjaxTimer.stop(target);
                    super.onEdit( target);
                }

                @Override
                    protected void onSubmit(AjaxRequestTarget target) {
                        try {
                            ersFilter.setApp(getModel().getObject()) ;
                            super.onSubmit(target) ;
                        } finally {
                            ersTableAjaxTimer.restart(target) ;
                            target.add(table) ;
                        }
                    }
                } ;
        } 

        @Override
        public String getCssClass() {
            return "ers-application";
        }
    });		

    String id_filter = "*" ;
    IModel<String> id_model = new Model<String>(id_filter) ;
    columns.add(new PropertyColumn<ERSIssue, String>(new Model<String>(id_filter), "id"){
        private static final long serialVersionUID = 143564534346546L;
        
        @Override
        public Component getHeader(String componentId) {
            return new AjaxEditableLabel<String>(componentId, id_model) {
                private static final long serialVersionUID = 15464644565456L;

                @Override
                public void onEdit(AjaxRequestTarget target) {
                    ersTableAjaxTimer.stop(target);
                    super.onEdit( target);
                }

                @Override
                    protected void onSubmit(AjaxRequestTarget target) {
                        try {
                            ersFilter.setId(getModel().getObject()) ;
                            super.onSubmit(target);
                        } finally {
                            ersTableAjaxTimer.restart(target) ;
                            target.add(table) ;
                        }
                    }
                } ;
        } 

        @Override
        public String getCssClass() {
            return "ers-id";
        }

        public void populateItem(Item<ICellPopulator<ERSIssue>> cellItem, String componentId, IModel<ERSIssue> rowModel) {
            super.populateItem(cellItem, componentId, rowModel);
            final String popup ;
            if ( rowModel.getObject().getChained() ) {
                popup = rowModel.getObject().getSeverityParent() + " " + rowModel.getObject().getApplicationParent() ;
            } else {
                popup = rowModel.getObject().getMessage() ;
            }
            cellItem.add(AttributeModifier.replace("title", popup ));		
        }
    });
    
    IModel<String> mess_model = new Model<String>(id_filter) ;
    columns.add(new PropertyColumn<ERSIssue, String>(mess_model, "message"){
        private static final long serialVersionUID = 34534545656567L;

        @Override
        public Component getHeader(String componentId) {
            return new AjaxEditableLabel<String>(componentId, mess_model) {
                private static final long serialVersionUID = 15464644565456L;

                @Override
                public void onEdit(AjaxRequestTarget target) {
                    ersTableAjaxTimer.stop(target);
                    super.onEdit(target);
                }

                @Override
                protected void onSubmit(AjaxRequestTarget target) {
                    try {
                        ersFilter.setMessage(getModel().getObject()) ;
                        super.onSubmit(target);
                    } finally {
                        ersTableAjaxTimer.restart(target) ;
                        target.add(table) ;
                    }
                }
            } ;
        } 

        @Override
        public String getCssClass() {
            return "ers-message";
        }

        @Override
        public void populateItem(Item<ICellPopulator<ERSIssue>> cellItem, String componentId, IModel<ERSIssue> rowModel) {
            super.populateItem(cellItem, componentId, rowModel);
            cellItem.add(AttributeModifier.replace("title", rowModel.getObject().getContext() ));		
        }
    });
    return columns ;
}

}