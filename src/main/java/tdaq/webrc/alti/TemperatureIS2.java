package tdaq.webrc.alti;


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * 
 * @author  produced by the IS generator
 */

public class TemperatureIS2 extends is.Info {
    public static final is.Type type = new is.Type( new TemperatureIS2( ) );


    /**
     * name of the sensor
     */
    public String              name;

    /**
     * value in degrees Celsius
     */
    public float               value;


    public TemperatureIS2() {
	this( "TemperatureIS2" );
    }

    protected TemperatureIS2( String type ) {
	super( type );
	name = "";
	value = 0f;

// <<BeginUserCode>>

// <<EndUserCode>>
    }

    public void publishGuts( is.Ostream out ) {
	super.publishGuts( out );
	out.put( name ).put( value );
    }

    public void refreshGuts( is.Istream in ) {
	super.refreshGuts( in );
	name = in.getString(  );
	value = in.getFloat(  );
    }


// <<BeginUserCode>>

// <<EndUserCode>>
}

// <<BeginUserCode>>

// <<EndUserCode>>

