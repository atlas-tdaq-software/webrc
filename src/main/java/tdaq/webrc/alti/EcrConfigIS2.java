package tdaq.webrc.alti;


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * 
 * @author  produced by the IS generator
 */

public class EcrConfigIS2 extends is.Info {
    public static final is.Type type = new is.Type( new EcrConfigIS2( ) );


    /**
     * Period of the ECR dignsl in microseconds
     */
    public float               period;

    /**
     * Internal deadtime before each ECR in microseconds
     */
    public float               pre_deadtime;

    /**
     * Internal deadtime after each ECR in microseconds
     */
    public float               post_deadtime;

    /**
     * Length of ECR/BGo-1 signal in units of BCs
     */
    public int                 length;

    /**
     * ECR automatic generation started or stopped
     */
    public String              is_started;


    public EcrConfigIS2() {
	this( "EcrConfigIS2" );
    }

    protected EcrConfigIS2( String type ) {
	super( type );
	period = 0f;
	pre_deadtime = 0f;
	post_deadtime = 0f;
	length = 0;
	is_started = "0";

// <<BeginUserCode>>

// <<EndUserCode>>
    }

    public void publishGuts( is.Ostream out ) {
	super.publishGuts( out );
	out.put( period ).put( pre_deadtime ).put( post_deadtime ).put( length, false ).put( is_started );
    }

    public void refreshGuts( is.Istream in ) {
	super.refreshGuts( in );
	period = in.getFloat(  );
	pre_deadtime = in.getFloat(  );
	post_deadtime = in.getFloat(  );
	length = in.getInt(  );
	is_started = in.getString(  );
    }


// <<BeginUserCode>>

// <<EndUserCode>>
}

// <<BeginUserCode>>

// <<EndUserCode>>

