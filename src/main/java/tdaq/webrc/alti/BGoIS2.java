package tdaq.webrc.alti;


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * 
 * @author  produced by the IS generator
 */

public class BGoIS2 extends is.Info {
    public static final is.Type type = new is.Type( new BGoIS2( ) );


    /**
     * BGo name
     */
    public String              name;

    /**
     * delay of the inhibit signal
     */
    public int                 inhibit_delay;

    /**
     * width of the inhibit signal
     */
    public int                 inhibit_width;

    /**
     * BGo mode
     */
    public String              mode;

    /**
     * BGo command busy gating
     */
    public boolean             busy_gate;

    /**
     * Fifo retransmit
     */
    public boolean             retransmit;


    public BGoIS2() {
	this( "BGoIS2" );
    }

    protected BGoIS2( String type ) {
	super( type );
	name = "";
	inhibit_delay = 0;
	inhibit_width = 0;
	mode = "";
	busy_gate = false;
	retransmit = false;

// <<BeginUserCode>>

// <<EndUserCode>>
    }

    public void publishGuts( is.Ostream out ) {
	super.publishGuts( out );
	out.put( name ).put( inhibit_delay, false ).put( inhibit_width, false ).put( mode );
	out.put( busy_gate ).put( retransmit );
    }

    public void refreshGuts( is.Istream in ) {
	super.refreshGuts( in );
	name = in.getString(  );
	inhibit_delay = in.getInt(  );
	inhibit_width = in.getInt(  );
	mode = in.getString(  );
	busy_gate = in.getBoolean(  );
	retransmit = in.getBoolean(  );
    }


// <<BeginUserCode>>

// <<EndUserCode>>
}

// <<BeginUserCode>>

// <<EndUserCode>>

