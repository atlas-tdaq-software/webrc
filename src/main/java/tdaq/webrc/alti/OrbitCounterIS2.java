package tdaq.webrc.alti;


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * 
 * @author  produced by the IS generator
 */

public class OrbitCounterIS2 extends is.Info {
    public static final is.Type type = new is.Type( new OrbitCounterIS2( ) );


    /**
     * number of orbits with 3564 BCs
     */
    public int                 nb_good;

    /**
     * number of orbits with less than 3564 BCs
     */
    public int                 nb_short;

    /**
     * number of orbits with more than 3564 BCs
     */
    public int                 nb_long;

    /**
     * minimum orbit length [BC]
     */
    public int                 min;

    /**
     * maximum orbit length [BC]
     */
    public int                 max;


    public OrbitCounterIS2() {
	this( "OrbitCounterIS2" );
    }

    protected OrbitCounterIS2( String type ) {
	super( type );
	nb_good = 0;
	nb_short = 0;
	nb_long = 0;
	min = 0;
	max = 0;

// <<BeginUserCode>>

// <<EndUserCode>>
    }

    public void publishGuts( is.Ostream out ) {
	super.publishGuts( out );
	out.put( nb_good, false ).put( nb_short, false ).put( nb_long, false ).put( min, false );
	out.put( max, false );
    }

    public void refreshGuts( is.Istream in ) {
	super.refreshGuts( in );
	nb_good = in.getInt(  );
	nb_short = in.getInt(  );
	nb_long = in.getInt(  );
	min = in.getInt(  );
	max = in.getInt(  );
    }


// <<BeginUserCode>>

// <<EndUserCode>>
}

// <<BeginUserCode>>

// <<EndUserCode>>

