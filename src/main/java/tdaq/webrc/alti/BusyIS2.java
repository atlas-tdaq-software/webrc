package tdaq.webrc.alti;


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * 
 * @author  produced by the IS generator
 */

public class BusyIS2 extends is.Info {
    public static final is.Type type = new is.Type( new BusyIS2( ) );


    /**
     * name of the counter
     */
    public String              name;

    /**
     * busy fraction [%]
     */
    public float               fraction;

    /**
     * number of BC with Busy
     */
    public int                 value;

    /**
     * total number of BCs
     */
    public int                 interval;

    /**
     * counter value overflow
     */
    public int                 overflow;

    /**
     * busy source enabled
     */
    public boolean             enabled;


    public BusyIS2() {
	this( "BusyIS2" );
    }

    protected BusyIS2( String type ) {
	super( type );
	name = "";
	fraction = 0f;
	value = 0;
	interval = 0;
	overflow = 0;
	enabled = false;

// <<BeginUserCode>>

// <<EndUserCode>>
    }

    public void publishGuts( is.Ostream out ) {
	super.publishGuts( out );
	out.put( name ).put( fraction ).put( value, false ).put( interval, false ).put( overflow, false );
	out.put( enabled );
    }

    public void refreshGuts( is.Istream in ) {
	super.refreshGuts( in );
	name = in.getString(  );
	fraction = in.getFloat(  );
	value = in.getInt(  );
	interval = in.getInt(  );
	overflow = in.getInt(  );
	enabled = in.getBoolean(  );
    }


// <<BeginUserCode>>

// <<EndUserCode>>
}

// <<BeginUserCode>>

// <<EndUserCode>>

