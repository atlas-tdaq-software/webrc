package tdaq.webrc.alti;


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * 
 * @author  produced by the IS generator
 */

public class AltiMonitoringIS2 extends is.Info {
    public static final is.Type type = new is.Type( new AltiMonitoringIS2( ) );


    /**
     * Alti board configured
     */
    public boolean             is_configured;

    /**
     * Ctp-slave, Alti-slave, Lemo-slave, Alti-master or Alti-expert
     */
    public String              mode;

    /**
     * internal or external
     */
    public String              clock_selection;

    /**
     * equalizer configuration of the CTP-IN cable
     */
    public String              equalizer_config_ctp;

    /**
     * equalizer configuration of the ALTI-IN cable
     */
    public String              equalizer_config_alti;

    /**
     * input signals clock phase synchronisation: [0, 7]
     */
    public short               input_clock_phase_sync;

    /**
     * input signals shaping: disabled, 1 BC or 2 BC
     */
    public short               input_shaping;

    /**
     * PLL output clock phase shift [ns]
     */
    public int                 clock_phase;

    /**
     * Jitter Cleaner configuration
     */
    public String              jc_config;

    /**
     * BGo-0 or BGo-2
     */
    public String              bgo2_output;

    /**
     * BGo-1 or BGo-3
     */
    public String              bgo3_output;

    /**
     * Local busy source from FRONT-PANEL (NIM), CTP-OUT, ALTI-OUT, PG, VME
     */
    public String              busy_local_source;

    /**
     * INACTIVE, from LOCAL, from CTP, from ALTI
     */
    public String              busy_to_ctp;

    /**
     * INACTIVE, from LOCAL, from CTP, from ALTI
     */
    public String              busy_to_alti;

    /**
     * ACTIVE or INACTIVE
     */
    public String              busy_vme;

    /**
     * NIM or TTL
     */
    public String              busy_level;

    /**
     * for bits 0, 1, 2: local calibration request source from RJ45, FRONT_PANEL, CTP, ALTI, PG, VME
     */
    public String              cal_req_local_source;

    /**
     * for bits 0, 1, 2: INACTIVE, from LOCAL, from CTP-OUT, from ALTI-OUT
     */
    public String              cal_req_to_ctp;

    /**
     * for bits 0, 1, 2: INACTIVE, from LOCAL, from CTP-OUT, from ALTI-OUT
     */
    public String              cal_req_to_alti;

    /**
     * Source of ORBIT: INACTIVE, EXTERNAL, PATTERN or MINICTP
     */
    public String              orb_source;

    /**
     * Source of L1A: INACTIVE, EXTERNAL, PATTERN or MINICTP
     */
    public String              l1a_source;

    /**
     * Source of TTR1..2: INACTIVE, EXTERNAL, PATTERN or CALIBREQUEST
     */
    public String              ttr_source;

    /**
     * Source of BGO0..3: INACTIVE, EXTERNAL, PATTERN or MINICTP
     */
    public String              bgo_source;

    /**
     * Source of TriggerType: INACTIVE, EXTERNAL, PATTERN or MINICTP
     */
    public String              ttyp_source;

    /**
     * source of L1A in TTC stream from: L1A, TTR1, TTR2, TTR3
     */
    public String              ttc_l1a_source;

    /**
     * Pattern-Generator enabled or disabled
     */
    public boolean             pg_enabled;

    /**
     * REPEATED or ONESHOT
     */
    public String              pg_repeat;

    /**
     * pattern generation start address
     */
    public int                 pg_start;

    /**
     * pattern generation stop address
     */
    public int                 pg_stop;

    /**
     * pattern generation input file
     */
    public String              pg_file;

    /**
     * trigger type delay
     */
    public int                 ttyp_delay;

    /**
     * trigger type address
     */
    public int                 ttyp_address;

    /**
     * trigger type sub-address
     */
    public int                 ttyp_subaddress;

    /**
     * trigger type frame type: SHORT or LONG
     */
    public String              ttyp_frame_type;

    /**
     * trigger type address space: INTERNAL or EXTERNAL
     */
    public String              ttyp_address_space;

    /**
     * name, to CTP_OUT, to ALTI_OUT, to NIM_OUT, to FPGA]
     */
    public SignalSwitchIS2[]   signals_switch;

    /**
     * period [ms], pre-deadtime [ms], post-deadtime [ms], length [BC], is started
     */
    public EcrConfigIS2        ecr_config;

    /**
     * [name, inhibit_delay, inhibit_width, mode, busy_gate, retransmit]
     */
    public BGoIS2[]            bgos;

    /**
     * [name, enabled]
     */
    public TransmitterIS2[]    transmitters;

    /**
     * [name, value, rate, nb orbits,  overflow]
     */
    public CounterIS2[]        rate_counters;

    /**
     * [name, fraction [%], value [BC], interval [BC], overflow, enabled]
     */
    public BusyIS2[]           busy_counters;

    /**
     * nb orbits = 3564 BCs, nb orbits below 3564 BCs, nb orbits above 3564 BCs, min length [BC], max length [BC]
     */
    public OrbitCounterIS2     orbit_counters;

    /**
     * [name, value, rate, nb orbits,  overflow]
     */
    public CounterIS2[]        minictp_counters;

    /**
     * [name, nominal [V], measured [V], difference [%]]
     */
    public VoltageIS2[]        voltages;

    /**
     * [name, temperature]
     */
    public TemperatureIS2[]    temperatures;


    public AltiMonitoringIS2() {
	this( "AltiMonitoringIS2" );
    }

    protected AltiMonitoringIS2( String type ) {
	super( type );
	mode = "";
	clock_selection = "";
	equalizer_config_ctp = "";
	equalizer_config_alti = "";
	jc_config = "";
	bgo2_output = "";
	bgo3_output = "";
	busy_local_source = "";
	busy_to_ctp = "";
	busy_to_alti = "";
	busy_vme = "";
	busy_level = "";
	cal_req_local_source = "";
	cal_req_to_ctp = "";
	cal_req_to_alti = "";
	orb_source = "";
	l1a_source = "";
	ttr_source = "";
	bgo_source = "";
	ttyp_source = "";
	ttc_l1a_source = "";
	pg_repeat = "";
	pg_file = "";
	ttyp_frame_type = "";
	ttyp_address_space = "";
	signals_switch = new SignalSwitchIS2[0];
	bgos = new BGoIS2[0];
	transmitters = new TransmitterIS2[0];
	rate_counters = new CounterIS2[0];
	busy_counters = new BusyIS2[0];
	minictp_counters = new CounterIS2[0];
	voltages = new VoltageIS2[0];
	temperatures = new TemperatureIS2[0];

// <<BeginUserCode>>

// <<EndUserCode>>
    }

    public void publishGuts( is.Ostream out ) {
	super.publishGuts( out );
	out.put( is_configured ).put( mode ).put( clock_selection ).put( equalizer_config_ctp );
	out.put( equalizer_config_alti ).put( input_clock_phase_sync, false ).put( input_shaping, false );
	out.put( clock_phase, true ).put( jc_config ).put( bgo2_output ).put( bgo3_output );
	out.put( busy_local_source ).put( busy_to_ctp ).put( busy_to_alti ).put( busy_vme );
	out.put( busy_level ).put( cal_req_local_source ).put( cal_req_to_ctp ).put( cal_req_to_alti );
	out.put( orb_source ).put( l1a_source ).put( ttr_source ).put( bgo_source ).put( ttyp_source );
	out.put( ttc_l1a_source ).put( pg_enabled ).put( pg_repeat ).put( pg_start, false );
	out.put( pg_stop, false ).put( pg_file ).put( ttyp_delay, false ).put( ttyp_address, false );
	out.put( ttyp_subaddress, false ).put( ttyp_frame_type ).put( ttyp_address_space );
	out.put( signals_switch, SignalSwitchIS2.type  ).put( ecr_config, EcrConfigIS2.type  );
	out.put( bgos, BGoIS2.type  ).put( transmitters, TransmitterIS2.type  ).put( rate_counters, CounterIS2.type  );
	out.put( busy_counters, BusyIS2.type  ).put( orbit_counters, OrbitCounterIS2.type  );
	out.put( minictp_counters, CounterIS2.type  ).put( voltages, VoltageIS2.type  ).put( temperatures, TemperatureIS2.type  );
    }

    public void refreshGuts( is.Istream in ) {
	super.refreshGuts( in );
	is_configured = in.getBoolean(  );
	mode = in.getString(  );
	clock_selection = in.getString(  );
	equalizer_config_ctp = in.getString(  );
	equalizer_config_alti = in.getString(  );
	input_clock_phase_sync = in.getShort(  );
	input_shaping = in.getShort(  );
	clock_phase = in.getInt(  );
	jc_config = in.getString(  );
	bgo2_output = in.getString(  );
	bgo3_output = in.getString(  );
	busy_local_source = in.getString(  );
	busy_to_ctp = in.getString(  );
	busy_to_alti = in.getString(  );
	busy_vme = in.getString(  );
	busy_level = in.getString(  );
	cal_req_local_source = in.getString(  );
	cal_req_to_ctp = in.getString(  );
	cal_req_to_alti = in.getString(  );
	orb_source = in.getString(  );
	l1a_source = in.getString(  );
	ttr_source = in.getString(  );
	bgo_source = in.getString(  );
	ttyp_source = in.getString(  );
	ttc_l1a_source = in.getString(  );
	pg_enabled = in.getBoolean(  );
	pg_repeat = in.getString(  );
	pg_start = in.getInt(  );
	pg_stop = in.getInt(  );
	pg_file = in.getString(  );
	ttyp_delay = in.getInt(  );
	ttyp_address = in.getInt(  );
	ttyp_subaddress = in.getInt(  );
	ttyp_frame_type = in.getString(  );
	ttyp_address_space = in.getString(  );
	signals_switch = in.getInfoArray( SignalSwitchIS2.class );
	ecr_config = in.getInfo( EcrConfigIS2.class );
	bgos = in.getInfoArray( BGoIS2.class );
	transmitters = in.getInfoArray( TransmitterIS2.class );
	rate_counters = in.getInfoArray( CounterIS2.class );
	busy_counters = in.getInfoArray( BusyIS2.class );
	orbit_counters = in.getInfo( OrbitCounterIS2.class );
	minictp_counters = in.getInfoArray( CounterIS2.class );
	voltages = in.getInfoArray( VoltageIS2.class );
	temperatures = in.getInfoArray( TemperatureIS2.class );
    }


// <<BeginUserCode>>

// <<EndUserCode>>
}

// <<BeginUserCode>>

// <<EndUserCode>>

