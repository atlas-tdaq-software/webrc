package tdaq.webrc.alti;


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * 
 * @author  produced by the IS generator
 */

public class TransmitterIS2 extends is.Info {
    public static final is.Type type = new is.Type( new TransmitterIS2( ) );


    /**
     * name of the transmitter
     */
    public String              name;

    /**
     * transmitter enabled or disabled
     */
    public boolean             enabled;

    /**
     * delay of the transmitter
     */
    public int                 delay;


    public TransmitterIS2() {
	this( "TransmitterIS2" );
    }

    protected TransmitterIS2( String type ) {
	super( type );
	name = "";
	enabled = false;
	delay = 0;

// <<BeginUserCode>>

// <<EndUserCode>>
    }

    public void publishGuts( is.Ostream out ) {
	super.publishGuts( out );
	out.put( name ).put( enabled ).put( delay, false );
    }

    public void refreshGuts( is.Istream in ) {
	super.refreshGuts( in );
	name = in.getString(  );
	enabled = in.getBoolean(  );
	delay = in.getInt(  );
    }


// <<BeginUserCode>>

// <<EndUserCode>>
}

// <<BeginUserCode>>

// <<EndUserCode>>

