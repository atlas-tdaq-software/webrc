package tdaq.webrc.alti;


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * 
 * @author  produced by the IS generator
 */

public class CounterIS2 extends is.Info {
    public static final is.Type type = new is.Type( new CounterIS2( ) );


    /**
     * name of the counter
     */
    public String              name;

    /**
     * value of the counter
     */
    public int                 value;

    /**
     * rate of the counter [kHz]
     */
    public float               rate;

    /**
     * number of orbits
     */
    public int                 nb_orbits;

    /**
     * counter value overflow
     */
    public int                 overflow;


    public CounterIS2() {
	this( "CounterIS2" );
    }

    protected CounterIS2( String type ) {
	super( type );
	name = "";
	value = 0;
	rate = 0f;
	nb_orbits = 0;
	overflow = 0;

// <<BeginUserCode>>

// <<EndUserCode>>
    }

    public void publishGuts( is.Ostream out ) {
	super.publishGuts( out );
	out.put( name ).put( value, false ).put( rate ).put( nb_orbits, false ).put( overflow, false );
    }

    public void refreshGuts( is.Istream in ) {
	super.refreshGuts( in );
	name = in.getString(  );
	value = in.getInt(  );
	rate = in.getFloat(  );
	nb_orbits = in.getInt(  );
	overflow = in.getInt(  );
    }


// <<BeginUserCode>>

// <<EndUserCode>>
}

// <<BeginUserCode>>

// <<EndUserCode>>

