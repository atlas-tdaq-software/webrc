package tdaq.webrc.alti;


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * 
 * @author  produced by the IS generator
 */

public class SignalSwitchIS2 extends is.Info {
    public static final is.Type type = new is.Type( new SignalSwitchIS2( ) );


    /**
     * signal name
     */
    public String              name;

    /**
     * source to CTP_OUT
     */
    public String              to_ctp;

    /**
     * source to ALTI_OUT
     */
    public String              to_alti;

    /**
     * source to NIM_OUT
     */
    public String              to_nim;

    /**
     * source to FPGA
     */
    public String              to_fpga;


    public SignalSwitchIS2() {
	this( "SignalSwitchIS2" );
    }

    protected SignalSwitchIS2( String type ) {
	super( type );
	name = "";
	to_ctp = "";
	to_alti = "";
	to_nim = "";
	to_fpga = "";

// <<BeginUserCode>>

// <<EndUserCode>>
    }

    public void publishGuts( is.Ostream out ) {
	super.publishGuts( out );
	out.put( name ).put( to_ctp ).put( to_alti ).put( to_nim ).put( to_fpga );
    }

    public void refreshGuts( is.Istream in ) {
	super.refreshGuts( in );
	name = in.getString(  );
	to_ctp = in.getString(  );
	to_alti = in.getString(  );
	to_nim = in.getString(  );
	to_fpga = in.getString(  );
    }


// <<BeginUserCode>>

// <<EndUserCode>>
}

// <<BeginUserCode>>

// <<EndUserCode>>

