package tdaq.webrc.alti;


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * 
 * @author  produced by the IS generator
 */

public class VoltageIS2 extends is.Info {
    public static final is.Type type = new is.Type( new VoltageIS2( ) );


    /**
     * name of the sensor
     */
    public String              name;

    /**
     * nominal value [V]
     */
    public float               nominal;

    /**
     * measured value [V]
     */
    public float               measured;

    /**
     * difference [%]
     */
    public float               difference;


    public VoltageIS2() {
	this( "VoltageIS2" );
    }

    protected VoltageIS2( String type ) {
	super( type );
	name = "";
	nominal = 0f;
	measured = 0f;
	difference = 0f;

// <<BeginUserCode>>

// <<EndUserCode>>
    }

    public void publishGuts( is.Ostream out ) {
	super.publishGuts( out );
	out.put( name ).put( nominal ).put( measured ).put( difference );
    }

    public void refreshGuts( is.Istream in ) {
	super.refreshGuts( in );
	name = in.getString(  );
	nominal = in.getFloat(  );
	measured = in.getFloat(  );
	difference = in.getFloat(  );
    }


// <<BeginUserCode>>

// <<EndUserCode>>
}

// <<BeginUserCode>>

// <<EndUserCode>>

