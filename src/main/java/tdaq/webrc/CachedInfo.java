package tdaq.webrc ;

public class CachedInfo<T> {

	public interface ValueProvider<T> {
		public T provideValue() ;
	}
	
	T value ;
	int timeout ;
	long last_update ;
	ValueProvider<T> provider ;

	public CachedInfo(int timeout, ValueProvider<T> provider) {
		this.timeout = timeout ;
		this.provider = provider ;
		value = provider.provideValue() ;
		last_update = java.time.Instant.now().getEpochSecond() ;
	}
	
	public synchronized T getValue() {
		final long currentTime = java.time.Instant.now().getEpochSecond() ;

		if ( currentTime < last_update + timeout ) {
			return value ;
		}

		last_update = currentTime ;
		value = provider.provideValue() ;
		return value ;
	}

}
