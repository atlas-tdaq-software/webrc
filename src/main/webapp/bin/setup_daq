#!/bin/sh

export TDAQ_ERS_DEBUG_LEVEL=0

source setup_functions

#source /sw/tdaq/setup/setup_tdaq-11-02-00.sh
[ -f /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh ] || { echo "CVMFS not available, exiting"; exit 1 ; }
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh -r tdaq-12-00-00 || { echo "Failed to source environment for TDAQ release"; exit 1 ; }
export TDAQ_TOKEN_CHECK=1

# oks-git-on, to allow r/w operations on databases
test -f /tbed/tdaq/sw/oks/tdaq_db_repository.value && export TDAQ_DB_REPOSITORY=`envsubst < /tbed/tdaq/sw/oks/tdaq_db_repository.value` || export TDAQ_DB_REPOSITORY="ssh://gitea@pc-tbed-git.cern.ch/oks/${CMTRELEASE}.git|http://pc-tbed-git.cern.ch/gitea/oks/${CMTRELEASE}.git";
echo "OKS GIT repository: ${TDAQ_DB_REPOSITORY}" 
#------------------------------------------------------------
#  default settings
#------------------------------------------------------------

use_gui="no"
use_elog="no"
timetest="no"
partition=""
timing_tests_file="play_daq_timing_tests"
archive_logs_locally="no"
archive_logs_centrally="no"
using_testbed=yes

if echo "${TDAQ_IPC_INIT_REF}" | egrep -q '^corbaloc:iiop:pc-tbed-onl-.*|^file:/tbed/tdaq/sw/ipc/.*'
then
    using_testbed=yes
fi

# some tuning for allowing more threads
ulimit -s 2048

#------------------------------------------------------------
# parse command line
#------------------------------------------------------------

while (test $# -gt 0 ); do
  case "$1" in
    -h | --help)
	print_usage
	exit 0
    ;;

    -ng)
	use_gui="no"
	shift
    ;;

    -ne)
        use_elog="no"
        shift
    ;;
    -nc)
        export TDAQ_IGUI_NO_WARNING_AT_CONNECT=TRUE
        shift
    ;;

    -al)
        archive_logs_locally="yes"
        shift
    ;;

    -ag)
        archive_logs_centrally="yes"
        shift
    ;;

    -tt)
	timetest="yes"
	use_gui="no"
	shift
    ;;

    -uc)
	timetest="yes"
	use_gui="no"
	shift
	timing_tests_file="$1"
	shift
    ;;
 
    -p)
	shift
	partition="$1"
	shift
    ;;

    -d)
	shift
	TDAQ_DB_DATA="$1"
	shift
    ;;

    *.xml)
	TDAQ_DB_DATA="$1"
	shift
    ;;

    *)
	partition="$1"
	shift
    ;;
  esac
done

if test -z "$TDAQ_DB_DATA"
  then
	echo "Error: Neither database file parameter nor TDAQ_DB_DATA environment are defined."
	print_usage
	exit 1
fi

if test -z "$partition"
  then
	echo "Error: partition parameter not specified. Please specify a partition you want to run."
	print_usage
	exit 1
fi

# file has absolute path or it is relative to pwd
# otherwise we assume that DAL calculates the rigth path
if test -r "${TDAQ_DB_DATA}"
 then
	TDAQ_DB_DATA="$(cd `dirname $TDAQ_DB_DATA` ; pwd)/`basename $TDAQ_DB_DATA`"
fi

export TDAQ_DB_DATA TDAQ_SETUP_CONNECT_TIMEOUT TDAQ_SETUP_ACTION_TIMEOUT
params="-d ${TDAQ_DB_DATA}"

print_release_info
#------------------------------------------------------------
# Disable elog if appropriate
#------------------------------------------------------------
if test "$use_elog" = "no"
    then
    unset TDAQ_ELOG_SERVER_URL
fi
#------------------------------------------------------------
# check if the database can be read by OKS
#------------------------------------------------------------
echo "Database file: ${TDAQ_DB_DATA}"
TDAQ_PARTITION="${partition}" verify_database
timestamp

echo "TDAQ_PARTITION: ${partition}"
TDAQ_PARTITION="${partition}"

timestamp

#------------------------
# Set config version
#------------------------
try_set_config_version

#------------------------
# Check AM permission
#------------------------
TDAQ_PARTITION=$TDAQ_PARTITION rc_am_requester > /dev/null
ret_val=$?
if [ ${ret_val} -ne 0 ]; then
      exit 1
fi

#--------------------------------------
# Check if the partition is already up
#--------------------------------------
test_ipc_server -p "${partition}" > /dev/null 2>&1
PARTITION_IS_UP=$?

#------------------------------------------------------------
# starting setup for partition 'initial', if it is not yet running
# and we are not in testbed.
#------------------------------------------------------------
if [ "${partition}" != "initial" -a "${using_testbed}" = "yes" ]; then
    echo -n "Checking initial partition..."

    if test_ipc_server > /dev/null 2>&1 && test_corba_server -c rmgr/ResMgr -n RM_Server > /dev/null 2>&1 && test_corba_server -p initial -c "rc/commander" -n DefaultRootController
    then
        echo "ok !"
    else
        cat <<EOF

The initial partition is not running.

If you are in Point 1 contact

  DAQ on-call (162772) - during data taking
  atlas-tdaq-daqhlt-ops@cern.ch - otherwise

If you are on the TDAQ testbed contact

  atlas-tdaq-cc-wg@cern.ch

EOF
        exit 1
    fi
else
    start_initial_partition || { echo "Exiting setup_daq."; exit 1 ; }
    if [ "${partition}" = "initial" ]; then
      use_gui=no
    fi
fi

timestamp
echo "TDAQ_DB_DATA: ${TDAQ_DB_DATA}"

echo -n "Getting the $partition Partition environment from the Database ... "
TDAQ_DB_DATA=$TDAQ_DB_DATA get_partition_env $partition
echo "OK"
echo "TDAQ_LOGS_PATH: ${TDAQ_LOGS_PATH}"
timestamp

#------------------------------------------------------------
# create and check logs directories
#------------------------------------------------------------

for d in ${TDAQ_LOGS_PATH} ${TDAQ_RESULTS_LOGS_PATH} ${TDAQ_BACKUP_PATH}; do
mkdir -p $d || { echo "Error: cannot create directory $d. Check your partition LogsRoot attribute." ; exit 1 ; }
chmod g+rwx $d > /dev/null 2>&1 &
done

#------------------------------------------------------------
# archive all err and out files from there
#------------------------------------------------------------
if test "$archive_logs_centrally" = "yes"
	then
	archive_gather_logs
elif  test "$archive_logs_locally" = "yes"
	then
	archive_logs 
fi

# this one sets start_time
if test "$timetest" = "yes"; then
	source play_daq_timing_functions
fi

#------------------------------------------------------------
# Process Management 
#------------------------------------------------------------
start_partition_pmg "${partition}"
ret_val=$?
if [ ${ret_val} -ne 0 ]; then
    exit 1
fi

#------------------------------------------------------------
# Run Control 
#------------------------------------------------------------

#echo "Starting Root Controller for partition $partition (log file in ${TDAQ_LOGS_PATH}/)"

start_root_controller
  ret_val=$?
  if [ ${ret_val} -ne 0 ]; then

	if [ -n "${TDAQ_SETUP_POINT1}" ]; then
		echo "FATAL: Failed to start up Root Controller for partition, exiting..."
		exit 1
	else
		echo "Failed to start up partition: cleaning up and exiting..."
        	pmg_kill_partition -p ${TDAQ_PARTITION}
		exit 1
	fi
  fi

echo "Root Controller started, infrastructure status: "
ipc_ls -lp ${TDAQ_PARTITION}

if test "$timetest" = "yes"
then
  export root_controller=${ROOT_CONTROLLER}
  echo "Running in automatic mode (no IGUI, automatic set-up)"

  echo "Waiting for Root controller to be ready"
  rc_waitstate -p ${TDAQ_PARTITION} -n $root_controller -W 150 NONE
 
  echo "Starting time tests from $timing_tests_file"
  source $timing_tests_file
      
  run_commands
  if test $? -ne 0; then aborted="true" ; fi
      
# RootController shutdown transition
  let dsashutdown_start_time=$SECONDS
  echo "Shutting down RC tree"
  rc_sender -p ${TDAQ_PARTITION} -n $root_controller -c SHUTDOWN
  rc_waitstate -p ${TDAQ_PARTITION} -n $root_controller -W 120 NONE
  let shutdown_stop_time=$SECONDS
      
  echo "Shutting the partition down..."
  rc_sender -p ${TDAQ_PARTITION} -n $root_controller -c EXIT && 
  { echo "Partition ${TDAQ_PARTITION} was terminated correctly." ; }
  
  let stop_time=$SECONDS
  
  source play_daq_write_timing  
  if ! test "$aborted" = "true"
      then
      write_test_results
  else
      write_test_results abort
  fi
  
  timestamp
  echo "Archiving log files"
  if ! test "$aborted" = "true"
      then
      archive_logs
      exit 0 
  else
      archive_logs "aborted"
      exit 2
  fi

# extra cleanup for safety	
  pmg_kill_partition -p ${TDAQ_PARTITION}

fi # timetest

# =======================================================================================
#  start IGUI
# =======================================================================================

if test "$use_gui" = "yes"; then

    my_time=`date -u +%s`
    IGUILOGFILE="${TDAQ_LOGS_PATH}/igui_${my_time}.out"
    echo "Starting IGUI for partition $partition (log file ${IGUILOGFILE})."
    echo "Please wait for a window to appear on your screen..."

    igui_pid=""
    TDAQ_PARTITION=$TDAQ_PARTITION TDAQ_DB=rdbconfig:RDB new_igui_start &
    igui_start_setup_pid=$!
fi

echo ""

	
echo "setup_daq script exiting"
