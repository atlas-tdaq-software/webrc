//const { Pod, Container } = require('@kubernetes/client-node')
const { Pod, Container, VolumeMount, Volume, Namespace } = require('kubernetes-models/v1')
const k8s = require('@kubernetes/client-node')
const kc = new k8s.KubeConfig()

kc.loadFromDefault()
const k8sCoreApi = kc.makeApiClient(k8s.CoreV1Api)
const k8sAppsApi = kc.makeApiClient(k8s.AppsV1Api)

var partition = "andrei-test-kube"
var release = "tdaq-09-03-00"
var ipc_ref = "corbaloc:iiop:ipc-gw-proxy-svc:12345/%ffipc/partition%00initial/ipc/partition/initial"
var image_tdaq = "gitlab-registry.cern.ch/atlas-tdaq-software/webrc:next_2"

var vm_cvmfs_atlas = new VolumeMount({mountPath: "/cvmfs/atlas.cern.ch", name: "cvmfs-atlas" }) ;
var vm_cvmfs_sft = new VolumeMount({mountPath: "/cvmfs/sft.cern.ch", name: "cvmfs-sft" }) ;
var vol_atlas = new Volume({ name: "cvmfs-atlas", persistentVolumeClaim: { claimName: "csi-cvmfs-atlas-pvc", readOnly: true } }) ;
var vol_sft = new Volume({ name: "cvmfs-sft", persistentVolumeClaim: { claimName: "csi-cvmfs-sft-pvc", readOnly: true } }) ;
var env_tdaq = [{ name: 'TDAQ_RELEASE', value: release }, { name: 'TDAQ_IPC_INIT_REF_KUBE', value: ipc_ref }] ;

function createPod(podname, command) {
  return new Pod({
    metadata: {
      name: podname,
    },
    spec: {
      	containers: [
        	new Container({
          		name: podname,
          		image: image_tdaq,
          		env: env_tdaq,
	  		command: ["/bin/bash"],
          		args: [ "--rcfile", "/etc/tdaqbashrc", "-ci", command],
          		volumeMounts: [ vm_cvmfs_atlas, vm_cvmfs_sft ]
        	}),
      		],
	volumes: [vol_atlas, vol_sft],
    },
  })
}


function createDeploy(podname, command, arguments, replica) {
  var depl = {
    metadata: {
      name: podname,
    },
    spec: {
      	replicas: replica,
	selector: {
          matchLabels: {
             app: podname
          }
        },
	template: {
	   metadata: {
             labels: {
                app: podname
             }
           },
	   spec: {
 	      	containers: [
        		new Container({
          			name: podname,
          			image: image_tdaq,
          			env: env_tdaq,
	  			command: ["/bin/bash"],
          			args: [ "--rcfile", "/etc/tdaqbashrc", "-ci", command],
          			volumeMounts: [ vm_cvmfs_atlas, vm_cvmfs_sft ]}),
      		],
		volumes: [vol_atlas, vol_sft],
	   }
    	},
    },
  } ;
 return depl ;
}


var ipc_server_deploy = createDeploy('ipc-server-' + partition, 'ipc_server -p ' + partition, 1) ;

const ipc_server = createPod('ipc-server', 'ipc_server -p ' + partition)
const is_mts_server = createPod('is-server', 'is_server -n MTS -p ' + partition)

const namespace = {
  metadata: {
    name: partition
  } } ;

/*  
var ret0 = k8sCoreApi.createNamespace(namespace).then(
	(response) => {
    		console.log('Namespace ' + partition + ' created.'); },
  	(err) => {
    	console.log('Something went wrong');
    	console.log(err);
	process.exit(1) ;
   })
*/   
var ret1 = k8sAppsApi.createNamespacedDeployment('default', ipc_server_deploy).then(
	(response) => {
    		console.log('started: ' + ipc_server_deploy.metadata.name); },
  	(err) => {
    	console.log('Something went wrong');
    	console.log(err);
	process.exit(1) ;
  })

var ret2 = k8sCoreApi.createNamespacedPod('default', is_mts_server).then(() => console.log('is_server: success'))

// Any valid JSON is also valid YAML
const json = JSON.stringify(ipc_server, null, 2)

// console.log(json)
