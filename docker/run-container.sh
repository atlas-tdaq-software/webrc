#!/bin/bash

#version=${VERSION:-"1.0.3"}
release=${TDAQ_RELEASE:-"tdaq-12-00-00"}

[ -f "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh" ] || { echo "CVMFS required and not found, exiting"; exit 1 ;} 

echo Setting up ${release} from CVMFS
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh -r ${release}

# we are not on TBED, so oks-git is disabled by above setup script
export TDAQ_DB_REPOSITORY="ssh://gitea@pc-tbed-git.cern.ch/oks/${release}.git|http://pc-tbed-git.cern.ch/gitea/oks/${release}.git"

# some customization...
export TDAQ_TOKEN_CHECK=1
# "env" when run as service, "gssapi" (kerberos) when run interactively
# export TDAQ_TOKEN_ACQUIRE=${TDAQ_TOKEN_ACQUIRE:-"env"}
export TDAQ_ERS_NO_SIGNAL_HANDLERS=1

# conservative default options
JRE_MEM_OPTS=${JRE_MEM_OPTS:="-Xms256M -Xmx512M"}

#export TDAQ_IPC_INIT_REF=$TDAQ_IPC_INIT_REF_KUBE
echo running with IPC ref ${TDAQ_IPC_INIT_REF}

# need a key to access gitea on tbed
echo running as $(whoami) with ids_rsa key
#echo ${SSH_KEY}
# needed to access oks-git in gitea as tdaqsw, SSH_KEY is set from a secret (tbed k8s only)
if ! test -z ${SSH_KEY+X}; then
mkdir -p ~/.ssh && echo -e "${SSH_KEY//_/\\n}" > ~/.ssh/id_rsa && chmod og-rwx ~/.ssh/id_rsa || { echo "Failed to get tdaqsw id_rsa key" ; exit 1 ; }
echo "StrictHostKeyChecking no" >> ~/.ssh/config
fi

cd /usr/local/webrc

# environment clientID and clientSecret shoudl be set in deployment from webrc-gpn Secret
echo Application will authenticate in SSO with app ID ${clientID}

#-Dis.mode.pull="true" \

# to add application root to all webrc urls
#-Dapplication.context="/tbed/webrc/" \

[ -d "/tbed/oks" ] || { echo "TBED OKS not found :(" ; }

exec $TDAQ_JAVA_HOME/bin/java \
-Dlog4j.configuration="file:${_here}/etc/log4j.xml" \
-Dorg.eclipse.jetty.util.log.class=org.eclipse.jetty.util.log.StdErrLog \
-Dorg.eclipse.jetty.LEVEL=${JETTY_LEVEL} \
-Ddbroot.path="/tbed/oks/${release}" \
-Dlocal.bin.path="/usr/local/webrc/bin" \
-Dtdaq.release.setup="/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh -r " \
-Dtdaq.webrc.properties="/usr/local/webrc/etc/webrc.properties.gpn" \
-Dcern.sso.openid.url="https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token" \
-Dcern.sso.client_id=${clientID} \
-Dcern.sso.client_secret=${clientSecret} \
-Dexperimental="true" \
-Dexpert.mode.group.required="atlas-testbed-user" \
-Djacorb.log.default.verbosity=1 -Djacorb.poa.log.verbosity=2 -Djacorb.poa.monitoring=off \
-Djacorb.giop.conn.log.verbosity=1 \
-Djacorb.poa.thread_pool_shared=true -Djacorb.poa.thread_pool_min=5 -Djacorb.poa.thread_pool_max=10 \
-Djacorb.poa.thread_priority=5 -Djacorb.poa.queue_wait=on -Djacorb.poa.queue_max=20000 -Djacorb.poa.queue_min=19000 \
-Djacorb.connection.client.idle_timeout=10000 \
-Dtdaq.ipc.init.ref=${TDAQ_IPC_INIT_REF} \
-XX:+UseParallelOldGC -XX:+UseNUMA \
${JRE_MEM_OPTS} \
-cp WEB-INF/classes:WEB-INF/lib/*:${TDAQ_CLASSPATH} \
tdaq/webrc/server/Start

