# HOWTO

Base image is cc7, taken from TDAQ CI repo at `gitlab.cern.ch/atlas-tdaq-software/tdaq_ci`
TDAQ s/w is taken from CVMFS, webrc RPM is installed in the same way as at P1. The only difference is the entry point, which is `run-container.sh`. Container assumes that application runs as `tdaqsw` and creates `/logs` folder tokeep the logs from tdaq applications.

## to build an image

provide path to webrc RPM (see ../build.sh) and build a new tagged version (e.g. 1.0.2)

`docker build --rm -v /home/akazarov/rpmbuild/RPMS/noarch/:/rpm:z -t webrc:1.0.2 .`

retag as latest and push the latest tag to the repo so it will be used by next deployment in k8s:
`docker tag webrc:1.0.2 gitlab-registry.cern.ch/atlas-tdaq-software/webrc`
`docker push gitlab-registry.cern.ch/atlas-tdaq-software/webrc:latest`

Update for Alma9:
Source is taken from target/webrc-$VERSION - generated during maven build.

`podman build  --rm -t webrc:1.1.24 -f docker/Dockerfile.el9 .`
`podman tag webrc:1.1.24 gitlab-registry.cern.ch/atlas-tdaq-software/webrc:1.1.24`
`podman push gitlab-registry.cern.ch/atlas-tdaq-software/webrc:1.1.24`

(to log in to the registry:)
`podman login gitlab-registry.cern.ch`

From lxplus after logging to paas, you may want to update image stream in test-webrc with new tag:
`oc login --token=...` // get it from paas.cern.ch
`oc project test-atdaq-webrc`
`oc tag gitlab-registry.cern.ch/atlas-tdaq-software/webrc:1.1.24 webrc:1.1.24`

Now you can update your paas deployment with new image.

## to run it in container with docker

expose web application port, connect your browser pr proxy to localhost:8888

`docker run -p 8888:8888 --env TDAQ_RELEASE=tdaq-11-02-00 -i -t -v /cvmfs:/cvmfs webrc`

## to run it in kubernetes cluster

first start the deployment
`kubectl create -f webrc.yaml`
and then the service wich exposes it on an external host:port (30088)
`kubectl create -f webrc-svc.yaml`

the port number you can get with
`kubectl describe svc webrc-service | grep NodePort:`
and the host is kubernetes cluster IP, which can be seen e.g. in
`kubectl cluster-info`

This service IP:PORT information is needed to configure SSO proxy in CERN PAAS, which exposes access to the `webrc-service` service via (https://atdaq-tbed-webrc.app.cern.ch) URL, at the same time allowing to execute DAQ actions on behalf of the authenticated user. The sso-proxy application is created in PAAS and managed in (https://application-portal.web.cern.ch), where one can specify that an authenticated user must belong to a e-group (e.g. atlas-tdaq-testbed) in order to get access to the application.

## Deployment details

`run-container.sh` is customized startup script which sets up TDAQ release, sets few environment variables and also creates `~tdaqsw/.ssh/id_rsa` from a secret which is created beforehand in the k8s. This is needed to access OKS/GIT databases (for getting partitions DBs, DB modification). In addition to id_rsa secret, an OpenID SSO application secret is needed, taken from its registration in paas - this secret is used in a key exchange with SSO to get a TDAQ tocken for each user authenticated in SSO. See [here](https://its.cern.ch/jira/browse/ADTCC-329) for more details. Webrc application configuration is loaded from `etc/webrc.properties.gpn`.
The deployment also needs CVMFS mounts for `atlas` and `sft` repos, which is created with `cvmfs-pvs.yaml` beforehand:

`kubectl create -f cvmfs-pvc.yaml`
