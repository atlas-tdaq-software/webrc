#!/bin/bash

HOST=`hostname -i`

source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh -r tdaq-09-03-00

#export TDAQ_IPC_INIT_REF="corbaloc:iiop:10.193.6.75:46603/%ffipc/partition%00initial/ipc/partition/initial"
export TDAQ_IPC_INIT_REF=$TDAQ_IPC_INIT_REF_KUBE

echo using $TDAQ_IPC_INIT_REF
echo starting on public $MY_HOST_IP:23456 and private $MY_POD_IP:12345

ipc_gateway_proxy -I $MY_HOST_IP -i $MY_POD_IP -p 12345 -P 23456
