#!/bin/bash

# -Dorg.eclipse.jetty.util.log.class=org.eclipse.jetty.util.log.StdErrLog \
# -verbose:gc -Xloggc:/tmp/webrc_gc_`/bin/date +%s` -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintTenuringDistribution -XX:+PrintGCApplicationConcurrentTime -XX:+PrintGCApplicationStoppedTime \
#version=1.0.5
source /sw/tdaq/setup/setup_tdaq-12-00-00.sh

# since use of java/config layer with JNI, this is needed to prevent crash (also in Igui)
export TDAQ_ERS_NO_SIGNAL_HANDLERS=1
export TDAQ_IPC_ENABLE_TLS=1

_here=$(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}}))

memopts_high="-Xms8192M -Xmx16384M"
memopts_low="-Xms1024M -Xmx2048M"

membytes=$(cat /proc/meminfo | grep MemTotal | awk '{print $2}')
memgbypes=$(expr $membytes / 1024 / 1024)
if test $memgbypes -gt 16 ;then memopts=$memopts_high; else memopts=$memopts_low; fi

cd ${_here}

exec $TDAQ_JAVA_HOME/bin/java \
-Dlog4j.configuration="file:${_here}/etc/log4j.xml" \
-Dtdaq.webrc.properties="${_here}/etc/webrc.properties" \
-Dwicket.configuration="deployment" \
-Dorg.eclipse.jetty.util.log.class=org.eclipse.jetty.util.log.StdErrLog \
-Dorg.eclipse.jetty.LEVEL=INFO \
-Djacorb.log.default.verbosity=1 -Djacorb.poa.log.verbosity=1 -Djacorb.poa.monitoring=off \
-Djacorb.giop.conn.log.verbosity=1 \
-XX:+UseParallelOldGC -XX:+UseNUMA \
$memopts \
-Djacorb.poa.thread_pool_shared=true -Djacorb.poa.thread_pool_min=20 -Djacorb.poa.thread_pool_max=40 \
-Djacorb.poa.thread_priority=5 -Djacorb.poa.queue_wait=on -Djacorb.poa.queue_max=400000 -Djacorb.poa.queue_min=399950 \
-Djacorb.connection.client.idle_timeout=10000 \
-Dtdaq.ipc.init.ref=${TDAQ_IPC_INIT_REF} \
-cp ${_here}/WEB-INF/classes:${_here}/WEB-INF/lib/*:${TDAQ_CLASSPATH} \
 tdaq/webrc/server/Start > /var/log/webrc/stdout.$$ 2> /var/log/webrc/stderr.$$

