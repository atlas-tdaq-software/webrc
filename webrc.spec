Name:		webrc
Version:	1.1.0
Release:	0
Summary:	TDAQ Web Run Control 

License:	Apache 2.0 license, Copyright 2000-2025 CERN for the benefit of the ATLAS collaboration
URL:      https://gitlab.cern.ch/atlas-tdaq-software/webrc
Packager: Andrei Kazarov <Andrei.Kazarov at cern.ch>

BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildArch:	noarch

%description
Web application presenting TDAQ Run Control tree and ERS table for partitions running at P1. TDAQ release version: 12.0.0

%define __jar_repack 0
%define _python_bytecompile_errors_terminate_build 0

%prep
cd %{_sourcedir}/
pwd

%build
cd %{_sourcedir}/
export LCG_INST_PATH=/cvmfs/sft.cern.ch/lcg/releases/
# this creates target/webrc-%{version} which contains whole webapp that we then install in /usr/local/webrc
./build.sh

# the package contains webapp built by maven in target/webrc-%{version} (exploded .war) plus few files needed to start it as a service
%install
echo %{buildroot}
echo %_topdir
echo %_rpmdir
rm -rf %{buildroot}
cd %{_sourcedir}
mkdir -p %{buildroot}/usr/lib/systemd/system/
mkdir -p %{buildroot}/usr/local/webrc/etc
mkdir -p %{buildroot}/var/log/webrc
cp -a target/webrc-%{version}/* %{buildroot}/usr/local/webrc
cp run.P1.sh %{buildroot}/usr/local/webrc/run.sh
cp run.gpn.sh %{buildroot}/usr/local/webrc/
cp etc/webrc.P1.properties %{buildroot}/usr/local/webrc/etc/webrc.properties
cp etc/webrc.properties %{buildroot}/usr/local/webrc/etc/webrc.properties.gpn
cp etc/log4j.xml %{buildroot}/usr/local/webrc/etc/
cp webrc.service %{buildroot}/usr/lib/systemd/system/

%clean
rm -rf %{buildroot}


%files
%defattr(-,tdaqsw,tdaqsw,-)
%dir /usr/local/webrc/
%dir /usr/local/webrc/etc
%dir /usr/local/webrc/bin
%dir %attr(755,tdaqsw,tdaqsw) /var/log/webrc
/usr/local/webrc/
%attr(755,tdaqsw,tdaqsw) /usr/local/webrc/run.sh
%attr(755,tdaqsw,tdaqsw) /usr/local/webrc/run.gpn.sh
%attr(755,tdaqsw,tdaqsw) /usr/local/webrc/bin/setup_daq
%attr(644,root,root) /usr/lib/systemd/system/webrc.service

%post
if [ "$1" = "1" ] ; then
echo "Enabling and starting webrc"
systemctl daemon-reload
systemctl start webrc
fi
exit 0

%preun
if [ "$1" = 0 ] ; then
echo "Stopping webrc"
systemctl stop webrc
systemctl daemon-reload
fi
exit 0 

%postun
if [ "$1" -ge 1 ]; then
echo "Restarting webrc"
systemctl daemon-reload
systemctl restart webrc
fi
exit 0 

%changelog
* Tue Dec 12 2023 Andrei Kazarov - 1.0.2
  new packaging, tdaq-11-02-00
* Fri Aug 14 2020 Andrei Kazarov - 0.9.9-0
  first working version, for P1
