#!/bin/sh

LCG_INST_PATH="/cvmfs/sft.cern.ch/lcg/releases"
# the following two may need updated with a new TDAQ release
LCG_VERSION="LCG_106b"
LCG_TAG="x86_64-el9-gcc13-opt"

#JAVA_TAG="8u312-80070"
#"8u222-884d8"
JAVA_TAG="8u392"
MAVEN_VERSION="3.6.1"

# in case /usr/bin/java is not available:
export JAVA_HOME=${LCG_INST_PATH}/${LCG_VERSION}/java/${JAVA_TAG}/${LCG_TAG}

# only maven is needed, just take it from some recent LCG release...
# the compile time dependencies on TDAQ s/w from cvmfs is controlled in pom.xml, along with other dependencies
$LCG_INST_PATH/${LCG_VERSION}/maven/${MAVEN_VERSION}/${LCG_TAG}/bin/mvn clean
$LCG_INST_PATH/${LCG_VERSION}/maven/${MAVEN_VERSION}/${LCG_TAG}/bin/mvn compile 
# -DskipTests is needed to avoid running real test, since it needs tdaq runtime stup (like in run.sh)
$LCG_INST_PATH/${LCG_VERSION}/maven/${MAVEN_VERSION}/${LCG_TAG}/bin/mvn -DskipTests package

chmod 755 target/webrc-*/bin/setup_daq

# for .tar deployment and runnin withg run.sh
#tar czvf webrc-910-0.9.9.1.tgz target/webrc-0.9.9 run.sh etc/webrc.properties

# for RPM deployment and run from systemd, rpm-build rpm is needed
# rpmbuild -bb --define "_sourcedir `pwd`" webrc.spec
